#!/bin/bash

PROJECTS="
device/unofficial/t-station
device/unofficial/t-station-binary
external/busybox
hardware/rockchip
hardware/realtek
"


# clone
for P in $PROJECTS
do
    DIR="./$(dirname ${P})"
    echo "--- $P( ${DIR} ) ---"
    mkdir -p ${DIR}
    pushd $DIR >> /dev/null
    git clone https://gitlab.com/aosp-tstation-ngt/aosp/${P}.git
    popd >> /dev/null
done


# rename(origin --> gitlab) and set upstream
for P in $PROJECTS
do
    echo "--- $P ---"
    pushd $P >> /dev/null
    git remote rename origin gitlab
    git branch -u gitlab/android-7.1.2_tstation android-7.1.2_tstation
    popd >> /dev/null
done
