typedef void* __gnuc_va_list;
typedef __gnuc_va_list va_list;
struct ftrace_branch_data {
 const char *func;
 const char *file;
 unsigned line;
 union {
  struct {
   unsigned long correct;
   unsigned long incorrect;
  };
  struct {
   unsigned long miss;
   unsigned long hit;
  };
  unsigned long miss_hit[2];
 };
};
struct kernel_symbol
{
 unsigned long value;
 const char *name;
};
enum {
 false = 0, true = 1
};
typedef signed char __s8;
typedef unsigned char __u8;
typedef signed short __s16;
typedef unsigned short __u16;
typedef signed int __s32;
typedef unsigned int __u32;
__extension__ typedef signed long long __s64;
__extension__ typedef unsigned long long __u64;
typedef signed char s8;
typedef unsigned char u8;
typedef signed short s16;
typedef unsigned short u16;
typedef signed int s32;
typedef unsigned int u32;
typedef signed long long s64;
typedef unsigned long long u64;
typedef struct {
 unsigned long fds_bits[1024 / (8 * sizeof(long))];
} __kernel_fd_set;
typedef void (*__kernel_sighandler_t)(int);
typedef int __kernel_key_t;
typedef int __kernel_mqd_t;
typedef unsigned short __kernel_mode_t;
typedef unsigned short __kernel_ipc_pid_t;
typedef unsigned short __kernel_uid_t;
typedef unsigned short __kernel_gid_t;
typedef unsigned short __kernel_old_dev_t;
typedef long __kernel_long_t;
typedef unsigned long __kernel_ulong_t;
typedef __kernel_ulong_t __kernel_ino_t;
typedef int __kernel_pid_t;
typedef __kernel_long_t __kernel_suseconds_t;
typedef int __kernel_daddr_t;
typedef unsigned int __kernel_uid32_t;
typedef unsigned int __kernel_gid32_t;
typedef __kernel_uid_t __kernel_old_uid_t;
typedef __kernel_gid_t __kernel_old_gid_t;
typedef unsigned int __kernel_size_t;
typedef int __kernel_ssize_t;
typedef int __kernel_ptrdiff_t;
typedef struct {
 int val[2];
} __kernel_fsid_t;
typedef __kernel_long_t __kernel_off_t;
typedef long long __kernel_loff_t;
typedef __kernel_long_t __kernel_time_t;
typedef __kernel_long_t __kernel_clock_t;
typedef int __kernel_timer_t;
typedef int __kernel_clockid_t;
typedef char * __kernel_caddr_t;
typedef unsigned short __kernel_uid16_t;
typedef unsigned short __kernel_gid16_t;
typedef __u16 __le16;
typedef __u16 __be16;
typedef __u32 __le32;
typedef __u32 __be32;
typedef __u64 __le64;
typedef __u64 __be64;
typedef __u16 __sum16;
typedef __u32 __wsum;
typedef __u32 __kernel_dev_t;
typedef __kernel_fd_set fd_set;
typedef __kernel_dev_t dev_t;
typedef __kernel_ino_t ino_t;
typedef __kernel_mode_t mode_t;
typedef unsigned short umode_t;
typedef __u32 nlink_t;
typedef __kernel_off_t off_t;
typedef __kernel_pid_t pid_t;
typedef __kernel_daddr_t daddr_t;
typedef __kernel_key_t key_t;
typedef __kernel_suseconds_t suseconds_t;
typedef __kernel_timer_t timer_t;
typedef __kernel_clockid_t clockid_t;
typedef __kernel_mqd_t mqd_t;
typedef int bool;
typedef __kernel_uid32_t uid_t;
typedef __kernel_gid32_t gid_t;
typedef __kernel_uid16_t uid16_t;
typedef __kernel_gid16_t gid16_t;
typedef unsigned long uintptr_t;
typedef __kernel_old_uid_t old_uid_t;
typedef __kernel_old_gid_t old_gid_t;
typedef __kernel_loff_t loff_t;
typedef __kernel_size_t size_t;
typedef __kernel_ssize_t ssize_t;
typedef __kernel_ptrdiff_t ptrdiff_t;
typedef __kernel_time_t time_t;
typedef __kernel_clock_t clock_t;
typedef __kernel_caddr_t caddr_t;
typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned int u_int;
typedef unsigned long u_long;
typedef unsigned char unchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef __u8 u_int8_t;
typedef __s8 int8_t;
typedef __u16 u_int16_t;
typedef __s16 int16_t;
typedef __u32 u_int32_t;
typedef __s32 int32_t;
typedef __u8 uint8_t;
typedef __u16 uint16_t;
typedef __u32 uint32_t;
typedef __u64 uint64_t;
typedef __u64 u_int64_t;
typedef __s64 int64_t;
typedef u64 sector_t;
typedef u64 blkcnt_t;
typedef u32 dma_addr_t;
typedef unsigned gfp_t;
typedef unsigned fmode_t;
typedef unsigned oom_flags_t;
typedef u32 phys_addr_t;
typedef phys_addr_t resource_size_t;
typedef unsigned long irq_hw_number_t;
typedef struct {
 int counter;
} atomic_t;
struct list_head {
 struct list_head *next, *prev;
};
struct hlist_head {
 struct hlist_node *first;
};
struct hlist_node {
 struct hlist_node *next, **pprev;
};
struct ustat {
 __kernel_daddr_t f_tfree;
 __kernel_ino_t f_tinode;
 char f_fname[6];
 char f_fpack[6];
};
struct callback_head {
 struct callback_head *next;
 void (*func)(struct callback_head *head);
};
struct pt_regs {
 unsigned long uregs[18];
};
extern
int ____ilog2_NaN(void);
typedef int (*initcall_t)(void);
typedef void (*exitcall_t)(void);
typedef void (*ctor_fn_t)(void);
void setup_arch(char **);
void prepare_namespace(void);
void load_default_modules(void);
struct obs_kernel_param {
 const char *str;
 int (*setup_func)(char *);
 int early;
};
void parse_early_param(void);
void parse_early_options(char *cmdline);
struct va_format {
 const char *fmt;
 va_list *va;
};


int vprintk_emit(int facility, int level, const char *dict, size_t dictlen, const char *fmt, va_list args);

int vprintk(const char *fmt, va_list args);

 int printk_emit(int facility, int level, const char *dict, size_t dictlen, const char *fmt, ...);

int printk(const char *fmt, ...);
 int printk_deferred(const char *fmt, ...);
void log_buf_kexec_setup(void);
void setup_log_buf(int early);
void dump_stack_set_arch_desc(const char *fmt, ...);
void dump_stack_print_info(const char *log_lvl);
void show_regs_print_info(const char *log_lvl);
enum {
 DUMP_PREFIX_NONE, DUMP_PREFIX_ADDRESS, DUMP_PREFIX_OFFSET
};
struct _ddebug {
 const char *modname;
 const char *function;
 const char *filename;
 const char *format;
 unsigned int lineno:18;
 unsigned int flags:8;
} ;
int ddebug_add_module(struct _ddebug *tab, unsigned int n, const char *modname);
size_t strlcpy(char *, const char *, size_t);
void *memchr_inv(const void *s, int c, size_t n);
char *strreplace(char *s, char old, char new);
int vbin_printf(u32 *bin_buf, size_t size, const char *fmt, va_list args);
int bstr_printf(char *buf, size_t size, const char *fmt, const u32 *bin_buf);
int bprintf(u32 *bin_buf, size_t size, const char *fmt, ...) ;
size_t memweight(const void *ptr, size_t bytes);
void memzero_explicit(void *s, size_t count);
struct sysinfo {
 __kernel_long_t uptime;
 __kernel_ulong_t loads[3];
 __kernel_ulong_t totalram;
 __kernel_ulong_t freeram;
 __kernel_ulong_t sharedram;
 __kernel_ulong_t bufferram;
 __kernel_ulong_t totalswap;
 __kernel_ulong_t freeswap;
 __u16 procs;
 __u16 pad;
 __kernel_ulong_t totalhigh;
 __kernel_ulong_t freehigh;
 __u32 mem_unit;
 char _f[20-2*sizeof(__kernel_ulong_t)-sizeof(__u32)];
};
struct bug_entry {
 unsigned long bug_addr;
 const char *file;
 unsigned short line;
 unsigned short flags;
};
extern
void warn_slowpath_fmt(const char *file, const int line, const char *fmt, ...);
extern
void warn_slowpath_fmt_taint(const char *file, const int line, unsigned taint, const char *fmt, ...);
struct pt_regs;
void die(const char *msg, struct pt_regs *regs, int err);
struct siginfo;
void arm_notify_die(const char *str, struct pt_regs *regs, struct siginfo *info, unsigned long err, unsigned long trap);
void hook_fault_code(int nr, int (*fn)(unsigned long, unsigned int, struct pt_regs *), int sig, int code, const char *name);
void hook_ifault_code(int nr, int (*fn)(unsigned long, unsigned int, struct pt_regs *), int sig, int code, const char *name);
struct mm_struct;
struct completion;
struct pt_regs;
struct user;
  static inline void __might_sleep(const char *file, int line, int preempt_offset) {
}


void panic(const char *fmt, ...)
 ;
void print_oops_end_marker(void);
void do_exit(long error_code)
 ;
void complete_and_exit(struct completion *, long)
 ;
int _kstrtoul(const char *s, unsigned int base, unsigned long *res);
int _kstrtol(const char *s, unsigned int base, long *res);
int kstrtoull(const char *s, unsigned int base, unsigned long long *res);
int kstrtoll(const char *s, unsigned int base, long long *res);
int kstrtouint(const char *s, unsigned int base, unsigned int *res);
int kstrtoint(const char *s, unsigned int base, int *res);
int kstrtou16(const char *s, unsigned int base, u16 *res);
int kstrtos16(const char *s, unsigned int base, s16 *res);
int kstrtou8(const char *s, unsigned int base, u8 *res);
int kstrtos8(const char *s, unsigned int base, s8 *res);
int kstrtoull_from_user(const char *s, size_t count, unsigned int base, unsigned long long *res);
int kstrtoll_from_user(const char *s, size_t count, unsigned int base, long long *res);
int kstrtoul_from_user(const char *s, size_t count, unsigned int base, unsigned long *res);
int kstrtol_from_user(const char *s, size_t count, unsigned int base, long *res);
int kstrtouint_from_user(const char *s, size_t count, unsigned int base, unsigned int *res);
int kstrtoint_from_user(const char *s, size_t count, unsigned int base, int *res);
int kstrtou16_from_user(const char *s, size_t count, unsigned int base, u16 *res);
int kstrtos16_from_user(const char *s, size_t count, unsigned int base, s16 *res);
int kstrtou8_from_user(const char *s, size_t count, unsigned int base, u8 *res);
int kstrtos8_from_user(const char *s, size_t count, unsigned int base, s8 *res);
extern
int snprintf(char *buf, size_t size, const char *fmt, ...);
extern
int vsnprintf(char *buf, size_t size, const char *fmt, va_list args);
extern
int scnprintf(char *buf, size_t size, const char *fmt, ...);
extern
int vscnprintf(char *buf, size_t size, const char *fmt, va_list args);
extern
char *kasprintf(gfp_t gfp, const char *fmt, ...);
extern
int sscanf(const char *, const char *, ...);
extern
int vsscanf(const char *, const char *, va_list);
struct pid;
unsigned long int_sqrt(unsigned long);
enum lockdep_ok {
 LOCKDEP_STILL_OK, LOCKDEP_NOW_UNRELIABLE
};
extern enum system_states {
 SYSTEM_BOOTING, SYSTEM_RUNNING, SYSTEM_HALT, SYSTEM_POWER_OFF, SYSTEM_RESTART, } system_state;
void tracing_off_permanent(void);
enum ftrace_dump_mode {
 DUMP_NONE, DUMP_ALL, DUMP_ORIG, };
void tracing_on(void);
void tracing_off(void);
int tracing_is_on(void);
void tracing_snapshot(void);
void tracing_snapshot_alloc(void);
extern
int __trace_bprintk(unsigned long ip, const char *fmt, ...);
extern
int __trace_printk(unsigned long ip, const char *fmt, ...);
extern int
__ftrace_vbprintk(unsigned long ip, const char *fmt, va_list ap);
extern int
__ftrace_vprintk(unsigned long ip, const char *fmt, va_list ap);
enum bug_trap_type {
 BUG_TRAP_TYPE_NONE = 0, BUG_TRAP_TYPE_WARN = 1, BUG_TRAP_TYPE_BUG = 2, };
struct pt_regs;
const struct bug_entry *find_bug(unsigned long bugaddr);
enum bug_trap_type report_bug(unsigned long bug_addr, struct pt_regs *regs);
int is_valid_bugaddr(unsigned long addr);
struct timespec;
struct compat_timespec;
struct restart_block {
 long (*fn)(struct restart_block *);
 union {
  struct {
   u32 *uaddr;
   u32 val;
   u32 flags;
   u32 bitset;
   u64 time;
   u32 *uaddr2;
  } futex;
  struct {
   clockid_t clockid;
   struct timespec *rmtp;
   u64 expires;
  } nanosleep;
  struct {
   struct pollfd *ufds;
   int nfds;
   int has_timeout;
   unsigned long tv_sec;
   unsigned long tv_nsec;
  } poll;
 };
};
struct vfp_hard_struct {
 __u64 fpregs[32];
 __u32 fpexc;
 __u32 fpscr;
 __u32 fpinst;
 __u32 fpinst2;
 __u32 cpu;
};
union vfp_state {
 struct vfp_hard_struct hard;
};
struct fp_hard_struct {
 unsigned int save[35];
};
struct fp_soft_struct {
 unsigned int save[35];
};
struct iwmmxt_struct {
 unsigned int save[0x98 / sizeof(unsigned int)];
};
union fp_state {
 struct fp_hard_struct hard;
 struct fp_soft_struct soft;
};
struct crunch_state {
 unsigned int mvdx[16][2];
 unsigned int mvax[4][3];
 unsigned int dspsc[2];
};
struct task_struct;
struct exec_domain;
struct outer_cache_fns {
 void (*inv_range)(unsigned long, unsigned long);
 void (*clean_range)(unsigned long, unsigned long);
 void (*flush_range)(unsigned long, unsigned long);
 void (*flush_all)(void);
 void (*inv_all)(void);
 void (*disable)(void);
 void (*set_debug)(unsigned long);
 void (*resume)(void);
};










typedef unsigned long mm_segment_t;
struct cpu_context_save {
 __u32 r4;
 __u32 r5;
 __u32 r6;
 __u32 r7;
 __u32 r8;
 __u32 r9;
 __u32 sl;
 __u32 fp;
 __u32 sp;
 __u32 pc;
 __u32 extra[2];
};
struct thread_info {
 unsigned long flags;
 int preempt_count;
 mm_segment_t addr_limit;
 struct task_struct *task;
 struct exec_domain *exec_domain;
 __u32 cpu;
 __u32 cpu_domain;
 struct cpu_context_save cpu_context;
 __u32 syscall;
 __u8 used_cp[16];
 unsigned long tp_value;
 union fp_state fpstate ;
 union vfp_state vfpstate;
 struct restart_block restart_block;
};
struct user_vfp;
struct user_vfp_exc;
 void preempt_schedule(void);
typedef struct {
 union {
  u32 slock;
  struct __raw_tickets {
   u16 owner;
   u16 next;
  } tickets;
 };
} arch_spinlock_t;
typedef struct {
 volatile unsigned int lock;
} arch_rwlock_t;
struct task_struct;
struct lockdep_map;
struct lock_class_key { };
typedef struct raw_spinlock {
 arch_spinlock_t raw_lock;
} raw_spinlock_t;
typedef struct spinlock {
 union {
  struct raw_spinlock rlock;
 };
} spinlock_t;
typedef struct {
 arch_rwlock_t raw_lock;
} rwlock_t;
struct task_struct;
struct arch_hw_breakpoint_ctrl {
  u32 __reserved : 9, mismatch : 1, : 9, len : 8, type : 2, privilege : 2, enabled : 1;
};
struct arch_hw_breakpoint {
 u32 address;
 u32 trigger;
 struct arch_hw_breakpoint_ctrl step_ctrl;
 struct arch_hw_breakpoint_ctrl ctrl;
};
struct notifier_block;
struct perf_event;
struct pmu;
int arch_install_hw_breakpoint(struct perf_event *bp);
void arch_uninstall_hw_breakpoint(struct perf_event *bp);
void hw_breakpoint_pmu_read(struct perf_event *bp);
int hw_breakpoint_slots(int type);
struct debug_info {
 struct perf_event *hbp[(16 + 16)];
};
struct thread_struct {
 unsigned long address;
 unsigned long trap_no;
 unsigned long error_code;
 struct debug_info debug;
};
struct task_struct;
unsigned long get_wchan(struct task_struct *p);
int in_lock_functions(unsigned long addr);
void _raw_spin_lock(raw_spinlock_t *lock) ;
void _raw_spin_lock_nested(raw_spinlock_t *lock, int subclass)
        ;
void
_raw_spin_lock_nest_lock(raw_spinlock_t *lock, struct lockdep_map *map)
        ;
void _raw_spin_lock_bh(raw_spinlock_t *lock) ;
void _raw_spin_lock_irq(raw_spinlock_t *lock)
        ;
unsigned long _raw_spin_lock_irqsave(raw_spinlock_t *lock)
        ;
unsigned long
_raw_spin_lock_irqsave_nested(raw_spinlock_t *lock, int subclass)
        ;
int _raw_spin_trylock(raw_spinlock_t *lock);
int _raw_spin_trylock_bh(raw_spinlock_t *lock);
void _raw_spin_unlock(raw_spinlock_t *lock) ;
void _raw_spin_unlock_bh(raw_spinlock_t *lock) ;
void _raw_spin_unlock_irq(raw_spinlock_t *lock) ;
void
_raw_spin_unlock_irqrestore(raw_spinlock_t *lock, unsigned long flags)
        ;
void _raw_read_lock(rwlock_t *lock) ;
void _raw_write_lock(rwlock_t *lock) ;
void _raw_read_lock_bh(rwlock_t *lock) ;
void _raw_write_lock_bh(rwlock_t *lock) ;
void _raw_read_lock_irq(rwlock_t *lock) ;
void _raw_write_lock_irq(rwlock_t *lock) ;
unsigned long _raw_read_lock_irqsave(rwlock_t *lock)
       ;
unsigned long _raw_write_lock_irqsave(rwlock_t *lock)
       ;
int _raw_read_trylock(rwlock_t *lock);
int _raw_write_trylock(rwlock_t *lock);
void _raw_read_unlock(rwlock_t *lock) ;
void _raw_write_unlock(rwlock_t *lock) ;
void _raw_read_unlock_bh(rwlock_t *lock) ;
void _raw_write_unlock_bh(rwlock_t *lock) ;
void _raw_read_unlock_irq(rwlock_t *lock) ;
void _raw_write_unlock_irq(rwlock_t *lock) ;
void
_raw_read_unlock_irqrestore(rwlock_t *lock, unsigned long flags)
       ;
void
_raw_write_unlock_irqrestore(rwlock_t *lock, unsigned long flags)
       ;
typedef struct {
 long long counter;
} atomic64_t;
typedef atomic_t atomic_long_t;
typedef struct __wait_queue wait_queue_t;
typedef int (*wait_queue_func_t)(wait_queue_t *wait, unsigned mode, int flags, void *key);
int default_wake_function(wait_queue_t *wait, unsigned mode, int flags, void *key);
struct __wait_queue {
 unsigned int flags;
 void *private;
 wait_queue_func_t func;
 struct list_head task_list;
};
struct wait_bit_key {
 void *flags;
 int bit_nr;
};
struct wait_bit_queue {
 struct wait_bit_key key;
 wait_queue_t wait;
};
struct __wait_queue_head {
 spinlock_t lock;
 struct list_head task_list;
};
typedef struct __wait_queue_head wait_queue_head_t;
struct task_struct;
void __wake_up(wait_queue_head_t *q, unsigned int mode, int nr, void *key);
void __wake_up_locked_key(wait_queue_head_t *q, unsigned int mode, void *key);
void __wake_up_sync_key(wait_queue_head_t *q, unsigned int mode, int nr, void *key);
void __wake_up_locked(wait_queue_head_t *q, unsigned int mode, int nr);
void __wake_up_sync(wait_queue_head_t *q, unsigned int mode, int nr);
void __wake_up_bit(wait_queue_head_t *, void *, int);
int __wait_on_bit(wait_queue_head_t *, struct wait_bit_queue *, int (*)(void *), unsigned);
int __wait_on_bit_lock(wait_queue_head_t *, struct wait_bit_queue *, int (*)(void *), unsigned);
void wake_up_bit(void *, int);
int out_of_line_wait_on_bit(void *, int, int (*)(void *), unsigned);
int out_of_line_wait_on_bit_lock(void *, int, int (*)(void *), unsigned);
wait_queue_head_t *bit_waitqueue(void *, int);
void prepare_to_wait(wait_queue_head_t *q, wait_queue_t *wait, int state);
void prepare_to_wait_exclusive(wait_queue_head_t *q, wait_queue_t *wait, int state);
void finish_wait(wait_queue_head_t *q, wait_queue_t *wait);
void abort_exclusive_wait(wait_queue_head_t *q, wait_queue_t *wait, unsigned int mode, void *key);
int autoremove_wake_function(wait_queue_t *wait, unsigned mode, int sync, void *key);
int wake_bit_function(wait_queue_t *wait, unsigned mode, int sync, void *key);
typedef struct seqcount {
 unsigned sequence;
} seqcount_t;
typedef struct {
 struct seqcount seqcount;
 spinlock_t lock;
} seqlock_t;
typedef struct { unsigned long bits[((((1 << 0)) + (8 * sizeof(long)) - 1) / (8 * sizeof(long)))]; } nodemask_t;
enum node_states {
 N_POSSIBLE, N_ONLINE, N_NORMAL_MEMORY, N_HIGH_MEMORY, N_MEMORY = N_HIGH_MEMORY, N_CPU, NR_NODE_STATES
};
struct nodemask_scratch {
 nodemask_t mask1;
 nodemask_t mask2;
};
enum pageblock_bits {
 PB_migrate, PB_migrate_end = PB_migrate + 3 - 1, PB_migrate_skip, NR_PAGEBLOCK_BITS
};
struct page;
unsigned long get_pageblock_flags_group(struct page *page, int start_bitidx, int end_bitidx);
void set_pageblock_flags_group(struct page *page, unsigned long flags, int start_bitidx, int end_bitidx);
struct page;
struct vm_area_struct;
struct cpu_user_fns {
 void (*cpu_clear_user_highpage)(struct page *page, unsigned long vaddr);
 void (*cpu_copy_user_highpage)(struct page *to, struct page *from, unsigned long vaddr, struct vm_area_struct *vma);
};
typedef u32 pteval_t;
typedef u32 pmdval_t;
typedef pteval_t pte_t;
typedef pmdval_t pmd_t;
typedef pmdval_t pgd_t[2];
typedef pteval_t pgprot_t;
typedef struct page *pgtable_t;
enum {
 MIGRATE_UNMOVABLE, MIGRATE_RECLAIMABLE, MIGRATE_MOVABLE, MIGRATE_PCPTYPES, MIGRATE_RESERVE = MIGRATE_PCPTYPES, MIGRATE_CMA, MIGRATE_ISOLATE, MIGRATE_TYPES
};
struct free_area {
 struct list_head free_list[MIGRATE_TYPES];
 unsigned long nr_free;
};
struct pglist_data;
struct zone_padding {
 char x[0];
} ;
enum zone_stat_item {
 NR_FREE_PAGES, NR_LRU_BASE, NR_INACTIVE_ANON = NR_LRU_BASE, NR_ACTIVE_ANON, NR_INACTIVE_FILE, NR_ACTIVE_FILE, NR_UNEVICTABLE, NR_MLOCK, NR_ANON_PAGES, NR_FILE_MAPPED, NR_FILE_PAGES, NR_FILE_DIRTY, NR_WRITEBACK, NR_SLAB_RECLAIMABLE, NR_SLAB_UNRECLAIMABLE, NR_PAGETABLE, NR_KERNEL_STACK, NR_UNSTABLE_NFS, NR_BOUNCE, NR_VMSCAN_WRITE, NR_VMSCAN_IMMEDIATE, NR_WRITEBACK_TEMP, NR_ISOLATED_ANON, NR_ISOLATED_FILE, NR_SHMEM, NR_DIRTIED, NR_WRITTEN, NR_ANON_TRANSPARENT_HUGEPAGES, NR_FREE_CMA_PAGES, NR_VM_ZONE_STAT_ITEMS };
enum lru_list {
 LRU_INACTIVE_ANON = 0, LRU_ACTIVE_ANON = 0 + 1, LRU_INACTIVE_FILE = 0 + 2, LRU_ACTIVE_FILE = 0 + 2 + 1, LRU_UNEVICTABLE, NR_LRU_LISTS
};
struct zone_reclaim_stat {
 unsigned long recent_rotated[2];
 unsigned long recent_scanned[2];
};
struct lruvec {
 struct list_head lists[NR_LRU_LISTS];
 struct zone_reclaim_stat reclaim_stat;
 struct zone *zone;
};
typedef unsigned isolate_mode_t;
enum zone_watermarks {
 WMARK_MIN, WMARK_LOW, WMARK_HIGH, NR_WMARK
};
struct per_cpu_pages {
 int count;
 int high;
 int batch;
 struct list_head lists[MIGRATE_PCPTYPES];
};
struct per_cpu_pageset {
 struct per_cpu_pages pcp;
 s8 stat_threshold;
 s8 vm_stat_diff[NR_VM_ZONE_STAT_ITEMS];
};
enum zone_type {
 ZONE_NORMAL, ZONE_HIGHMEM, ZONE_MOVABLE, __MAX_NR_ZONES
};
struct zone {
 unsigned long watermark[NR_WMARK];
 unsigned long percpu_drift_mark;
 unsigned long lowmem_reserve[3];
 unsigned long dirty_balance_reserve;
 struct per_cpu_pageset *pageset;
 spinlock_t lock;
 int all_unreclaimable;
 bool compact_blockskip_flush;
 unsigned long compact_cached_free_pfn;
 unsigned long compact_cached_migrate_pfn;
 struct free_area free_area[11];
 unsigned long *pageblock_flags;
 unsigned int compact_considered;
 unsigned int compact_defer_shift;
 int compact_order_failed;
 struct zone_padding _pad1_;
 spinlock_t lru_lock;
 struct lruvec lruvec;
 unsigned long pages_scanned;
 unsigned long flags;
 atomic_long_t vm_stat[NR_VM_ZONE_STAT_ITEMS];
 unsigned int inactive_ratio;
 struct zone_padding _pad2_;
 wait_queue_head_t * wait_table;
 unsigned long wait_table_hash_nr_entries;
 unsigned long wait_table_bits;
 struct pglist_data *zone_pgdat;
 unsigned long zone_start_pfn;
 unsigned long spanned_pages;
 unsigned long present_pages;
 unsigned long managed_pages;
 const char *name;
} ;
typedef enum {
 ZONE_RECLAIM_LOCKED, ZONE_OOM_LOCKED, ZONE_CONGESTED, } zone_flags_t;
struct zonelist_cache;
struct zoneref {
 struct zone *zone;
 int zone_idx;
};
struct zonelist {
 struct zonelist_cache *zlcache_ptr;
 struct zoneref _zonerefs[((1 << 0) * 3) + 1];
};
struct bootmem_data;
typedef struct pglist_data {
 struct zone node_zones[3];
 struct zonelist node_zonelists[1];
 int nr_zones;
 struct page *node_mem_map;
 struct page_cgroup *node_page_cgroup;
 struct bootmem_data *bdata;
 unsigned long node_start_pfn;
 unsigned long node_present_pages;
 unsigned long node_spanned_pages;
 int node_id;
 nodemask_t reclaim_nodes;
 wait_queue_head_t kswapd_wait;
 wait_queue_head_t pfmemalloc_wait;
 struct task_struct *kswapd;
 int kswapd_max_order;
 enum zone_type classzone_idx;
} pg_data_t;
struct mutex {
 atomic_t count;
 spinlock_t wait_lock;
 struct list_head wait_list;
 struct task_struct *owner;
 void *spin_mlock;
};
struct mutex_waiter {
 struct list_head list;
 struct task_struct *task;
};

struct rw_semaphore;
struct rw_semaphore {
 __s32 activity;
 raw_spinlock_t wait_lock;
 struct list_head wait_list;
};
typedef struct cpumask { unsigned long bits[(((4) + (8 * sizeof(long)) - 1) / (8 * sizeof(long)))]; } cpumask_t;
int cpumask_next_and(int n, const struct cpumask *, const struct cpumask *);
int cpumask_any_but(const struct cpumask *mask, unsigned int cpu);
typedef struct cpumask cpumask_var_t[1];
void set_cpu_possible(unsigned int cpu, bool possible);
void set_cpu_present(unsigned int cpu, bool present);
void set_cpu_online(unsigned int cpu, bool online);
void set_cpu_active(unsigned int cpu, bool active);
void init_cpu_present(const struct cpumask *src);
void init_cpu_possible(const struct cpumask *src);
void init_cpu_online(const struct cpumask *src);
extern const unsigned long
 cpu_bit_bitmap[32 +1][(((4) + (8 * sizeof(long)) - 1) / (8 * sizeof(long)))];
int __first_cpu(const cpumask_t *srcp);
int __next_cpu(int n, const cpumask_t *srcp);
struct completion {
 unsigned int done;
 wait_queue_head_t wait;
};
enum debug_obj_state {
 ODEBUG_STATE_NONE, ODEBUG_STATE_INIT, ODEBUG_STATE_INACTIVE, ODEBUG_STATE_ACTIVE, ODEBUG_STATE_DESTROYED, ODEBUG_STATE_NOTAVAILABLE, ODEBUG_STATE_MAX, };
struct debug_obj_descr;
struct debug_obj {
 struct hlist_node node;
 enum debug_obj_state state;
 unsigned int astate;
 void *object;
 struct debug_obj_descr *descr;
};
struct debug_obj_descr {
 const char *name;
 void *(*debug_hint) (void *addr);
 int (*fixup_init) (void *addr, enum debug_obj_state state);
 int (*fixup_activate) (void *addr, enum debug_obj_state state);
 int (*fixup_destroy) (void *addr, enum debug_obj_state state);
 int (*fixup_free) (void *addr, enum debug_obj_state state);
 int (*fixup_assert_init)(void *addr, enum debug_obj_state state);
};
void kfree_call_rcu(struct callback_head *head, void (*func)(struct callback_head *rcu));

u32 iter_div_u64_rem(u64 dividend, u32 divisor, u64 *remainder);
struct timespec {
 __kernel_time_t tv_sec;
 long tv_nsec;
};
struct timeval {
 __kernel_time_t tv_sec;
 __kernel_suseconds_t tv_usec;
};
struct timezone {
 int tz_minuteswest;
 int tz_dsttime;
};
struct itimerspec {
 struct timespec it_interval;
 struct timespec it_value;
};
struct itimerval {
 struct timeval it_interval;
 struct timeval it_value;
};
void timekeeping_init(void);
unsigned long get_seconds(void);
struct timespec current_kernel_time(void);
struct timespec __current_kernel_time(void);
struct timespec get_monotonic_coarse(void);
void get_xtime_and_monotonic_and_sleep_offset(struct timespec *xtim, struct timespec *wtom, struct timespec *sleep);
void timekeeping_inject_sleeptime(struct timespec *delta);
struct itimerval;
struct tms;
struct tm {
 int tm_sec;
 int tm_min;
 int tm_hour;
 int tm_mday;
 int tm_mon;
 long tm_year;
 int tm_wday;
 int tm_yday;
};
void time_to_tm(time_t totalsecs, int offset, struct tm *result);
struct timex {
 unsigned int modes;
 long offset;
 long freq;
 long maxerror;
 long esterror;
 int status;
 long constant;
 long precision;
 long tolerance;
 struct timeval time;
 long tick;
 long ppsfreq;
 long jitter;
 int shift;
 long stabil;
 long jitcnt;
 long calcnt;
 long errcnt;
 long stbcnt;
 int tai;
 int :32; int :32; int :32; int :32;
 int :32; int :32; int :32; int :32;
 int :32; int :32; int :32;
};
typedef unsigned long cycles_t;
int read_current_timer(unsigned long *timer_val);
void ntp_notify_cmos_timer(void);
u64 get_jiffies_64(void);
union ktime {
 s64 tv64;
};
typedef union ktime ktime_t;
struct tvec_base;
struct timer_list {
 struct list_head entry;
 unsigned long expires;
 struct tvec_base *base;
 void (*function)(unsigned long);
 unsigned long data;
 int slack;
 int start_pid;
 void *start_site;
 char start_comm[16];
};
void init_timer_key(struct timer_list *timer, unsigned int flags, const char *name, struct lock_class_key *key);

  extern int del_timer_sync(struct timer_list *timer);
struct hrtimer;
unsigned long __round_jiffies(unsigned long j, int cpu);
unsigned long __round_jiffies_relative(unsigned long j, int cpu);
unsigned long round_jiffies(unsigned long j);
unsigned long round_jiffies_relative(unsigned long j);
unsigned long __round_jiffies_up(unsigned long j, int cpu);
unsigned long __round_jiffies_up_relative(unsigned long j, int cpu);
unsigned long round_jiffies_up(unsigned long j);
unsigned long round_jiffies_up_relative(unsigned long j);
struct workqueue_struct;
struct work_struct;
typedef void (*work_func_t)(struct work_struct *work);
void delayed_work_timer_fn(unsigned long __data);
enum {
 WORK_STRUCT_PENDING_BIT = 0, WORK_STRUCT_DELAYED_BIT = 1, WORK_STRUCT_PWQ_BIT = 2, WORK_STRUCT_LINKED_BIT = 3, WORK_STRUCT_COLOR_SHIFT = 4, WORK_STRUCT_COLOR_BITS = 4, WORK_STRUCT_PENDING = 1 << WORK_STRUCT_PENDING_BIT, WORK_STRUCT_DELAYED = 1 << WORK_STRUCT_DELAYED_BIT, WORK_STRUCT_PWQ = 1 << WORK_STRUCT_PWQ_BIT, WORK_STRUCT_LINKED = 1 << WORK_STRUCT_LINKED_BIT, WORK_STRUCT_STATIC = 0, WORK_NR_COLORS = (1 << WORK_STRUCT_COLOR_BITS) - 1, WORK_NO_COLOR = WORK_NR_COLORS, WORK_CPU_UNBOUND = 4, WORK_CPU_END = 4 + 1, WORK_STRUCT_FLAG_BITS = WORK_STRUCT_COLOR_SHIFT +
      WORK_STRUCT_COLOR_BITS, WORK_OFFQ_FLAG_BASE = WORK_STRUCT_COLOR_SHIFT, __WORK_OFFQ_CANCELING = WORK_OFFQ_FLAG_BASE, WORK_OFFQ_CANCELING = (1 << __WORK_OFFQ_CANCELING), WORK_OFFQ_FLAG_BITS = 1, WORK_OFFQ_POOL_SHIFT = WORK_OFFQ_FLAG_BASE + WORK_OFFQ_FLAG_BITS, WORK_OFFQ_LEFT = 32 - WORK_OFFQ_POOL_SHIFT, WORK_OFFQ_POOL_BITS = WORK_OFFQ_LEFT <= 31 ? WORK_OFFQ_LEFT : 31, WORK_OFFQ_POOL_NONE = (1UL << WORK_OFFQ_POOL_BITS) - 1, WORK_STRUCT_FLAG_MASK = (1UL << WORK_STRUCT_FLAG_BITS) - 1, WORK_STRUCT_WQ_DATA_MASK = ~WORK_STRUCT_FLAG_MASK, WORK_STRUCT_NO_POOL = (unsigned long)WORK_OFFQ_POOL_NONE << WORK_OFFQ_POOL_SHIFT, WORK_BUSY_PENDING = 1 << 0, WORK_BUSY_RUNNING = 1 << 1, WORKER_DESC_LEN = 24, };
struct work_struct {
 atomic_long_t data;
 struct list_head entry;
 work_func_t func;
};
struct delayed_work {
 struct work_struct work;
 struct timer_list timer;
 struct workqueue_struct *wq;
 int cpu;
};
struct workqueue_attrs {
 int nice;
 cpumask_var_t cpumask;
 bool no_numa;
};
struct execute_work {
 struct work_struct work;
};



enum {
 WQ_NON_REENTRANT = 1 << 0, WQ_UNBOUND = 1 << 1, WQ_FREEZABLE = 1 << 2, WQ_MEM_RECLAIM = 1 << 3, WQ_HIGHPRI = 1 << 4, WQ_CPU_INTENSIVE = 1 << 5, WQ_SYSFS = 1 << 6, WQ_POWER_EFFICIENT = 1 << 7, __WQ_DRAINING = 1 << 16, __WQ_ORDERED = 1 << 17, WQ_MAX_ACTIVE = 512, WQ_MAX_UNBOUND_PER_CPU = 4, WQ_DFL_ACTIVE = WQ_MAX_ACTIVE / 2, };
extern struct workqueue_struct *
__alloc_workqueue_key(const char *fmt, unsigned int flags, int max_active, struct lock_class_key *key, const char *lock_name, ...) ;
struct workqueue_attrs *alloc_workqueue_attrs(gfp_t gfp_mask);
void free_workqueue_attrs(struct workqueue_attrs *attrs);
int apply_workqueue_attrs(struct workqueue_struct *wq, const struct workqueue_attrs *attrs);
int execute_in_process_context(work_func_t fn, struct execute_work *);
long work_on_cpu(int cpu, long (*fn)(void *), void *arg);
int workqueue_sysfs_register(struct workqueue_struct *wq);
struct srcu_struct_array {
 unsigned long c[2];
 unsigned long seq[2];
};
struct rcu_batch {
 struct callback_head *head, **tail;
};
struct srcu_struct {
 unsigned completed;
 struct srcu_struct_array *per_cpu_ref;
 spinlock_t queue_lock;
 bool running;
 struct rcu_batch batch_queue;
 struct rcu_batch batch_check0;
 struct rcu_batch batch_check1;
 struct rcu_batch batch_done;
 struct delayed_work work;
};
int init_srcu_struct(struct srcu_struct *sp);
void process_srcu(struct work_struct *work);
void call_srcu(struct srcu_struct *sp, struct callback_head *head, void (*func)(struct callback_head *head));
void cleanup_srcu_struct(struct srcu_struct *sp);
int __srcu_read_lock(struct srcu_struct *sp) ;
void __srcu_read_unlock(struct srcu_struct *sp, int idx) ;
void synchronize_srcu(struct srcu_struct *sp);
void synchronize_srcu_expedited(struct srcu_struct *sp);
long srcu_batches_completed(struct srcu_struct *sp);
void srcu_barrier(struct srcu_struct *sp);
typedef int (*notifier_fn_t)(struct notifier_block *nb, unsigned long action, void *data);
struct notifier_block {
 notifier_fn_t notifier_call;
 struct notifier_block *next;
 int priority;
};
struct atomic_notifier_head {
 spinlock_t lock;
 struct notifier_block *head;
};
struct blocking_notifier_head {
 struct rw_semaphore rwsem;
 struct notifier_block *head;
};
struct raw_notifier_head {
 struct notifier_block *head;
};
struct srcu_notifier_head {
 struct mutex mutex;
 struct srcu_struct srcu;
 struct notifier_block *head;
};
struct page;
struct zone;
struct pglist_data;
struct mem_section;
struct memory_block;









void build_all_zonelists(pg_data_t *pgdat, struct zone *zone);
void wakeup_kswapd(struct zone *zone, int order, enum zone_type classzone_idx);
bool zone_watermark_ok(struct zone *z, unsigned int order, unsigned long mark, int classzone_idx, int alloc_flags);
bool zone_watermark_ok_safe(struct zone *z, unsigned int order, unsigned long mark, int classzone_idx, int alloc_flags);
enum memmap_context {
 MEMMAP_EARLY, MEMMAP_HOTPLUG, };

;
struct ctl_table;
int min_free_kbytes_sysctl_handler(struct ctl_table *, int, void *, size_t *, loff_t *);
int lowmem_reserve_ratio_sysctl_handler(struct ctl_table *, int, void *, size_t *, loff_t *);
int percpu_pagelist_fraction_sysctl_handler(struct ctl_table *, int, void *, size_t *, loff_t *);
int sysctl_min_unmapped_ratio_sysctl_handler(struct ctl_table *, int, void *, size_t *, loff_t *);
int sysctl_min_slab_ratio_sysctl_handler(struct ctl_table *, int, void *, size_t *, loff_t *);
struct zoneref *next_zones_zonelist(struct zoneref *z, enum zone_type highest_zoneidx, nodemask_t *nodes, struct zone **zone);
void memory_present(int nid, unsigned long start, unsigned long end);
unsigned long node_memmap_size_bytes(int, unsigned long, unsigned long);
typedef void (*smp_call_func_t)(void *info);
struct call_single_data {
 struct list_head list;
 smp_call_func_t func;
 void *info;
 u16 flags;
};
int smp_call_function_single(int cpuid, smp_call_func_t func, void *info, int wait);
struct seq_file;
 void do_IPI(int ipinr, struct pt_regs *regs);
void handle_IPI(int ipinr, struct pt_regs *regs);
 void secondary_start_kernel(void);
struct secondary_data {
 unsigned long pgdir;
 unsigned long swapper_pg_dir;
 void *stack;
};
struct smp_operations {
 void (*smp_init_cpus)(void);
 void (*smp_prepare_cpus)(unsigned int max_cpus);
 void (*smp_secondary_init)(unsigned int cpu);
 int (*smp_boot_secondary)(unsigned int cpu, struct task_struct *idle);
 int (*cpu_kill)(unsigned int cpu);
 void (*cpu_die)(unsigned int cpu);
 int (*cpu_disable)(unsigned int cpu);
};
int smp_call_function(smp_call_func_t func, void *info, int wait);
void smp_call_function_many(const struct cpumask *mask, smp_call_func_t func, void *info, bool wait);
void __smp_call_function_single(int cpuid, struct call_single_data *data, int wait);
int smp_call_function_any(const struct cpumask *mask, smp_call_func_t func, void *info, int wait);
void kick_all_cpus_sync(void);
void call_function_init(void);
void generic_smp_call_function_single_interrupt(void);
int on_each_cpu(smp_call_func_t func, void *info, int wait);
void on_each_cpu_mask(const struct cpumask *mask, smp_call_func_t func, void *info, bool wait);
void on_each_cpu_cond(bool (*cond_func)(int cpu, void *info), smp_call_func_t func, void *info, bool wait, gfp_t gfp_flags);
void smp_prepare_boot_cpu(void);
void smp_setup_processor_id(void);
struct pcpu_group_info {
 int nr_units;
 unsigned long base_offset;
 unsigned int *cpu_map;
};
struct pcpu_alloc_info {
 size_t static_size;
 size_t reserved_size;
 size_t dyn_size;
 size_t unit_size;
 size_t atom_size;
 size_t alloc_size;
 size_t __ai_size;
 int nr_groups;
 struct pcpu_group_info groups[];
};
enum pcpu_fc {
 PCPU_FC_AUTO, PCPU_FC_EMBED, PCPU_FC_PAGE, PCPU_FC_NR, };
typedef void * (*pcpu_fc_alloc_fn_t)(unsigned int cpu, size_t size, size_t align);
typedef void (*pcpu_fc_free_fn_t)(void *ptr, size_t size);
typedef void (*pcpu_fc_populate_pte_fn_t)(unsigned long addr);
typedef int (pcpu_fc_cpu_distance_fn_t)(unsigned int from, unsigned int to);
struct cputopo_arm {
 int thread_id;
 int core_id;
 int socket_id;
 cpumask_t thread_sibling;
 cpumask_t core_sibling;
};
void init_cpu_topology(void);
void store_cpu_topology(unsigned int cpuid);
const struct cpumask *cpu_coregroup_mask(int cpu);
int cluster_to_logical_mask(unsigned int socket_id, cpumask_t *cluster_mask);
int arch_update_cpu_topology(void);
struct vm_area_struct;


struct page *
__alloc_pages_nodemask(gfp_t gfp_mask, unsigned int order, struct zonelist *zonelist, nodemask_t *nodemask);
void *alloc_pages_exact(size_t size, gfp_t gfp_mask);
void free_pages_exact(void *virt, size_t size);
void *alloc_pages_exact_nid(int nid, size_t size, gfp_t gfp_mask);
void page_alloc_init(void);
void drain_zone_pages(struct zone *zone, struct per_cpu_pages *pcp);
void drain_all_pages(void);
void drain_local_pages(void *dummy);
bool gfp_pfmemalloc_allowed(gfp_t gfp_mask);
struct mem_cgroup;
void kmem_cache_init(void);
int slab_is_available(void);
struct kmem_cache *kmem_cache_create(const char *, size_t, size_t, unsigned long, void (*)(void *));
struct kmem_cache *
kmem_cache_create_memcg(struct mem_cgroup *, const char *, size_t, size_t, unsigned long, void (*)(void *), struct kmem_cache *);
void kmem_cache_destroy(struct kmem_cache *);
int kmem_cache_shrink(struct kmem_cache *);
void kmem_cache_free(struct kmem_cache *, void *);
void * __krealloc(const void *, size_t, gfp_t);
void * krealloc(const void *, size_t, gfp_t);
void kfree(const void *);
void kzfree(const void *);
size_t ksize(const void *);
struct sock;
struct kobject;
enum kobj_ns_type {
 KOBJ_NS_TYPE_NONE = 0, KOBJ_NS_TYPE_NET, KOBJ_NS_TYPES
};
struct kobj_ns_type_operations {
 enum kobj_ns_type type;
 void *(*grab_current_ns)(void);
 const void *(*netlink_ns)(struct sock *sk);
 const void *(*initial_ns)(void);
 void (*drop_ns)(void *);
};
int kobj_ns_type_register(const struct kobj_ns_type_operations *ops);
int kobj_ns_type_registered(enum kobj_ns_type type);
const struct kobj_ns_type_operations *kobj_child_ns_ops(struct kobject *parent);
const struct kobj_ns_type_operations *kobj_ns_ops(struct kobject *kobj);
void *kobj_ns_grab_current(enum kobj_ns_type type);
const void *kobj_ns_netlink(enum kobj_ns_type type, struct sock *sk);
const void *kobj_ns_initial(enum kobj_ns_type type);
void kobj_ns_drop(enum kobj_ns_type type, void *ns);
struct __old_kernel_stat {
 unsigned short st_dev;
 unsigned short st_ino;
 unsigned short st_mode;
 unsigned short st_nlink;
 unsigned short st_uid;
 unsigned short st_gid;
 unsigned short st_rdev;
 unsigned long st_size;
 unsigned long st_atime;
 unsigned long st_mtime;
 unsigned long st_ctime;
};
struct stat {
 unsigned long st_dev;
 unsigned long st_ino;
 unsigned short st_mode;
 unsigned short st_nlink;
 unsigned short st_uid;
 unsigned short st_gid;
 unsigned long st_rdev;
 unsigned long st_size;
 unsigned long st_blksize;
 unsigned long st_blocks;
 unsigned long st_atime;
 unsigned long st_atime_nsec;
 unsigned long st_mtime;
 unsigned long st_mtime_nsec;
 unsigned long st_ctime;
 unsigned long st_ctime_nsec;
 unsigned long __unused4;
 unsigned long __unused5;
};
struct stat64 {
 unsigned long long st_dev;
 unsigned char __pad0[4];
 unsigned long __st_ino;
 unsigned int st_mode;
 unsigned int st_nlink;
 unsigned long st_uid;
 unsigned long st_gid;
 unsigned long long st_rdev;
 unsigned char __pad3[4];
 long long st_size;
 unsigned long st_blksize;
 unsigned long long st_blocks;
 unsigned long st_atime;
 unsigned long st_atime_nsec;
 unsigned long st_mtime;
 unsigned long st_mtime_nsec;
 unsigned long st_ctime;
 unsigned long st_ctime_nsec;
 unsigned long long st_ino;
};
struct user_namespace;
typedef uid_t kuid_t;
typedef gid_t kgid_t;
struct kstat {
 u64 ino;
 dev_t dev;
 umode_t mode;
 unsigned int nlink;
 kuid_t uid;
 kgid_t gid;
 dev_t rdev;
 loff_t size;
 struct timespec atime;
 struct timespec mtime;
 struct timespec ctime;
 unsigned long blksize;
 unsigned long long blocks;
};
struct kobject;
struct module;
enum kobj_ns_type;
struct attribute {
 const char *name;
 umode_t mode;
};
struct attribute_group {
 const char *name;
 umode_t (*is_visible)(struct kobject *, struct attribute *, int);
 struct attribute **attrs;
};
struct file;
struct vm_area_struct;
struct bin_attribute {
 struct attribute attr;
 size_t size;
 void *private;
 ssize_t (*read)(struct file *, struct kobject *, struct bin_attribute *, char *, loff_t, size_t);
 ssize_t (*write)(struct file *,struct kobject *, struct bin_attribute *, char *, loff_t, size_t);
 int (*mmap)(struct file *, struct kobject *, struct bin_attribute *attr, struct vm_area_struct *vma);
};
struct sysfs_ops {
 ssize_t (*show)(struct kobject *, struct attribute *,char *);
 ssize_t (*store)(struct kobject *,struct attribute *,const char *, size_t);
 const void *(*namespace)(struct kobject *, const struct attribute *);
};
struct sysfs_dirent;
int sysfs_schedule_callback(struct kobject *kobj, void (*func)(void *), void *data, struct module *owner);
int sysfs_create_dir(struct kobject *kobj);
void sysfs_remove_dir(struct kobject *kobj);
int sysfs_rename_dir(struct kobject *kobj, const char *new_name);
int sysfs_move_dir(struct kobject *kobj, struct kobject *new_parent_kobj);
int sysfs_create_file(struct kobject *kobj, const struct attribute *attr);
int sysfs_create_files(struct kobject *kobj, const struct attribute **attr);
int sysfs_chmod_file(struct kobject *kobj, const struct attribute *attr, umode_t mode);
void sysfs_remove_file(struct kobject *kobj, const struct attribute *attr);
void sysfs_remove_files(struct kobject *kobj, const struct attribute **attr);
int sysfs_create_bin_file(struct kobject *kobj, const struct bin_attribute *attr);
void sysfs_remove_bin_file(struct kobject *kobj, const struct bin_attribute *attr);
int sysfs_create_link(struct kobject *kobj, struct kobject *target, const char *name);
int sysfs_create_link_nowarn(struct kobject *kobj, struct kobject *target, const char *name);
void sysfs_remove_link(struct kobject *kobj, const char *name);
int sysfs_rename_link(struct kobject *kobj, struct kobject *target, const char *old_name, const char *new_name);
void sysfs_delete_link(struct kobject *dir, struct kobject *targ, const char *name);
int sysfs_create_group(struct kobject *kobj, const struct attribute_group *grp);
int sysfs_create_groups(struct kobject *kobj, const struct attribute_group **groups);
int sysfs_update_group(struct kobject *kobj, const struct attribute_group *grp);
void sysfs_remove_group(struct kobject *kobj, const struct attribute_group *grp);
void sysfs_remove_groups(struct kobject *kobj, const struct attribute_group **groups);
int sysfs_add_file_to_group(struct kobject *kobj, const struct attribute *attr, const char *group);
void sysfs_remove_file_from_group(struct kobject *kobj, const struct attribute *attr, const char *group);
int sysfs_merge_group(struct kobject *kobj, const struct attribute_group *grp);
void sysfs_unmerge_group(struct kobject *kobj, const struct attribute_group *grp);
int sysfs_add_link_to_group(struct kobject *kobj, const char *group_name, struct kobject *target, const char *link_name);
void sysfs_remove_link_from_group(struct kobject *kobj, const char *group_name, const char *link_name);
void sysfs_notify(struct kobject *kobj, const char *dir, const char *attr);
void sysfs_notify_dirent(struct sysfs_dirent *sd);
struct sysfs_dirent *sysfs_get_dirent(struct sysfs_dirent *parent_sd, const void *ns, const unsigned char *name);
struct sysfs_dirent *sysfs_get(struct sysfs_dirent *sd);
void sysfs_put(struct sysfs_dirent *sd);
int sysfs_init(void);
struct kref {
 atomic_t refcount;
};
enum kobject_action {
 KOBJ_ADD, KOBJ_REMOVE, KOBJ_CHANGE, KOBJ_MOVE, KOBJ_ONLINE, KOBJ_OFFLINE, KOBJ_MAX
};
struct kobject {
 const char *name;
 struct list_head entry;
 struct kobject *parent;
 struct kset *kset;
 struct kobj_type *ktype;
 struct sysfs_dirent *sd;
 struct kref kref;
 unsigned int state_initialized:1;
 unsigned int state_in_sysfs:1;
 unsigned int state_add_uevent_sent:1;
 unsigned int state_remove_uevent_sent:1;
 unsigned int uevent_suppress:1;
};
extern
int kobject_set_name(struct kobject *kobj, const char *name, ...);
extern
int kobject_add(struct kobject *kobj, struct kobject *parent, const char *fmt, ...);
extern
int kobject_init_and_add(struct kobject *kobj, struct kobj_type *ktype, struct kobject *parent, const char *fmt, ...);
struct kobj_type {
 void (*release)(struct kobject *kobj);
 const struct sysfs_ops *sysfs_ops;
 struct attribute **default_attrs;
 const struct kobj_ns_type_operations *(*child_ns_type)(struct kobject *kobj);
 const void *(*namespace)(struct kobject *kobj);
};
struct kobj_uevent_env {
 char *envp[32];
 int envp_idx;
 char buf[2048];
 int buflen;
};
struct kset_uevent_ops {
 int (* const filter)(struct kset *kset, struct kobject *kobj);
 const char *(* const name)(struct kset *kset, struct kobject *kobj);
 int (* const uevent)(struct kset *kset, struct kobject *kobj, struct kobj_uevent_env *env);
};
struct kobj_attribute {
 struct attribute attr;
 ssize_t (*show)(struct kobject *kobj, struct kobj_attribute *attr, char *buf);
 ssize_t (*store)(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count);
};
struct sock;
struct kset {
 struct list_head list;
 spinlock_t list_lock;
 struct kobject kobj;
 const struct kset_uevent_ops *uevent_ops;
};
int kobject_uevent(struct kobject *kobj, enum kobject_action action);
int kobject_uevent_env(struct kobject *kobj, enum kobject_action action, char *envp[]);

int add_uevent_var(struct kobj_uevent_env *env, const char *format, ...);
int kobject_action_type(const char *buf, size_t count, enum kobject_action *type);
enum stat_item {
 ALLOC_FASTPATH, ALLOC_SLOWPATH, FREE_FASTPATH, FREE_SLOWPATH, FREE_FROZEN, FREE_ADD_PARTIAL, FREE_REMOVE_PARTIAL, ALLOC_FROM_PARTIAL, ALLOC_SLAB, ALLOC_REFILL, ALLOC_NODE_MISMATCH, FREE_SLAB, CPUSLAB_FLUSH, DEACTIVATE_FULL, DEACTIVATE_EMPTY, DEACTIVATE_TO_HEAD, DEACTIVATE_TO_TAIL, DEACTIVATE_REMOTE_FREES, DEACTIVATE_BYPASS, ORDER_FALLBACK, CMPXCHG_DOUBLE_CPU_FAIL, CMPXCHG_DOUBLE_FAIL, CPU_PARTIAL_ALLOC, CPU_PARTIAL_FREE, CPU_PARTIAL_NODE, CPU_PARTIAL_DRAIN, NR_SLUB_STAT_ITEMS };
struct kmem_cache_cpu {
 void **freelist;
 unsigned long tid;
 struct page *page;
 struct page *partial;
};
struct kmem_cache_order_objects {
 unsigned long x;
};
struct kmem_cache {
 struct kmem_cache_cpu *cpu_slab;
 unsigned long flags;
 unsigned long min_partial;
 int size;
 int object_size;
 int offset;
 int cpu_partial;
 struct kmem_cache_order_objects oo;
 struct kmem_cache_order_objects max;
 struct kmem_cache_order_objects min;
 gfp_t allocflags;
 int refcount;
 void (*ctor)(void *);
 int inuse;
 int align;
 int reserved;
 const char *name;
 struct list_head list;
 struct kobject kobj;
 struct kmem_cache_node *node[(1 << 0)];
};
void *kmem_cache_alloc(struct kmem_cache *, gfp_t);
void *__kmalloc(size_t size, gfp_t flags);
extern void *
kmem_cache_alloc_trace(struct kmem_cache *s, gfp_t gfpflags, size_t size);
struct memcg_cache_params {
 bool is_root_cache;
 union {
  struct kmem_cache *memcg_caches[0];
  struct {
   struct mem_cgroup *memcg;
   struct list_head list;
   struct kmem_cache *root_cache;
   bool dead;
   atomic_t nr_pages;
   struct work_struct destroy;
  };
 };
};
int memcg_update_all_caches(int num_memcgs);
struct seq_file;
int cache_show(struct kmem_cache *s, struct seq_file *m);
void print_slabinfo_header(struct seq_file *m);
void *kmem_cache_alloc(struct kmem_cache *, gfp_t);
void kmem_cache_init_late(void);
struct rb_node {
 unsigned long __rb_parent_color;
 struct rb_node *rb_right;
 struct rb_node *rb_left;
} ;
struct rb_root {
 struct rb_node *rb_node;
};
struct completion;
struct __sysctl_args {
 int *name;
 int nlen;
 void *oldval;
 size_t *oldlenp;
 void *newval;
 size_t newlen;
 unsigned long __unused[4];
};
enum
{
 CTL_KERN=1, CTL_VM=2, CTL_NET=3, CTL_PROC=4, CTL_FS=5, CTL_DEBUG=6, CTL_DEV=7, CTL_BUS=8, CTL_ABI=9, CTL_CPU=10, CTL_ARLAN=254, CTL_S390DBF=5677, CTL_SUNRPC=7249, CTL_PM=9899, CTL_FRV=9898, };
enum
{
 CTL_BUS_ISA=1
};
enum
{
 INOTIFY_MAX_USER_INSTANCES=1, INOTIFY_MAX_USER_WATCHES=2, INOTIFY_MAX_QUEUED_EVENTS=3
};
enum
{
 KERN_OSTYPE=1, KERN_OSRELEASE=2, KERN_OSREV=3, KERN_VERSION=4, KERN_SECUREMASK=5, KERN_PROF=6, KERN_NODENAME=7, KERN_DOMAINNAME=8, KERN_PANIC=15, KERN_REALROOTDEV=16, KERN_SPARC_REBOOT=21, KERN_CTLALTDEL=22, KERN_PRINTK=23, KERN_NAMETRANS=24, KERN_PPC_HTABRECLAIM=25, KERN_PPC_ZEROPAGED=26, KERN_PPC_POWERSAVE_NAP=27, KERN_MODPROBE=28, KERN_SG_BIG_BUFF=29, KERN_ACCT=30, KERN_PPC_L2CR=31, KERN_RTSIGNR=32, KERN_RTSIGMAX=33, KERN_SHMMAX=34, KERN_MSGMAX=35, KERN_MSGMNB=36, KERN_MSGPOOL=37, KERN_SYSRQ=38, KERN_MAX_THREADS=39, KERN_RANDOM=40, KERN_SHMALL=41, KERN_MSGMNI=42, KERN_SEM=43, KERN_SPARC_STOP_A=44, KERN_SHMMNI=45, KERN_OVERFLOWUID=46, KERN_OVERFLOWGID=47, KERN_SHMPATH=48, KERN_HOTPLUG=49, KERN_IEEE_EMULATION_WARNINGS=50, KERN_S390_USER_DEBUG_LOGGING=51, KERN_CORE_USES_PID=52, KERN_TAINTED=53, KERN_CADPID=54, KERN_PIDMAX=55, KERN_CORE_PATTERN=56, KERN_PANIC_ON_OOPS=57, KERN_HPPA_PWRSW=58, KERN_HPPA_UNALIGNED=59, KERN_PRINTK_RATELIMIT=60, KERN_PRINTK_RATELIMIT_BURST=61, KERN_PTY=62, KERN_NGROUPS_MAX=63, KERN_SPARC_SCONS_PWROFF=64, KERN_HZ_TIMER=65, KERN_UNKNOWN_NMI_PANIC=66, KERN_BOOTLOADER_TYPE=67, KERN_RANDOMIZE=68, KERN_SETUID_DUMPABLE=69, KERN_SPIN_RETRY=70, KERN_ACPI_VIDEO_FLAGS=71, KERN_IA64_UNALIGNED=72, KERN_COMPAT_LOG=73, KERN_MAX_LOCK_DEPTH=74, KERN_NMI_WATCHDOG=75, KERN_PANIC_ON_NMI=76, };
enum
{
 VM_UNUSED1=1, VM_UNUSED2=2, VM_UNUSED3=3, VM_UNUSED4=4, VM_OVERCOMMIT_MEMORY=5, VM_UNUSED5=6, VM_UNUSED7=7, VM_UNUSED8=8, VM_UNUSED9=9, VM_PAGE_CLUSTER=10, VM_DIRTY_BACKGROUND=11, VM_DIRTY_RATIO=12, VM_DIRTY_WB_CS=13, VM_DIRTY_EXPIRE_CS=14, VM_NR_PDFLUSH_THREADS=15, VM_OVERCOMMIT_RATIO=16, VM_PAGEBUF=17, VM_HUGETLB_PAGES=18, VM_SWAPPINESS=19, VM_LOWMEM_RESERVE_RATIO=20, VM_MIN_FREE_KBYTES=21, VM_MAX_MAP_COUNT=22, VM_LAPTOP_MODE=23, VM_BLOCK_DUMP=24, VM_HUGETLB_GROUP=25, VM_VFS_CACHE_PRESSURE=26, VM_LEGACY_VA_LAYOUT=27, VM_SWAP_TOKEN_TIMEOUT=28, VM_DROP_PAGECACHE=29, VM_PERCPU_PAGELIST_FRACTION=30, VM_ZONE_RECLAIM_MODE=31, VM_MIN_UNMAPPED=32, VM_PANIC_ON_OOM=33, VM_VDSO_ENABLED=34, VM_MIN_SLAB=35, };
enum
{
 NET_CORE=1, NET_ETHER=2, NET_802=3, NET_UNIX=4, NET_IPV4=5, NET_IPX=6, NET_ATALK=7, NET_NETROM=8, NET_AX25=9, NET_BRIDGE=10, NET_ROSE=11, NET_IPV6=12, NET_X25=13, NET_TR=14, NET_DECNET=15, NET_ECONET=16, NET_SCTP=17, NET_LLC=18, NET_NETFILTER=19, NET_DCCP=20, NET_IRDA=412, };
enum
{
 RANDOM_POOLSIZE=1, RANDOM_ENTROPY_COUNT=2, RANDOM_READ_THRESH=3, RANDOM_WRITE_THRESH=4, RANDOM_BOOT_ID=5, RANDOM_UUID=6
};
enum
{
 PTY_MAX=1, PTY_NR=2
};
enum
{
 BUS_ISA_MEM_BASE=1, BUS_ISA_PORT_BASE=2, BUS_ISA_PORT_SHIFT=3
};
enum
{
 NET_CORE_WMEM_MAX=1, NET_CORE_RMEM_MAX=2, NET_CORE_WMEM_DEFAULT=3, NET_CORE_RMEM_DEFAULT=4, NET_CORE_MAX_BACKLOG=6, NET_CORE_FASTROUTE=7, NET_CORE_MSG_COST=8, NET_CORE_MSG_BURST=9, NET_CORE_OPTMEM_MAX=10, NET_CORE_HOT_LIST_LENGTH=11, NET_CORE_DIVERT_VERSION=12, NET_CORE_NO_CONG_THRESH=13, NET_CORE_NO_CONG=14, NET_CORE_LO_CONG=15, NET_CORE_MOD_CONG=16, NET_CORE_DEV_WEIGHT=17, NET_CORE_SOMAXCONN=18, NET_CORE_BUDGET=19, NET_CORE_AEVENT_ETIME=20, NET_CORE_AEVENT_RSEQTH=21, NET_CORE_WARNINGS=22, };
enum
{
 NET_UNIX_DESTROY_DELAY=1, NET_UNIX_DELETE_DELAY=2, NET_UNIX_MAX_DGRAM_QLEN=3, };
enum
{
 NET_NF_CONNTRACK_MAX=1, NET_NF_CONNTRACK_TCP_TIMEOUT_SYN_SENT=2, NET_NF_CONNTRACK_TCP_TIMEOUT_SYN_RECV=3, NET_NF_CONNTRACK_TCP_TIMEOUT_ESTABLISHED=4, NET_NF_CONNTRACK_TCP_TIMEOUT_FIN_WAIT=5, NET_NF_CONNTRACK_TCP_TIMEOUT_CLOSE_WAIT=6, NET_NF_CONNTRACK_TCP_TIMEOUT_LAST_ACK=7, NET_NF_CONNTRACK_TCP_TIMEOUT_TIME_WAIT=8, NET_NF_CONNTRACK_TCP_TIMEOUT_CLOSE=9, NET_NF_CONNTRACK_UDP_TIMEOUT=10, NET_NF_CONNTRACK_UDP_TIMEOUT_STREAM=11, NET_NF_CONNTRACK_ICMP_TIMEOUT=12, NET_NF_CONNTRACK_GENERIC_TIMEOUT=13, NET_NF_CONNTRACK_BUCKETS=14, NET_NF_CONNTRACK_LOG_INVALID=15, NET_NF_CONNTRACK_TCP_TIMEOUT_MAX_RETRANS=16, NET_NF_CONNTRACK_TCP_LOOSE=17, NET_NF_CONNTRACK_TCP_BE_LIBERAL=18, NET_NF_CONNTRACK_TCP_MAX_RETRANS=19, NET_NF_CONNTRACK_SCTP_TIMEOUT_CLOSED=20, NET_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_WAIT=21, NET_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_ECHOED=22, NET_NF_CONNTRACK_SCTP_TIMEOUT_ESTABLISHED=23, NET_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_SENT=24, NET_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_RECD=25, NET_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_ACK_SENT=26, NET_NF_CONNTRACK_COUNT=27, NET_NF_CONNTRACK_ICMPV6_TIMEOUT=28, NET_NF_CONNTRACK_FRAG6_TIMEOUT=29, NET_NF_CONNTRACK_FRAG6_LOW_THRESH=30, NET_NF_CONNTRACK_FRAG6_HIGH_THRESH=31, NET_NF_CONNTRACK_CHECKSUM=32, };
enum
{
 NET_IPV4_FORWARD=8, NET_IPV4_DYNADDR=9, NET_IPV4_CONF=16, NET_IPV4_NEIGH=17, NET_IPV4_ROUTE=18, NET_IPV4_FIB_HASH=19, NET_IPV4_NETFILTER=20, NET_IPV4_TCP_TIMESTAMPS=33, NET_IPV4_TCP_WINDOW_SCALING=34, NET_IPV4_TCP_SACK=35, NET_IPV4_TCP_RETRANS_COLLAPSE=36, NET_IPV4_DEFAULT_TTL=37, NET_IPV4_AUTOCONFIG=38, NET_IPV4_NO_PMTU_DISC=39, NET_IPV4_TCP_SYN_RETRIES=40, NET_IPV4_IPFRAG_HIGH_THRESH=41, NET_IPV4_IPFRAG_LOW_THRESH=42, NET_IPV4_IPFRAG_TIME=43, NET_IPV4_TCP_MAX_KA_PROBES=44, NET_IPV4_TCP_KEEPALIVE_TIME=45, NET_IPV4_TCP_KEEPALIVE_PROBES=46, NET_IPV4_TCP_RETRIES1=47, NET_IPV4_TCP_RETRIES2=48, NET_IPV4_TCP_FIN_TIMEOUT=49, NET_IPV4_IP_MASQ_DEBUG=50, NET_TCP_SYNCOOKIES=51, NET_TCP_STDURG=52, NET_TCP_RFC1337=53, NET_TCP_SYN_TAILDROP=54, NET_TCP_MAX_SYN_BACKLOG=55, NET_IPV4_LOCAL_PORT_RANGE=56, NET_IPV4_ICMP_ECHO_IGNORE_ALL=57, NET_IPV4_ICMP_ECHO_IGNORE_BROADCASTS=58, NET_IPV4_ICMP_SOURCEQUENCH_RATE=59, NET_IPV4_ICMP_DESTUNREACH_RATE=60, NET_IPV4_ICMP_TIMEEXCEED_RATE=61, NET_IPV4_ICMP_PARAMPROB_RATE=62, NET_IPV4_ICMP_ECHOREPLY_RATE=63, NET_IPV4_ICMP_IGNORE_BOGUS_ERROR_RESPONSES=64, NET_IPV4_IGMP_MAX_MEMBERSHIPS=65, NET_TCP_TW_RECYCLE=66, NET_IPV4_ALWAYS_DEFRAG=67, NET_IPV4_TCP_KEEPALIVE_INTVL=68, NET_IPV4_INET_PEER_THRESHOLD=69, NET_IPV4_INET_PEER_MINTTL=70, NET_IPV4_INET_PEER_MAXTTL=71, NET_IPV4_INET_PEER_GC_MINTIME=72, NET_IPV4_INET_PEER_GC_MAXTIME=73, NET_TCP_ORPHAN_RETRIES=74, NET_TCP_ABORT_ON_OVERFLOW=75, NET_TCP_SYNACK_RETRIES=76, NET_TCP_MAX_ORPHANS=77, NET_TCP_MAX_TW_BUCKETS=78, NET_TCP_FACK=79, NET_TCP_REORDERING=80, NET_TCP_ECN=81, NET_TCP_DSACK=82, NET_TCP_MEM=83, NET_TCP_WMEM=84, NET_TCP_RMEM=85, NET_TCP_APP_WIN=86, NET_TCP_ADV_WIN_SCALE=87, NET_IPV4_NONLOCAL_BIND=88, NET_IPV4_ICMP_RATELIMIT=89, NET_IPV4_ICMP_RATEMASK=90, NET_TCP_TW_REUSE=91, NET_TCP_FRTO=92, NET_TCP_LOW_LATENCY=93, NET_IPV4_IPFRAG_SECRET_INTERVAL=94, NET_IPV4_IGMP_MAX_MSF=96, NET_TCP_NO_METRICS_SAVE=97, NET_TCP_DEFAULT_WIN_SCALE=105, NET_TCP_MODERATE_RCVBUF=106, NET_TCP_TSO_WIN_DIVISOR=107, NET_TCP_BIC_BETA=108, NET_IPV4_ICMP_ERRORS_USE_INBOUND_IFADDR=109, NET_TCP_CONG_CONTROL=110, NET_TCP_ABC=111, NET_IPV4_IPFRAG_MAX_DIST=112, NET_TCP_MTU_PROBING=113, NET_TCP_BASE_MSS=114, NET_IPV4_TCP_WORKAROUND_SIGNED_WINDOWS=115, NET_TCP_DMA_COPYBREAK=116, NET_TCP_SLOW_START_AFTER_IDLE=117, NET_CIPSOV4_CACHE_ENABLE=118, NET_CIPSOV4_CACHE_BUCKET_SIZE=119, NET_CIPSOV4_RBM_OPTFMT=120, NET_CIPSOV4_RBM_STRICTVALID=121, NET_TCP_AVAIL_CONG_CONTROL=122, NET_TCP_ALLOWED_CONG_CONTROL=123, NET_TCP_MAX_SSTHRESH=124, NET_TCP_FRTO_RESPONSE=125, };
enum {
 NET_IPV4_ROUTE_FLUSH=1, NET_IPV4_ROUTE_MIN_DELAY=2, NET_IPV4_ROUTE_MAX_DELAY=3, NET_IPV4_ROUTE_GC_THRESH=4, NET_IPV4_ROUTE_MAX_SIZE=5, NET_IPV4_ROUTE_GC_MIN_INTERVAL=6, NET_IPV4_ROUTE_GC_TIMEOUT=7, NET_IPV4_ROUTE_GC_INTERVAL=8, NET_IPV4_ROUTE_REDIRECT_LOAD=9, NET_IPV4_ROUTE_REDIRECT_NUMBER=10, NET_IPV4_ROUTE_REDIRECT_SILENCE=11, NET_IPV4_ROUTE_ERROR_COST=12, NET_IPV4_ROUTE_ERROR_BURST=13, NET_IPV4_ROUTE_GC_ELASTICITY=14, NET_IPV4_ROUTE_MTU_EXPIRES=15, NET_IPV4_ROUTE_MIN_PMTU=16, NET_IPV4_ROUTE_MIN_ADVMSS=17, NET_IPV4_ROUTE_SECRET_INTERVAL=18, NET_IPV4_ROUTE_GC_MIN_INTERVAL_MS=19, };
enum
{
 NET_PROTO_CONF_ALL=-2, NET_PROTO_CONF_DEFAULT=-3
};
enum
{
 NET_IPV4_CONF_FORWARDING=1, NET_IPV4_CONF_MC_FORWARDING=2, NET_IPV4_CONF_PROXY_ARP=3, NET_IPV4_CONF_ACCEPT_REDIRECTS=4, NET_IPV4_CONF_SECURE_REDIRECTS=5, NET_IPV4_CONF_SEND_REDIRECTS=6, NET_IPV4_CONF_SHARED_MEDIA=7, NET_IPV4_CONF_RP_FILTER=8, NET_IPV4_CONF_ACCEPT_SOURCE_ROUTE=9, NET_IPV4_CONF_BOOTP_RELAY=10, NET_IPV4_CONF_LOG_MARTIANS=11, NET_IPV4_CONF_TAG=12, NET_IPV4_CONF_ARPFILTER=13, NET_IPV4_CONF_MEDIUM_ID=14, NET_IPV4_CONF_NOXFRM=15, NET_IPV4_CONF_NOPOLICY=16, NET_IPV4_CONF_FORCE_IGMP_VERSION=17, NET_IPV4_CONF_ARP_ANNOUNCE=18, NET_IPV4_CONF_ARP_IGNORE=19, NET_IPV4_CONF_PROMOTE_SECONDARIES=20, NET_IPV4_CONF_ARP_ACCEPT=21, NET_IPV4_CONF_ARP_NOTIFY=22, };
enum
{
 NET_IPV4_NF_CONNTRACK_MAX=1, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_SYN_SENT=2, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_SYN_RECV=3, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_ESTABLISHED=4, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_FIN_WAIT=5, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_CLOSE_WAIT=6, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_LAST_ACK=7, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_TIME_WAIT=8, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_CLOSE=9, NET_IPV4_NF_CONNTRACK_UDP_TIMEOUT=10, NET_IPV4_NF_CONNTRACK_UDP_TIMEOUT_STREAM=11, NET_IPV4_NF_CONNTRACK_ICMP_TIMEOUT=12, NET_IPV4_NF_CONNTRACK_GENERIC_TIMEOUT=13, NET_IPV4_NF_CONNTRACK_BUCKETS=14, NET_IPV4_NF_CONNTRACK_LOG_INVALID=15, NET_IPV4_NF_CONNTRACK_TCP_TIMEOUT_MAX_RETRANS=16, NET_IPV4_NF_CONNTRACK_TCP_LOOSE=17, NET_IPV4_NF_CONNTRACK_TCP_BE_LIBERAL=18, NET_IPV4_NF_CONNTRACK_TCP_MAX_RETRANS=19, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_CLOSED=20, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_WAIT=21, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_COOKIE_ECHOED=22, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_ESTABLISHED=23, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_SENT=24, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_RECD=25, NET_IPV4_NF_CONNTRACK_SCTP_TIMEOUT_SHUTDOWN_ACK_SENT=26, NET_IPV4_NF_CONNTRACK_COUNT=27, NET_IPV4_NF_CONNTRACK_CHECKSUM=28, };
enum {
 NET_IPV6_CONF=16, NET_IPV6_NEIGH=17, NET_IPV6_ROUTE=18, NET_IPV6_ICMP=19, NET_IPV6_BINDV6ONLY=20, NET_IPV6_IP6FRAG_HIGH_THRESH=21, NET_IPV6_IP6FRAG_LOW_THRESH=22, NET_IPV6_IP6FRAG_TIME=23, NET_IPV6_IP6FRAG_SECRET_INTERVAL=24, NET_IPV6_MLD_MAX_MSF=25, };
enum {
 NET_IPV6_ROUTE_FLUSH=1, NET_IPV6_ROUTE_GC_THRESH=2, NET_IPV6_ROUTE_MAX_SIZE=3, NET_IPV6_ROUTE_GC_MIN_INTERVAL=4, NET_IPV6_ROUTE_GC_TIMEOUT=5, NET_IPV6_ROUTE_GC_INTERVAL=6, NET_IPV6_ROUTE_GC_ELASTICITY=7, NET_IPV6_ROUTE_MTU_EXPIRES=8, NET_IPV6_ROUTE_MIN_ADVMSS=9, NET_IPV6_ROUTE_GC_MIN_INTERVAL_MS=10
};
enum {
 NET_IPV6_FORWARDING=1, NET_IPV6_HOP_LIMIT=2, NET_IPV6_MTU=3, NET_IPV6_ACCEPT_RA=4, NET_IPV6_ACCEPT_REDIRECTS=5, NET_IPV6_AUTOCONF=6, NET_IPV6_DAD_TRANSMITS=7, NET_IPV6_RTR_SOLICITS=8, NET_IPV6_RTR_SOLICIT_INTERVAL=9, NET_IPV6_RTR_SOLICIT_DELAY=10, NET_IPV6_USE_TEMPADDR=11, NET_IPV6_TEMP_VALID_LFT=12, NET_IPV6_TEMP_PREFERED_LFT=13, NET_IPV6_REGEN_MAX_RETRY=14, NET_IPV6_MAX_DESYNC_FACTOR=15, NET_IPV6_MAX_ADDRESSES=16, NET_IPV6_FORCE_MLD_VERSION=17, NET_IPV6_ACCEPT_RA_DEFRTR=18, NET_IPV6_ACCEPT_RA_PINFO=19, NET_IPV6_ACCEPT_RA_RTR_PREF=20, NET_IPV6_RTR_PROBE_INTERVAL=21, NET_IPV6_ACCEPT_RA_RT_INFO_MAX_PLEN=22, NET_IPV6_PROXY_NDP=23, NET_IPV6_ACCEPT_SOURCE_ROUTE=25, __NET_IPV6_MAX
};
enum {
 NET_IPV6_ICMP_RATELIMIT=1
};
enum {
 NET_NEIGH_MCAST_SOLICIT=1, NET_NEIGH_UCAST_SOLICIT=2, NET_NEIGH_APP_SOLICIT=3, NET_NEIGH_RETRANS_TIME=4, NET_NEIGH_REACHABLE_TIME=5, NET_NEIGH_DELAY_PROBE_TIME=6, NET_NEIGH_GC_STALE_TIME=7, NET_NEIGH_UNRES_QLEN=8, NET_NEIGH_PROXY_QLEN=9, NET_NEIGH_ANYCAST_DELAY=10, NET_NEIGH_PROXY_DELAY=11, NET_NEIGH_LOCKTIME=12, NET_NEIGH_GC_INTERVAL=13, NET_NEIGH_GC_THRESH1=14, NET_NEIGH_GC_THRESH2=15, NET_NEIGH_GC_THRESH3=16, NET_NEIGH_RETRANS_TIME_MS=17, NET_NEIGH_REACHABLE_TIME_MS=18, };
enum {
 NET_DCCP_DEFAULT=1, };
enum {
 NET_IPX_PPROP_BROADCASTING=1, NET_IPX_FORWARDING=2
};
enum {
 NET_LLC2=1, NET_LLC_STATION=2, };
enum {
 NET_LLC2_TIMEOUT=1, };
enum {
 NET_LLC_STATION_ACK_TIMEOUT=1, };
enum {
 NET_LLC2_ACK_TIMEOUT=1, NET_LLC2_P_TIMEOUT=2, NET_LLC2_REJ_TIMEOUT=3, NET_LLC2_BUSY_TIMEOUT=4, };
enum {
 NET_ATALK_AARP_EXPIRY_TIME=1, NET_ATALK_AARP_TICK_TIME=2, NET_ATALK_AARP_RETRANSMIT_LIMIT=3, NET_ATALK_AARP_RESOLVE_TIME=4
};
enum {
 NET_NETROM_DEFAULT_PATH_QUALITY=1, NET_NETROM_OBSOLESCENCE_COUNT_INITIALISER=2, NET_NETROM_NETWORK_TTL_INITIALISER=3, NET_NETROM_TRANSPORT_TIMEOUT=4, NET_NETROM_TRANSPORT_MAXIMUM_TRIES=5, NET_NETROM_TRANSPORT_ACKNOWLEDGE_DELAY=6, NET_NETROM_TRANSPORT_BUSY_DELAY=7, NET_NETROM_TRANSPORT_REQUESTED_WINDOW_SIZE=8, NET_NETROM_TRANSPORT_NO_ACTIVITY_TIMEOUT=9, NET_NETROM_ROUTING_CONTROL=10, NET_NETROM_LINK_FAILS_COUNT=11, NET_NETROM_RESET=12
};
enum {
 NET_AX25_IP_DEFAULT_MODE=1, NET_AX25_DEFAULT_MODE=2, NET_AX25_BACKOFF_TYPE=3, NET_AX25_CONNECT_MODE=4, NET_AX25_STANDARD_WINDOW=5, NET_AX25_EXTENDED_WINDOW=6, NET_AX25_T1_TIMEOUT=7, NET_AX25_T2_TIMEOUT=8, NET_AX25_T3_TIMEOUT=9, NET_AX25_IDLE_TIMEOUT=10, NET_AX25_N2=11, NET_AX25_PACLEN=12, NET_AX25_PROTOCOL=13, NET_AX25_DAMA_SLAVE_TIMEOUT=14
};
enum {
 NET_ROSE_RESTART_REQUEST_TIMEOUT=1, NET_ROSE_CALL_REQUEST_TIMEOUT=2, NET_ROSE_RESET_REQUEST_TIMEOUT=3, NET_ROSE_CLEAR_REQUEST_TIMEOUT=4, NET_ROSE_ACK_HOLD_BACK_TIMEOUT=5, NET_ROSE_ROUTING_CONTROL=6, NET_ROSE_LINK_FAIL_TIMEOUT=7, NET_ROSE_MAX_VCS=8, NET_ROSE_WINDOW_SIZE=9, NET_ROSE_NO_ACTIVITY_TIMEOUT=10
};
enum {
 NET_X25_RESTART_REQUEST_TIMEOUT=1, NET_X25_CALL_REQUEST_TIMEOUT=2, NET_X25_RESET_REQUEST_TIMEOUT=3, NET_X25_CLEAR_REQUEST_TIMEOUT=4, NET_X25_ACK_HOLD_BACK_TIMEOUT=5, NET_X25_FORWARD=6
};
enum
{
 NET_TR_RIF_TIMEOUT=1
};
enum {
 NET_DECNET_NODE_TYPE = 1, NET_DECNET_NODE_ADDRESS = 2, NET_DECNET_NODE_NAME = 3, NET_DECNET_DEFAULT_DEVICE = 4, NET_DECNET_TIME_WAIT = 5, NET_DECNET_DN_COUNT = 6, NET_DECNET_DI_COUNT = 7, NET_DECNET_DR_COUNT = 8, NET_DECNET_DST_GC_INTERVAL = 9, NET_DECNET_CONF = 10, NET_DECNET_NO_FC_MAX_CWND = 11, NET_DECNET_MEM = 12, NET_DECNET_RMEM = 13, NET_DECNET_WMEM = 14, NET_DECNET_DEBUG_LEVEL = 255
};
enum {
 NET_DECNET_CONF_LOOPBACK = -2, NET_DECNET_CONF_DDCMP = -3, NET_DECNET_CONF_PPP = -4, NET_DECNET_CONF_X25 = -5, NET_DECNET_CONF_GRE = -6, NET_DECNET_CONF_ETHER = -7
};
enum {
 NET_DECNET_CONF_DEV_PRIORITY = 1, NET_DECNET_CONF_DEV_T1 = 2, NET_DECNET_CONF_DEV_T2 = 3, NET_DECNET_CONF_DEV_T3 = 4, NET_DECNET_CONF_DEV_FORWARDING = 5, NET_DECNET_CONF_DEV_BLKSIZE = 6, NET_DECNET_CONF_DEV_STATE = 7
};
enum {
 NET_SCTP_RTO_INITIAL = 1, NET_SCTP_RTO_MIN = 2, NET_SCTP_RTO_MAX = 3, NET_SCTP_RTO_ALPHA = 4, NET_SCTP_RTO_BETA = 5, NET_SCTP_VALID_COOKIE_LIFE = 6, NET_SCTP_ASSOCIATION_MAX_RETRANS = 7, NET_SCTP_PATH_MAX_RETRANS = 8, NET_SCTP_MAX_INIT_RETRANSMITS = 9, NET_SCTP_HB_INTERVAL = 10, NET_SCTP_PRESERVE_ENABLE = 11, NET_SCTP_MAX_BURST = 12, NET_SCTP_ADDIP_ENABLE = 13, NET_SCTP_PRSCTP_ENABLE = 14, NET_SCTP_SNDBUF_POLICY = 15, NET_SCTP_SACK_TIMEOUT = 16, NET_SCTP_RCVBUF_POLICY = 17, };
enum {
 NET_BRIDGE_NF_CALL_ARPTABLES = 1, NET_BRIDGE_NF_CALL_IPTABLES = 2, NET_BRIDGE_NF_CALL_IP6TABLES = 3, NET_BRIDGE_NF_FILTER_VLAN_TAGGED = 4, NET_BRIDGE_NF_FILTER_PPPOE_TAGGED = 5, };
enum {
 NET_IRDA_DISCOVERY=1, NET_IRDA_DEVNAME=2, NET_IRDA_DEBUG=3, NET_IRDA_FAST_POLL=4, NET_IRDA_DISCOVERY_SLOTS=5, NET_IRDA_DISCOVERY_TIMEOUT=6, NET_IRDA_SLOT_TIMEOUT=7, NET_IRDA_MAX_BAUD_RATE=8, NET_IRDA_MIN_TX_TURN_TIME=9, NET_IRDA_MAX_TX_DATA_SIZE=10, NET_IRDA_MAX_TX_WINDOW=11, NET_IRDA_MAX_NOREPLY_TIME=12, NET_IRDA_WARN_NOREPLY_TIME=13, NET_IRDA_LAP_KEEPALIVE_TIME=14, };
enum
{
 FS_NRINODE=1, FS_STATINODE=2, FS_MAXINODE=3, FS_NRDQUOT=4, FS_MAXDQUOT=5, FS_NRFILE=6, FS_MAXFILE=7, FS_DENTRY=8, FS_NRSUPER=9, FS_MAXSUPER=10, FS_OVERFLOWUID=11, FS_OVERFLOWGID=12, FS_LEASES=13, FS_DIR_NOTIFY=14, FS_LEASE_TIME=15, FS_DQSTATS=16, FS_XFS=17, FS_AIO_NR=18, FS_AIO_MAX_NR=19, FS_INOTIFY=20, FS_OCFS2=988, };
enum {
 FS_DQ_LOOKUPS = 1, FS_DQ_DROPS = 2, FS_DQ_READS = 3, FS_DQ_WRITES = 4, FS_DQ_CACHE_HITS = 5, FS_DQ_ALLOCATED = 6, FS_DQ_FREE = 7, FS_DQ_SYNCS = 8, FS_DQ_WARNINGS = 9, };
enum {
 DEV_CDROM=1, DEV_HWMON=2, DEV_PARPORT=3, DEV_RAID=4, DEV_MAC_HID=5, DEV_SCSI=6, DEV_IPMI=7, };
enum {
 DEV_CDROM_INFO=1, DEV_CDROM_AUTOCLOSE=2, DEV_CDROM_AUTOEJECT=3, DEV_CDROM_DEBUG=4, DEV_CDROM_LOCK=5, DEV_CDROM_CHECK_MEDIA=6
};
enum {
 DEV_PARPORT_DEFAULT=-3
};
enum {
 DEV_RAID_SPEED_LIMIT_MIN=1, DEV_RAID_SPEED_LIMIT_MAX=2
};
enum {
 DEV_PARPORT_DEFAULT_TIMESLICE=1, DEV_PARPORT_DEFAULT_SPINTIME=2
};
enum {
 DEV_PARPORT_SPINTIME=1, DEV_PARPORT_BASE_ADDR=2, DEV_PARPORT_IRQ=3, DEV_PARPORT_DMA=4, DEV_PARPORT_MODES=5, DEV_PARPORT_DEVICES=6, DEV_PARPORT_AUTOPROBE=16
};
enum {
 DEV_PARPORT_DEVICES_ACTIVE=-3, };
enum {
 DEV_PARPORT_DEVICE_TIMESLICE=1, };
enum {
 DEV_MAC_HID_KEYBOARD_SENDS_LINUX_KEYCODES=1, DEV_MAC_HID_KEYBOARD_LOCK_KEYCODES=2, DEV_MAC_HID_MOUSE_BUTTON_EMULATION=3, DEV_MAC_HID_MOUSE_BUTTON2_KEYCODE=4, DEV_MAC_HID_MOUSE_BUTTON3_KEYCODE=5, DEV_MAC_HID_ADB_MOUSE_SENDS_KEYCODES=6
};
enum {
 DEV_SCSI_LOGGING_LEVEL=1, };
enum {
 DEV_IPMI_POWEROFF_POWERCYCLE=1, };
enum
{
 ABI_DEFHANDLER_COFF=1, ABI_DEFHANDLER_ELF=2, ABI_DEFHANDLER_LCALL7=3, ABI_DEFHANDLER_LIBCSO=4, ABI_TRACE=5, ABI_FAKE_UTSNAME=6, };
struct ctl_table;
struct nsproxy;
struct ctl_table_root;
struct ctl_table_header;
struct ctl_dir;
typedef struct ctl_table ctl_table;
typedef int proc_handler (struct ctl_table *ctl, int write, void *buffer, size_t *lenp, loff_t *ppos);
struct ctl_table_poll {
 atomic_t event;
 wait_queue_head_t wait;
};
struct ctl_table
{
 const char *procname;
 void *data;
 int maxlen;
 umode_t mode;
 struct ctl_table *child;
 proc_handler *proc_handler;
 struct ctl_table_poll *poll;
 void *extra1;
 void *extra2;
};
struct ctl_node {
 struct rb_node node;
 struct ctl_table_header *header;
};
struct ctl_table_header
{
 union {
  struct {
   struct ctl_table *ctl_table;
   int used;
   int count;
   int nreg;
  };
  struct callback_head rcu;
 };
 struct completion *unregistering;
 struct ctl_table *ctl_table_arg;
 struct ctl_table_root *root;
 struct ctl_table_set *set;
 struct ctl_dir *parent;
 struct ctl_node *node;
};
struct ctl_dir {
 struct ctl_table_header header;
 struct rb_root root;
};
struct ctl_table_set {
 int (*is_seen)(struct ctl_table_set *);
 struct ctl_dir dir;
};
struct ctl_table_root {
 struct ctl_table_set default_set;
 struct ctl_table_set *(*lookup)(struct ctl_table_root *root, struct nsproxy *namespaces);
 int (*permissions)(struct ctl_table_header *head, struct ctl_table *table);
};
struct ctl_path {
 const char *procname;
};
void proc_sys_poll_notify(struct ctl_table_poll *poll);
void register_sysctl_root(struct ctl_table_root *root);
struct ctl_table_header *__register_sysctl_table( struct ctl_table_set *set, const char *path, struct ctl_table *table);
struct ctl_table_header *__register_sysctl_paths( struct ctl_table_set *set, const struct ctl_path *path, struct ctl_table *table);
struct ctl_table_header *register_sysctl(const char *path, struct ctl_table *table);
struct ctl_table_header *register_sysctl_table(struct ctl_table * table);
struct ctl_table_header *register_sysctl_paths(const struct ctl_path *path, struct ctl_table *table);
void unregister_sysctl_table(struct ctl_table_header * table);
extern
int __request_module(bool wait, const char *name, ...);
struct cred;
struct file;
struct subprocess_info {
 struct work_struct work;
 struct completion *complete;
 char *path;
 char **argv;
 char **envp;
 int wait;
 int retval;
 int (*init)(struct subprocess_info *info, struct cred *new);
 void (*cleanup)(struct subprocess_info *info);
 void *data;
};
extern int
call_usermodehelper(char *path, char **argv, char **envp, int wait);
extern struct subprocess_info *
call_usermodehelper_setup(char *path, char **argv, char **envp, gfp_t gfp_mask, int (*init)(struct subprocess_info *info, struct cred *new), void (*cleanup)(struct subprocess_info *), void *data);
extern int
call_usermodehelper_exec(struct subprocess_info *info, int wait);
enum umh_disable_depth {
 UMH_ENABLED = 0, UMH_FREEZING, UMH_DISABLED, };
struct user_fp {
 struct fp_reg {
  unsigned int sign1:1;
  unsigned int unused:15;
  unsigned int sign2:1;
  unsigned int exponent:14;
  unsigned int j:1;
  unsigned int mantissa1:31;
  unsigned int mantissa0:32;
 } fpregs[8];
 unsigned int fpsr:32;
 unsigned int fpcr:32;
 unsigned char ftype[8];
 unsigned int init_flag;
};
struct user{
  struct pt_regs regs;
  int u_fpvalid;
  unsigned long int u_tsize;
  unsigned long int u_dsize;
  unsigned long int u_ssize;
  unsigned long start_code;
  unsigned long start_stack;
  long int signal;
  int reserved;
  unsigned long u_ar0;
  unsigned long magic;
  char u_comm[32];
  int u_debugreg[8];
  struct user_fp u_fp;
  struct user_fp_struct * u_fp0;
};
struct user_vfp {
 unsigned long long fpregs[32];
 unsigned long fpscr;
};
struct user_vfp_exc {
 unsigned long fpexc;
 unsigned long fpinst;
 unsigned long fpinst2;
};
struct task_struct;
typedef unsigned long elf_greg_t;
typedef unsigned long elf_freg_t[3];
typedef elf_greg_t elf_gregset_t[(sizeof (struct pt_regs) / sizeof(elf_greg_t))];
typedef struct user_fp elf_fpregset_t;
struct elf32_hdr;
struct task_struct;
int dump_task_regs(struct task_struct *t, elf_gregset_t *elfregs);
struct mm_struct;
struct linux_binprm;
int arch_setup_additional_pages(struct linux_binprm *, int);
typedef __u32 Elf32_Addr;
typedef __u16 Elf32_Half;
typedef __u32 Elf32_Off;
typedef __s32 Elf32_Sword;
typedef __u32 Elf32_Word;
typedef __u64 Elf64_Addr;
typedef __u16 Elf64_Half;
typedef __s16 Elf64_SHalf;
typedef __u64 Elf64_Off;
typedef __s32 Elf64_Sword;
typedef __u32 Elf64_Word;
typedef __u64 Elf64_Xword;
typedef __s64 Elf64_Sxword;
typedef struct dynamic{
  Elf32_Sword d_tag;
  union{
    Elf32_Sword d_val;
    Elf32_Addr d_ptr;
  } d_un;
} Elf32_Dyn;
typedef struct {
  Elf64_Sxword d_tag;
  union {
    Elf64_Xword d_val;
    Elf64_Addr d_ptr;
  } d_un;
} Elf64_Dyn;
typedef struct elf32_rel {
  Elf32_Addr r_offset;
  Elf32_Word r_info;
} Elf32_Rel;
typedef struct elf64_rel {
  Elf64_Addr r_offset;
  Elf64_Xword r_info;
} Elf64_Rel;
typedef struct elf32_rela{
  Elf32_Addr r_offset;
  Elf32_Word r_info;
  Elf32_Sword r_addend;
} Elf32_Rela;
typedef struct elf64_rela {
  Elf64_Addr r_offset;
  Elf64_Xword r_info;
  Elf64_Sxword r_addend;
} Elf64_Rela;
typedef struct elf32_sym{
  Elf32_Word st_name;
  Elf32_Addr st_value;
  Elf32_Word st_size;
  unsigned char st_info;
  unsigned char st_other;
  Elf32_Half st_shndx;
} Elf32_Sym;
typedef struct elf64_sym {
  Elf64_Word st_name;
  unsigned char st_info;
  unsigned char st_other;
  Elf64_Half st_shndx;
  Elf64_Addr st_value;
  Elf64_Xword st_size;
} Elf64_Sym;
typedef struct elf32_hdr{
  unsigned char e_ident[16];
  Elf32_Half e_type;
  Elf32_Half e_machine;
  Elf32_Word e_version;
  Elf32_Addr e_entry;
  Elf32_Off e_phoff;
  Elf32_Off e_shoff;
  Elf32_Word e_flags;
  Elf32_Half e_ehsize;
  Elf32_Half e_phentsize;
  Elf32_Half e_phnum;
  Elf32_Half e_shentsize;
  Elf32_Half e_shnum;
  Elf32_Half e_shstrndx;
} Elf32_Ehdr;
typedef struct elf64_hdr {
  unsigned char e_ident[16];
  Elf64_Half e_type;
  Elf64_Half e_machine;
  Elf64_Word e_version;
  Elf64_Addr e_entry;
  Elf64_Off e_phoff;
  Elf64_Off e_shoff;
  Elf64_Word e_flags;
  Elf64_Half e_ehsize;
  Elf64_Half e_phentsize;
  Elf64_Half e_phnum;
  Elf64_Half e_shentsize;
  Elf64_Half e_shnum;
  Elf64_Half e_shstrndx;
} Elf64_Ehdr;
typedef struct elf32_phdr{
  Elf32_Word p_type;
  Elf32_Off p_offset;
  Elf32_Addr p_vaddr;
  Elf32_Addr p_paddr;
  Elf32_Word p_filesz;
  Elf32_Word p_memsz;
  Elf32_Word p_flags;
  Elf32_Word p_align;
} Elf32_Phdr;
typedef struct elf64_phdr {
  Elf64_Word p_type;
  Elf64_Word p_flags;
  Elf64_Off p_offset;
  Elf64_Addr p_vaddr;
  Elf64_Addr p_paddr;
  Elf64_Xword p_filesz;
  Elf64_Xword p_memsz;
  Elf64_Xword p_align;
} Elf64_Phdr;
typedef struct elf32_shdr {
  Elf32_Word sh_name;
  Elf32_Word sh_type;
  Elf32_Word sh_flags;
  Elf32_Addr sh_addr;
  Elf32_Off sh_offset;
  Elf32_Word sh_size;
  Elf32_Word sh_link;
  Elf32_Word sh_info;
  Elf32_Word sh_addralign;
  Elf32_Word sh_entsize;
} Elf32_Shdr;
typedef struct elf64_shdr {
  Elf64_Word sh_name;
  Elf64_Word sh_type;
  Elf64_Xword sh_flags;
  Elf64_Addr sh_addr;
  Elf64_Off sh_offset;
  Elf64_Xword sh_size;
  Elf64_Word sh_link;
  Elf64_Word sh_info;
  Elf64_Xword sh_addralign;
  Elf64_Xword sh_entsize;
} Elf64_Shdr;
typedef struct elf32_note {
  Elf32_Word n_namesz;
  Elf32_Word n_descsz;
  Elf32_Word n_type;
} Elf32_Nhdr;
typedef struct elf64_note {
  Elf64_Word n_namesz;
  Elf64_Word n_descsz;
  Elf64_Word n_type;
} Elf64_Nhdr;
struct file;

struct kernel_param {
 const char *name;
 const struct kernel_param_ops *ops;
 u16 perm;
 s16 level;
 union {
  void *arg;
  const struct kparam_string *str;
  const struct kparam_array *arr;
 };
};
struct kparam_string {
 unsigned int maxlen;
 char *string;
};
struct kparam_array
{
 unsigned int max;
 unsigned int elemsize;
 unsigned int *num;
 const struct kernel_param_ops *ops;
 void *elem;
};
struct module;
struct static_key {
 atomic_t enabled;
 struct jump_entry *entries;
 struct static_key_mod *next;
};
struct static_key_deferred {
 struct static_key key;
 unsigned long timeout;
 struct delayed_work work;
};
void soft_restart(unsigned long);
typedef u32 jump_label_t;
struct jump_entry {
 jump_label_t code;
 jump_label_t target;
 jump_label_t key;
};
enum jump_label_type {
 JUMP_LABEL_DISABLE = 0, JUMP_LABEL_ENABLE, };
struct module;
extern void
jump_label_rate_limit(struct static_key_deferred *key, unsigned long rl);
struct module;
struct tracepoint;
struct tracepoint_func {
 void *func;
 void *data;
};
struct tracepoint {
 const char *name;
 struct static_key key;
 void (*regfunc)(void);
 void (*unregfunc)(void);
 struct tracepoint_func *funcs;
};
extern int
tracepoint_probe_unregister(const char *name, void *probe, void *data);
struct tp_module {
 struct list_head list;
 unsigned int num_tracepoints;
 struct tracepoint * const *tracepoints_ptrs;
};
bool trace_module_has_bad_taint(struct module *mod);
struct tracepoint_iter {
 struct tp_module *module;
 struct tracepoint * const *tracepoint;
};
struct unwind_table;
enum {
 ARM_SEC_INIT, ARM_SEC_DEVINIT, ARM_SEC_CORE, ARM_SEC_EXIT, ARM_SEC_DEVEXIT, ARM_SEC_HOT, ARM_SEC_UNLIKELY, ARM_SEC_MAX, };
struct mod_arch_specific {
 struct unwind_table *unwind[ARM_SEC_MAX];
};
struct modversion_info
{
 unsigned long crc;
 char name[(64 - sizeof(unsigned long))];
};
struct module;
struct module_kobject {
 struct kobject kobj;
 struct module *mod;
 struct kobject *drivers_dir;
 struct module_param_attrs *mp;
};
struct module_attribute {
 struct attribute attr;
 ssize_t (*show)(struct module_attribute *, struct module_kobject *, char *);
 ssize_t (*store)(struct module_attribute *, struct module_kobject *, const char *, size_t count);
 void (*setup)(struct module *, const char *);
 int (*test)(struct module *);
 void (*free)(struct module *);
};
struct module_version_attribute {
 struct module_attribute mattr;
 const char *module_name;
 const char *version;
} ;
struct exception_table_entry;
const struct exception_table_entry *
search_extable(const struct exception_table_entry *first, const struct exception_table_entry *last, unsigned long value);
void sort_extable(struct exception_table_entry *start, struct exception_table_entry *finish);
void sort_main_extable(void);
void trim_init_extable(struct module *m);
const struct exception_table_entry *search_exception_tables(unsigned long add);
struct notifier_block;
void *__symbol_get(const char *symbol);
void *__symbol_get_gpl(const char *symbol);
struct module_use {
 struct list_head source_list;
 struct list_head target_list;
 struct module *source, *target;
};
enum module_state {
 MODULE_STATE_LIVE, MODULE_STATE_COMING, MODULE_STATE_GOING, MODULE_STATE_UNFORMED, };
struct module_ref {
 unsigned long incs;
 unsigned long decs;
} ;
struct mod_kallsyms {
 Elf32_Sym *symtab;
 unsigned int num_symtab;
 char *strtab;
};
struct module
{
 enum module_state state;
 struct list_head list;
 char name[(64 - sizeof(unsigned long))];
 struct module_kobject mkobj;
 struct module_attribute *modinfo_attrs;
 const char *version;
 const char *srcversion;
 struct kobject *holders_dir;
 const struct kernel_symbol *syms;
 const unsigned long *crcs;
 unsigned int num_syms;
 struct kernel_param *kp;
 unsigned int num_kp;
 unsigned int num_gpl_syms;
 const struct kernel_symbol *gpl_syms;
 const unsigned long *gpl_crcs;
 const struct kernel_symbol *gpl_future_syms;
 const unsigned long *gpl_future_crcs;
 unsigned int num_gpl_future_syms;
 unsigned int num_exentries;
 struct exception_table_entry *extable;
 int (*init)(void);
 void *module_init;
 void *module_core;
 unsigned int init_size, core_size;
 unsigned int init_text_size, core_text_size;
 unsigned int init_ro_size, core_ro_size;
 struct mod_arch_specific arch;
 unsigned int taints;
 unsigned num_bugs;
 struct list_head bug_list;
 struct bug_entry *bug_table;
 struct mod_kallsyms *kallsyms;
 struct mod_kallsyms core_kallsyms;
 struct module_sect_attrs *sect_attrs;
 struct module_notes_attrs *notes_attrs;
 char *args;
 void *percpu;
 unsigned int percpu_size;
 unsigned int num_tracepoints;
 struct tracepoint * const *tracepoints_ptrs;
 struct jump_entry *jump_entries;
 unsigned int num_jump_entries;
 unsigned int num_trace_bprintk_fmt;
 const char **trace_bprintk_fmt_start;
 struct ftrace_event_call **trace_events;
 unsigned int num_trace_events;
 struct list_head source_list;
 struct list_head target_list;
 struct task_struct *waiter;
 void (*exit)(void);
 struct module_ref *refptr;
};
struct module *__module_text_address(unsigned long addr);
struct module *__module_address(unsigned long addr);
bool is_module_address(unsigned long addr);
bool is_module_percpu_address(unsigned long addr);
bool is_module_text_address(unsigned long addr);
struct module *find_module(const char *name);
struct symsearch {
 const struct kernel_symbol *start, *stop;
 const unsigned long *crcs;
 enum {
  NOT_GPL_ONLY, GPL_ONLY, WILL_BE_GPL_ONLY, } licence;
 bool unused;
};
const struct kernel_symbol *find_symbol(const char *name, struct module **owner, const unsigned long **crc, bool gplok, bool warn);
bool each_symbol_section(bool (*fn)(const struct symsearch *arr, struct module *owner, void *data), void *data);
int module_get_kallsym(unsigned int symnum, unsigned long *value, char *type, char *name, char *module_name, int *exported);
unsigned long module_kallsyms_lookup_name(const char *name);
int module_kallsyms_on_each_symbol(int (*fn)(void *, const char *, struct module *, unsigned long), void *data);
extern void __module_put_and_exit(struct module *mod, long code)
 ;
unsigned long module_refcount(struct module *mod);
void __symbol_put(const char *symbol);
void symbol_put_addr(void *addr);
int ref_module(struct module *a, struct module *b);
const char *module_address_lookup(unsigned long addr, unsigned long *symbolsize, unsigned long *offset, char **modname, char *namebuf);
int lookup_module_symbol_name(unsigned long addr, char *symname);
int lookup_module_symbol_attrs(unsigned long addr, unsigned long *size, unsigned long *offset, char *modname, char *name);
const struct exception_table_entry *search_module_extables(unsigned long addr);
int register_module_notifier(struct notifier_block * nb);
int unregister_module_notifier(struct notifier_block * nb);


void module_bug_finalize(const Elf32_Ehdr *, const Elf32_Shdr *, struct module *);
void module_bug_cleanup(struct module *);
struct hlist_bl_head {
 struct hlist_bl_node *first;
};
struct hlist_bl_node {
 struct hlist_bl_node *next, **pprev;
};
struct nameidata;
struct path;
struct vfsmount;
struct qstr {
 union {
  struct {
   u32 hash; u32 len;;
  };
  u64 hash_len;
 };
 const unsigned char *name;
};
struct dentry_stat_t {
 int nr_dentry;
 int nr_unused;
 int age_limit;
 int want_pages;
 int dummy[2];
};
struct dentry {
 unsigned int d_flags;
 seqcount_t d_seq;
 struct hlist_bl_node d_hash;
 struct dentry *d_parent;
 struct qstr d_name;
 struct inode *d_inode;
 unsigned char d_iname[36];
 unsigned int d_count;
 spinlock_t d_lock;
 const struct dentry_operations *d_op;
 struct super_block *d_sb;
 unsigned long d_time;
 void *d_fsdata;
 struct list_head d_lru;
 struct list_head d_child;
 struct list_head d_subdirs;
 union {
  struct hlist_node d_alias;
   struct callback_head d_rcu;
 } d_u;
};
enum dentry_d_lock_class
{
 DENTRY_D_LOCK_NORMAL, DENTRY_D_LOCK_NESTED
};
struct dentry_operations {
 int (*d_revalidate)(struct dentry *, unsigned int);
 int (*d_weak_revalidate)(struct dentry *, unsigned int);
 int (*d_hash)(const struct dentry *, const struct inode *, struct qstr *);
 int (*d_compare)(const struct dentry *, const struct inode *, const struct dentry *, const struct inode *, unsigned int, const char *, const struct qstr *);
 int (*d_delete)(const struct dentry *);
 void (*d_release)(struct dentry *);
 void (*d_prune)(struct dentry *);
 void (*d_iput)(struct dentry *, struct inode *);
 char *(*d_dname)(struct dentry *, char *, int);
 struct vfsmount *(*d_automount)(struct path *);
 int (*d_manage)(struct dentry *, bool);
 void (*d_canonical_path)(const struct path *, struct path *);
} ;
struct dentry;
struct vfsmount;
struct path {
 struct vfsmount *mnt;
 struct dentry *dentry;
};
struct llist_head {
 struct llist_node *first;
};
struct llist_node {
 struct llist_node *next;
};
struct radix_tree_root {
 unsigned int height;
 gfp_t gfp_mask;
 struct radix_tree_node *rnode;
};
int radix_tree_insert(struct radix_tree_root *, unsigned long, void *);
void *radix_tree_lookup(struct radix_tree_root *, unsigned long);
void **radix_tree_lookup_slot(struct radix_tree_root *, unsigned long);
void *radix_tree_delete(struct radix_tree_root *, unsigned long);
unsigned int
radix_tree_gang_lookup(struct radix_tree_root *root, void **results, unsigned long first_index, unsigned int max_items);
unsigned int radix_tree_gang_lookup_slot(struct radix_tree_root *root, void ***results, unsigned long *indices, unsigned long first_index, unsigned int max_items);
unsigned long radix_tree_next_hole(struct radix_tree_root *root, unsigned long index, unsigned long max_scan);
unsigned long radix_tree_prev_hole(struct radix_tree_root *root, unsigned long index, unsigned long max_scan);
int radix_tree_preload(gfp_t gfp_mask);
void radix_tree_init(void);
void *radix_tree_tag_set(struct radix_tree_root *root, unsigned long index, unsigned int tag);
void *radix_tree_tag_clear(struct radix_tree_root *root, unsigned long index, unsigned int tag);
int radix_tree_tag_get(struct radix_tree_root *root, unsigned long index, unsigned int tag);
unsigned int
radix_tree_gang_lookup_tag(struct radix_tree_root *root, void **results, unsigned long first_index, unsigned int max_items, unsigned int tag);
unsigned int
radix_tree_gang_lookup_tag_slot(struct radix_tree_root *root, void ***results, unsigned long first_index, unsigned int max_items, unsigned int tag);
unsigned long radix_tree_range_tag_if_tagged(struct radix_tree_root *root, unsigned long *first_indexp, unsigned long last_index, unsigned long nr_to_tag, unsigned int fromtag, unsigned int totag);
int radix_tree_tagged(struct radix_tree_root *root, unsigned int tag);
unsigned long radix_tree_locate_item(struct radix_tree_root *root, void *item);
struct radix_tree_iter {
 unsigned long index;
 unsigned long next_index;
 unsigned long tags;
};
void **radix_tree_next_chunk(struct radix_tree_root *root, struct radix_tree_iter *iter, unsigned flags);
enum pid_type
{
 PIDTYPE_PID, PIDTYPE_PGID, PIDTYPE_SID, PIDTYPE_MAX
};
struct upid {
 int nr;
 struct pid_namespace *ns;
 struct hlist_node pid_chain;
};
struct pid
{
 atomic_t count;
 unsigned int level;
 struct hlist_head tasks[PIDTYPE_MAX];
 struct callback_head rcu;
 struct upid numbers[1];
};
struct pid_link
{
 struct hlist_node node;
 struct pid *pid;
};
struct pid_namespace;
int next_pidmap(struct pid_namespace *pid_ns, unsigned int last);
pid_t pid_nr_ns(struct pid *pid, struct pid_namespace *ns);
pid_t pid_vnr(struct pid *pid);
struct task_struct;
typedef struct __user_cap_header_struct {
 __u32 version;
 int pid;
} *cap_user_header_t;
typedef struct __user_cap_data_struct {
        __u32 effective;
        __u32 permitted;
        __u32 inheritable;
} *cap_user_data_t;
struct vfs_cap_data {
 __le32 magic_etc;
 struct {
  __le32 permitted;
  __le32 inheritable;
 } data[2];
};
typedef struct kernel_cap_struct {
 __u32 cap[2];
} kernel_cap_t;
struct cpu_vfs_cap_data {
 __u32 magic_etc;
 kernel_cap_t permitted;
 kernel_cap_t inheritable;
};
struct file;
struct inode;
struct dentry;
struct user_namespace;
struct user_namespace *current_user_ns(void);
struct semaphore {
 raw_spinlock_t lock;
 unsigned int count;
 struct list_head wait_list;
};
struct fiemap_extent {
 __u64 fe_logical;
 __u64 fe_physical;
 __u64 fe_length;
 __u64 fe_reserved64[2];
 __u32 fe_flags;
 __u32 fe_reserved[3];
};
struct fiemap {
 __u64 fm_start;
 __u64 fm_length;
 __u32 fm_flags;
 __u32 fm_mapped_extents;
 __u32 fm_extent_count;
 __u32 fm_reserved;
 struct fiemap_extent fm_extents[0];
};
struct shrink_control {
 gfp_t gfp_mask;
 unsigned long nr_to_scan;
};
struct shrinker {
 int (*shrink)(struct shrinker *, struct shrink_control *sc);
 int seeks;
 long batch;
 struct list_head list;
 atomic_long_t nr_in_batch;
};
enum migrate_mode {
 MIGRATE_ASYNC, MIGRATE_SYNC_LIGHT, MIGRATE_SYNC, };
struct percpu_rw_semaphore {
 unsigned int *fast_read_ctr;
 atomic_t write_ctr;
 struct rw_semaphore rw_sem;
 atomic_t slow_read_ctr;
 wait_queue_head_t write_waitq;
};
struct bio_set;
struct bio;
struct bio_integrity_payload;
struct page;
struct block_device;
struct io_context;
struct cgroup_subsys_state;
typedef void (bio_end_io_t) (struct bio *, int);
typedef void (bio_destructor_t) (struct bio *);
struct bio_vec {
 struct page *bv_page;
 unsigned int bv_len;
 unsigned int bv_offset;
};
struct bio {
 sector_t bi_sector;
 struct bio *bi_next;
 struct block_device *bi_bdev;
 unsigned long bi_flags;
 unsigned long bi_rw;
 unsigned short bi_vcnt;
 unsigned short bi_idx;
 unsigned int bi_phys_segments;
 unsigned int bi_size;
 unsigned int bi_seg_front_size;
 unsigned int bi_seg_back_size;
 bio_end_io_t *bi_end_io;
 void *bi_private;
 unsigned int bi_max_vecs;
 atomic_t bi_cnt;
 struct bio_vec *bi_io_vec;
 struct bio_set *bi_pool;
 struct bio_vec bi_inline_vecs[0];
};
enum rq_flag_bits {
 __REQ_WRITE, __REQ_FAILFAST_DEV, __REQ_FAILFAST_TRANSPORT, __REQ_FAILFAST_DRIVER, __REQ_SYNC, __REQ_META, __REQ_PRIO, __REQ_DISCARD, __REQ_SECURE, __REQ_WRITE_SAME, __REQ_NOIDLE, __REQ_FUA, __REQ_FLUSH, __REQ_RAHEAD, __REQ_THROTTLED, __REQ_SORTED, __REQ_SOFTBARRIER, __REQ_NOMERGE, __REQ_STARTED, __REQ_DONTPREP, __REQ_QUEUED, __REQ_ELVPRIV, __REQ_FAILED, __REQ_QUIET, __REQ_PREEMPT, __REQ_ALLOCED, __REQ_COPY_USER, __REQ_FLUSH_SEQ, __REQ_IO_STAT, __REQ_MIXED_MERGE, __REQ_KERNEL, __REQ_PM, __REQ_NR_BITS, };
struct fstrim_range {
 __u64 start;
 __u64 len;
 __u64 minlen;
};
struct files_stat_struct {
 unsigned long nr_files;
 unsigned long nr_free_files;
 unsigned long max_files;
};
struct inodes_stat_t {
 int nr_inodes;
 int nr_unused;
 int dummy[5];
};
struct export_operations;
struct hd_geometry;
struct iovec;
struct nameidata;
struct kiocb;
struct kobject;
struct pipe_inode_info;
struct poll_table_struct;
struct kstatfs;
struct vm_area_struct;
struct vfsmount;
struct cred;
struct swap_info_struct;
struct seq_file;
struct workqueue_struct;
struct buffer_head;
typedef int (get_block_t)(struct inode *inode, sector_t iblock, struct buffer_head *bh_result, int create);
typedef void (dio_iodone_t)(struct kiocb *iocb, loff_t offset, ssize_t bytes, void *private);
struct iattr {
 unsigned int ia_valid;
 umode_t ia_mode;
 kuid_t ia_uid;
 kgid_t ia_gid;
 loff_t ia_size;
 struct timespec ia_atime;
 struct timespec ia_mtime;
 struct timespec ia_ctime;
 struct file *ia_file;
};
struct percpu_counter {
 raw_spinlock_t lock;
 s64 count;
 struct list_head list;
 s32 *counters;
};
int __percpu_counter_init(struct percpu_counter *fbc, s64 amount, struct lock_class_key *key);
void percpu_counter_destroy(struct percpu_counter *fbc);
void percpu_counter_set(struct percpu_counter *fbc, s64 amount);
void __percpu_counter_add(struct percpu_counter *fbc, s64 amount, s32 batch);
s64 __percpu_counter_sum(struct percpu_counter *fbc);
int percpu_counter_compare(struct percpu_counter *fbc, s64 rhs);
typedef struct fs_disk_quota {
 __s8 d_version;
 __s8 d_flags;
 __u16 d_fieldmask;
 __u32 d_id;
 __u64 d_blk_hardlimit;
 __u64 d_blk_softlimit;
 __u64 d_ino_hardlimit;
 __u64 d_ino_softlimit;
 __u64 d_bcount;
 __u64 d_icount;
 __s32 d_itimer;
 __s32 d_btimer;
 __u16 d_iwarns;
 __u16 d_bwarns;
 __s32 d_padding2;
 __u64 d_rtb_hardlimit;
 __u64 d_rtb_softlimit;
 __u64 d_rtbcount;
 __s32 d_rtbtimer;
 __u16 d_rtbwarns;
 __s16 d_padding3;
 char d_padding4[8];
} fs_disk_quota_t;
typedef struct fs_qfilestat {
 __u64 qfs_ino;
 __u64 qfs_nblks;
 __u32 qfs_nextents;
} fs_qfilestat_t;
typedef struct fs_quota_stat {
 __s8 qs_version;
 __u16 qs_flags;
 __s8 qs_pad;
 fs_qfilestat_t qs_uquota;
 fs_qfilestat_t qs_gquota;
 __u32 qs_incoredqs;
 __s32 qs_btimelimit;
 __s32 qs_itimelimit;
 __s32 qs_rtbtimelimit;
 __u16 qs_bwarnlimit;
 __u16 qs_iwarnlimit;
} fs_quota_stat_t;
struct dquot;
struct qtree_fmt_operations {
 void (*mem2disk_dqblk)(void *disk, struct dquot *dquot);
 void (*disk2mem_dqblk)(struct dquot *dquot, void *disk);
 int (*is_id)(void *disk, struct dquot *dquot);
};
struct qtree_mem_dqinfo {
 struct super_block *dqi_sb;
 int dqi_type;
 unsigned int dqi_blocks;
 unsigned int dqi_free_blk;
 unsigned int dqi_free_entry;
 unsigned int dqi_blocksize_bits;
 unsigned int dqi_entry_size;
 unsigned int dqi_usable_bs;
 unsigned int dqi_qtree_depth;
 struct qtree_fmt_operations *dqi_ops;
};
int qtree_write_dquot(struct qtree_mem_dqinfo *info, struct dquot *dquot);
int qtree_read_dquot(struct qtree_mem_dqinfo *info, struct dquot *dquot);
int qtree_delete_dquot(struct qtree_mem_dqinfo *info, struct dquot *dquot);
int qtree_release_dquot(struct qtree_mem_dqinfo *info, struct dquot *dquot);
int qtree_entry_unused(struct qtree_mem_dqinfo *info, char *disk);
struct user_namespace;
typedef __kernel_uid32_t projid_t;
typedef projid_t kprojid_t;
enum {
 QIF_BLIMITS_B = 0, QIF_SPACE_B, QIF_ILIMITS_B, QIF_INODES_B, QIF_BTIME_B, QIF_ITIME_B, };
struct if_dqblk {
 __u64 dqb_bhardlimit;
 __u64 dqb_bsoftlimit;
 __u64 dqb_curspace;
 __u64 dqb_ihardlimit;
 __u64 dqb_isoftlimit;
 __u64 dqb_curinodes;
 __u64 dqb_btime;
 __u64 dqb_itime;
 __u32 dqb_valid;
};
struct if_dqinfo {
 __u64 dqi_bgrace;
 __u64 dqi_igrace;
 __u32 dqi_flags;
 __u32 dqi_valid;
};
enum {
 QUOTA_NL_C_UNSPEC, QUOTA_NL_C_WARNING, __QUOTA_NL_C_MAX, };
enum {
 QUOTA_NL_A_UNSPEC, QUOTA_NL_A_QTYPE, QUOTA_NL_A_EXCESS_ID, QUOTA_NL_A_WARNING, QUOTA_NL_A_DEV_MAJOR, QUOTA_NL_A_DEV_MINOR, QUOTA_NL_A_CAUSED_ID, __QUOTA_NL_A_MAX, };
enum quota_type {
 USRQUOTA = 0, GRPQUOTA = 1, PRJQUOTA = 2, };
typedef __kernel_uid32_t qid_t;
typedef long long qsize_t;
struct kqid {
 union {
  kuid_t uid;
  kgid_t gid;
  kprojid_t projid;
 };
 enum quota_type type;
};
struct mem_dqblk {
 qsize_t dqb_bhardlimit;
 qsize_t dqb_bsoftlimit;
 qsize_t dqb_curspace;
 qsize_t dqb_rsvspace;
 qsize_t dqb_ihardlimit;
 qsize_t dqb_isoftlimit;
 qsize_t dqb_curinodes;
 time_t dqb_btime;
 time_t dqb_itime;
};
struct quota_format_type;
struct mem_dqinfo {
 struct quota_format_type *dqi_format;
 int dqi_fmt_id;
 struct list_head dqi_dirty_list;
 unsigned long dqi_flags;
 unsigned int dqi_bgrace;
 unsigned int dqi_igrace;
 qsize_t dqi_maxblimit;
 qsize_t dqi_maxilimit;
 void *dqi_priv;
};
struct super_block;
enum {
 DQST_LOOKUPS, DQST_DROPS, DQST_READS, DQST_WRITES, DQST_CACHE_HITS, DQST_ALLOC_DQUOTS, DQST_FREE_DQUOTS, DQST_SYNCS, _DQST_DQSTAT_LAST
};
struct dqstats {
 int stat[_DQST_DQSTAT_LAST];
 struct percpu_counter counter[_DQST_DQSTAT_LAST];
};
struct dquot {
 struct hlist_node dq_hash;
 struct list_head dq_inuse;
 struct list_head dq_free;
 struct list_head dq_dirty;
 struct mutex dq_lock;
 atomic_t dq_count;
 wait_queue_head_t dq_wait_unused;
 struct super_block *dq_sb;
 struct kqid dq_id;
 loff_t dq_off;
 unsigned long dq_flags;
 struct mem_dqblk dq_dqb;
};
struct quota_format_ops {
 int (*check_quota_file)(struct super_block *sb, int type);
 int (*read_file_info)(struct super_block *sb, int type);
 int (*write_file_info)(struct super_block *sb, int type);
 int (*free_file_info)(struct super_block *sb, int type);
 int (*read_dqblk)(struct dquot *dquot);
 int (*commit_dqblk)(struct dquot *dquot);
 int (*release_dqblk)(struct dquot *dquot);
};
struct dquot_operations {
 int (*write_dquot) (struct dquot *);
 struct dquot *(*alloc_dquot)(struct super_block *, int);
 void (*destroy_dquot)(struct dquot *);
 int (*acquire_dquot) (struct dquot *);
 int (*release_dquot) (struct dquot *);
 int (*mark_dirty) (struct dquot *);
 int (*write_info) (struct super_block *, int);
 qsize_t *(*get_reserved_space) (struct inode *);
};
struct path;
struct quotactl_ops {
 int (*quota_on)(struct super_block *, int, int, struct path *);
 int (*quota_on_meta)(struct super_block *, int, int);
 int (*quota_off)(struct super_block *, int);
 int (*quota_sync)(struct super_block *, int);
 int (*get_info)(struct super_block *, int, struct if_dqinfo *);
 int (*set_info)(struct super_block *, int, struct if_dqinfo *);
 int (*get_dqblk)(struct super_block *, struct kqid, struct fs_disk_quota *);
 int (*set_dqblk)(struct super_block *, struct kqid, struct fs_disk_quota *);
 int (*get_xstate)(struct super_block *, struct fs_quota_stat *);
 int (*set_xstate)(struct super_block *, unsigned int, int);
};
struct quota_format_type {
 int qf_fmt_id;
 const struct quota_format_ops *qf_ops;
 struct module *qf_owner;
 struct quota_format_type *qf_next;
};
enum {
 _DQUOT_USAGE_ENABLED = 0, _DQUOT_LIMITS_ENABLED, _DQUOT_SUSPENDED, _DQUOT_STATE_FLAGS
};
struct quota_info {
 unsigned int flags;
 struct mutex dqio_mutex;
 struct mutex dqonoff_mutex;
 struct rw_semaphore dqptr_sem;
 struct inode *files[2];
 struct mem_dqinfo info[2];
 const struct quota_format_ops *ops[2];
};
int register_quota_format(struct quota_format_type *fmt);
void unregister_quota_format(struct quota_format_type *fmt);
struct quota_module_name {
 int qm_fmt_id;
 char *qm_mod_name;
};
enum positive_aop_returns {
 AOP_WRITEPAGE_ACTIVATE = 0x80000, AOP_TRUNCATED_PAGE = 0x80001, };
struct page;
struct address_space;
struct writeback_control;
struct iov_iter {
 const struct iovec *iov;
 unsigned long nr_segs;
 size_t iov_offset;
 size_t count;
};
size_t iov_iter_copy_from_user_atomic(struct page *page, struct iov_iter *i, unsigned long offset, size_t bytes);
size_t iov_iter_copy_from_user(struct page *page, struct iov_iter *i, unsigned long offset, size_t bytes);
void iov_iter_advance(struct iov_iter *i, size_t bytes);
int iov_iter_fault_in_readable(struct iov_iter *i, size_t bytes);
size_t iov_iter_single_seg_count(const struct iov_iter *i);
typedef struct {
 size_t written;
 size_t count;
 union {
  char *buf;
  void *data;
 } arg;
 int error;
} read_descriptor_t;
typedef int (*read_actor_t)(read_descriptor_t *, struct page *, unsigned long, unsigned long);
struct address_space_operations {
 int (*writepage)(struct page *page, struct writeback_control *wbc);
 int (*readpage)(struct file *, struct page *);
 int (*writepages)(struct address_space *, struct writeback_control *);
 int (*set_page_dirty)(struct page *page);
 int (*readpages)(struct file *filp, struct address_space *mapping, struct list_head *pages, unsigned nr_pages);
 int (*write_begin)(struct file *, struct address_space *mapping, loff_t pos, unsigned len, unsigned flags, struct page **pagep, void **fsdata);
 int (*write_end)(struct file *, struct address_space *mapping, loff_t pos, unsigned len, unsigned copied, struct page *page, void *fsdata);
 sector_t (*bmap)(struct address_space *, sector_t);
 void (*invalidatepage) (struct page *, unsigned int, unsigned int);
 int (*releasepage) (struct page *, gfp_t);
 void (*freepage)(struct page *);
 ssize_t (*direct_IO)(int, struct kiocb *, const struct iovec *iov, loff_t offset, unsigned long nr_segs);
 int (*get_xip_mem)(struct address_space *, unsigned long, int, void **, unsigned long *);
 int (*migratepage) (struct address_space *, struct page *, struct page *, enum migrate_mode);
 int (*launder_page) (struct page *);
 int (*is_partially_uptodate) (struct page *, read_descriptor_t *, unsigned long);
 int (*error_remove_page)(struct address_space *, struct page *);
 int (*swap_activate)(struct swap_info_struct *sis, struct file *file, sector_t *span);
 void (*swap_deactivate)(struct file *file);
};
int pagecache_write_begin(struct file *, struct address_space *mapping, loff_t pos, unsigned len, unsigned flags, struct page **pagep, void **fsdata);
int pagecache_write_end(struct file *, struct address_space *mapping, loff_t pos, unsigned len, unsigned copied, struct page *page, void *fsdata);
struct backing_dev_info;
struct address_space {
 struct inode *host;
 struct radix_tree_root page_tree;
 spinlock_t tree_lock;
 unsigned int i_mmap_writable;
 struct rb_root i_mmap;
 struct list_head i_mmap_nonlinear;
 struct mutex i_mmap_mutex;
 unsigned long nrpages;
 unsigned long writeback_index;
 const struct address_space_operations *a_ops;
 unsigned long flags;
 struct backing_dev_info *backing_dev_info;
 spinlock_t private_lock;
 struct list_head private_list;
 void *private_data;
} ;
struct request_queue;
struct block_device {
 dev_t bd_dev;
 int bd_openers;
 struct inode * bd_inode;
 struct super_block * bd_super;
 struct mutex bd_mutex;
 struct list_head bd_inodes;
 void * bd_claiming;
 void * bd_holder;
 int bd_holders;
 bool bd_write_holder;
 struct list_head bd_holder_disks;
 struct block_device * bd_contains;
 unsigned bd_block_size;
 struct hd_struct * bd_part;
 unsigned bd_part_count;
 int bd_invalidated;
 struct gendisk * bd_disk;
 struct request_queue * bd_queue;
 struct list_head bd_list;
 unsigned long bd_private;
 int bd_fsfreeze_count;
 struct mutex bd_fsfreeze_mutex;
};
int mapping_tagged(struct address_space *mapping, int tag);
struct posix_acl;
struct inode {
 umode_t i_mode;
 unsigned short i_opflags;
 kuid_t i_uid;
 kgid_t i_gid;
 unsigned int i_flags;
 struct posix_acl *i_acl;
 struct posix_acl *i_default_acl;
 const struct inode_operations *i_op;
 struct super_block *i_sb;
 struct address_space *i_mapping;
 void *i_security;
 unsigned long i_ino;
 union {
  const unsigned int i_nlink;
  unsigned int __i_nlink;
 };
 dev_t i_rdev;
 loff_t i_size;
 struct timespec i_atime;
 struct timespec i_mtime;
 struct timespec i_ctime;
 spinlock_t i_lock;
 unsigned short i_bytes;
 unsigned int i_blkbits;
 blkcnt_t i_blocks;
 seqcount_t i_size_seqcount;
 unsigned long i_state;
 struct mutex i_mutex;
 unsigned long dirtied_when;
 struct hlist_node i_hash;
 struct list_head i_wb_list;
 struct list_head i_lru;
 struct list_head i_sb_list;
 union {
  struct hlist_head i_dentry;
  struct callback_head i_rcu;
 };
 u64 i_version;
 atomic_t i_count;
 atomic_t i_dio_count;
 atomic_t i_writecount;
 const struct file_operations *i_fop;
 struct file_lock *i_flock;
 struct address_space i_data;
 struct dquot *i_dquot[2];
 struct list_head i_devices;
 union {
  struct pipe_inode_info *i_pipe;
  struct block_device *i_bdev;
  struct cdev *i_cdev;
 };
 __u32 i_generation;
 __u32 i_fsnotify_mask;
 struct hlist_head i_fsnotify_marks;
 void *i_private;
};
enum inode_i_mutex_lock_class
{
 I_MUTEX_NORMAL, I_MUTEX_PARENT, I_MUTEX_CHILD, I_MUTEX_XATTR, I_MUTEX_QUOTA, I_MUTEX_NONDIR2, I_MUTEX_PARENT2, };
void lock_two_nondirectories(struct inode *, struct inode*);
void unlock_two_nondirectories(struct inode *, struct inode*);
struct fown_struct {
 rwlock_t lock;
 struct pid *pid;
 enum pid_type pid_type;
 kuid_t uid, euid;
 int signum;
};
struct file_ra_state {
 unsigned long start;
 unsigned int size;
 unsigned int async_size;
 unsigned int ra_pages;
 unsigned int mmap_miss;
 loff_t prev_pos;
};
struct file {
 union {
  struct llist_node fu_llist;
  struct callback_head fu_rcuhead;
 } f_u;
 struct path f_path;
 struct inode *f_inode;
 const struct file_operations *f_op;
 spinlock_t f_lock;
 atomic_long_t f_count;
 unsigned int f_flags;
 fmode_t f_mode;
 loff_t f_pos;
 struct fown_struct f_owner;
 const struct cred *f_cred;
 struct file_ra_state f_ra;
 u64 f_version;
 void *f_security;
 void *private_data;
 struct list_head f_ep_links;
 struct list_head f_tfile_llink;
 struct address_space *f_mapping;
};
struct file_handle {
 __u32 handle_bytes;
 int handle_type;
 unsigned char f_handle[0];
};




typedef struct files_struct *fl_owner_t;
struct file_lock_operations {
 void (*fl_copy_lock)(struct file_lock *, struct file_lock *);
 void (*fl_release_private)(struct file_lock *);
};
struct lock_manager_operations {
 int (*lm_compare_owner)(struct file_lock *, struct file_lock *);
 void (*lm_notify)(struct file_lock *);
 int (*lm_grant)(struct file_lock *, struct file_lock *, int);
 void (*lm_break)(struct file_lock *);
 int (*lm_change)(struct file_lock **, int);
};
struct lock_manager {
 struct list_head list;
};
struct net;
void locks_start_grace(struct net *, struct lock_manager *);
void locks_end_grace(struct lock_manager *);
int locks_in_grace(struct net *);
struct nlm_lockowner;
struct nfs_lock_info {
 u32 state;
 struct nlm_lockowner *owner;
 struct list_head list;
};
struct nfs4_lock_state;
struct nfs4_lock_info {
 struct nfs4_lock_state *owner;
};
struct file_lock {
 struct file_lock *fl_next;
 struct list_head fl_link;
 struct list_head fl_block;
 fl_owner_t fl_owner;
 unsigned int fl_flags;
 unsigned char fl_type;
 unsigned int fl_pid;
 struct pid *fl_nspid;
 wait_queue_head_t fl_wait;
 struct file *fl_file;
 loff_t fl_start;
 loff_t fl_end;
 struct fasync_struct * fl_fasync;
 unsigned long fl_break_time;
 unsigned long fl_downgrade_time;
 const struct file_lock_operations *fl_ops;
 const struct lock_manager_operations *fl_lmops;
 union {
  struct nfs_lock_info nfs_fl;
  struct nfs4_lock_info nfs4_fl;
  struct {
   struct list_head link;
   int state;
  } afs;
 } fl_u;
};
struct f_owner_ex {
 int type;
 __kernel_pid_t pid;
};
struct flock {
 short l_type;
 short l_whence;
 __kernel_off_t l_start;
 __kernel_off_t l_len;
 __kernel_pid_t l_pid;
};
struct flock64 {
 short l_type;
 short l_whence;
 __kernel_loff_t l_start;
 __kernel_loff_t l_len;
 __kernel_pid_t l_pid;
};
void locks_free_lock(struct file_lock *fl);
struct fasync_struct {
 spinlock_t fa_lock;
 int magic;
 int fa_fd;
 struct fasync_struct *fa_next;
 struct file *fa_file;
 struct callback_head fa_rcu;
};
struct mm_struct;
enum {
 SB_UNFROZEN = 0, SB_FREEZE_WRITE = 1, SB_FREEZE_PAGEFAULT = 2, SB_FREEZE_FS = 3, SB_FREEZE_COMPLETE = 4, };
struct sb_writers {
 struct percpu_counter counter[(SB_FREEZE_COMPLETE - 1)];
 wait_queue_head_t wait;
 int frozen;
 wait_queue_head_t wait_unfrozen;
};
struct super_block {
 struct list_head s_list;
 dev_t s_dev;
 unsigned char s_blocksize_bits;
 unsigned long s_blocksize;
 loff_t s_maxbytes;
 struct file_system_type *s_type;
 const struct super_operations *s_op;
 const struct dquot_operations *dq_op;
 const struct quotactl_ops *s_qcop;
 const struct export_operations *s_export_op;
 unsigned long s_flags;
 unsigned long s_magic;
 struct dentry *s_root;
 struct rw_semaphore s_umount;
 int s_count;
 atomic_t s_active;
 void *s_security;
 const struct xattr_handler **s_xattr;
 struct list_head s_inodes;
 struct hlist_bl_head s_anon;
 struct list_head s_mounts;
 struct list_head s_dentry_lru;
 int s_nr_dentry_unused;
 spinlock_t s_inode_lru_lock ;
 struct list_head s_inode_lru;
 int s_nr_inodes_unused;
 struct block_device *s_bdev;
 struct backing_dev_info *s_bdi;
 struct mtd_info *s_mtd;
 struct hlist_node s_instances;
 struct quota_info s_dquot;
 struct sb_writers s_writers;
 char s_id[32];
 u8 s_uuid[16];
 void *s_fs_info;
 unsigned int s_max_links;
 fmode_t s_mode;
 u32 s_time_gran;
 struct mutex s_vfs_rename_mutex;
 char *s_subtype;
 char *s_options;
 const struct dentry_operations *s_d_op;
 int cleancache_poolid;
 struct shrinker s_shrink;
 atomic_long_t s_remove_count;
 int s_readonly_remount;
 struct workqueue_struct *s_dio_done_wq;
 int s_stack_depth;
};
void __sb_end_write(struct super_block *sb, int level);
int __sb_start_write(struct super_block *sb, int level, bool wait);
struct fiemap_extent_info {
 unsigned int fi_flags;
 unsigned int fi_extents_mapped;
 unsigned int fi_extents_max;
 struct fiemap_extent *fi_extents_start;
};
int fiemap_fill_next_extent(struct fiemap_extent_info *info, u64 logical, u64 phys, u64 len, u32 flags);
int fiemap_check_flags(struct fiemap_extent_info *fieinfo, u32 fs_flags);
typedef int (*filldir_t)(void *, const char *, int, loff_t, u64, unsigned);
struct dir_context {
 const filldir_t actor;
 loff_t pos;
};
struct block_device_operations;
struct file_operations {
 struct module *owner;
 loff_t (*llseek) (struct file *, loff_t, int);
 ssize_t (*read) (struct file *, char *, size_t, loff_t *);
 ssize_t (*write) (struct file *, const char *, size_t, loff_t *);
 ssize_t (*aio_read) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
 ssize_t (*aio_write) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
 int (*readdir) (struct file *, void *, filldir_t);
  //@@@@int (*iterate) (struct file *, struct dir_context *);
 unsigned int (*poll) (struct file *, struct poll_table_struct *);
 long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
 long (*compat_ioctl) (struct file *, unsigned int, unsigned long);
 int (*mmap) (struct file *, struct vm_area_struct *);
 int (*open) (struct inode *, struct file *);
 int (*flush) (struct file *, fl_owner_t id);
 int (*release) (struct inode *, struct file *);
 int (*fsync) (struct file *, loff_t, loff_t, int datasync);
 int (*aio_fsync) (struct kiocb *, int datasync);
 int (*fasync) (int, struct file *, int);
 int (*lock) (struct file *, int, struct file_lock *);
 ssize_t (*sendpage) (struct file *, struct page *, int, size_t, loff_t *, int);
 unsigned long (*get_unmapped_area)(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
 int (*check_flags)(int);
 int (*flock) (struct file *, int, struct file_lock *);
 ssize_t (*splice_write)(struct pipe_inode_info *, struct file *, loff_t *, size_t, unsigned int);
 ssize_t (*splice_read)(struct file *, loff_t *, struct pipe_inode_info *, size_t, unsigned int);
 int (*setlease)(struct file *, long, struct file_lock **);
 long (*fallocate)(struct file *file, int mode, loff_t offset, loff_t len);
 int (*show_fdinfo)(struct seq_file *m, struct file *f);
};
struct inode_operations {
 struct dentry * (*lookup) (struct inode *,struct dentry *, unsigned int);
 void * (*follow_link) (struct dentry *, struct nameidata *);
 int (*permission) (struct inode *, int);
 struct posix_acl * (*get_acl)(struct inode *, int);
 int (*readlink) (struct dentry *, char *,int);
 void (*put_link) (struct dentry *, struct nameidata *, void *);
 int (*create) (struct inode *,struct dentry *, umode_t, bool);
 int (*link) (struct dentry *,struct inode *,struct dentry *);
 int (*unlink) (struct inode *,struct dentry *);
 int (*symlink) (struct inode *,struct dentry *,const char *);
 int (*mkdir) (struct inode *,struct dentry *,umode_t);
 int (*rmdir) (struct inode *,struct dentry *);
 int (*mknod) (struct inode *,struct dentry *,umode_t,dev_t);
 int (*rename) (struct inode *, struct dentry *, struct inode *, struct dentry *);
 int (*setattr) (struct dentry *, struct iattr *);
 int (*getattr) (struct vfsmount *mnt, struct dentry *, struct kstat *);
 int (*setxattr) (struct dentry *, const char *,const void *,size_t,int);
 ssize_t (*getxattr) (struct dentry *, const char *, void *, size_t);
 ssize_t (*listxattr) (struct dentry *, char *, size_t);
 int (*removexattr) (struct dentry *, const char *);
 int (*fiemap)(struct inode *, struct fiemap_extent_info *, u64 start, u64 len);
 int (*update_time)(struct inode *, struct timespec *, int);
 int (*atomic_open)(struct inode *, struct dentry *, struct file *, unsigned open_flag, umode_t create_mode, int *opened);
} ;
ssize_t rw_copy_check_uvector(int type, const struct iovec * uvector, unsigned long nr_segs, unsigned long fast_segs, struct iovec *fast_pointer, struct iovec **ret_pointer);
struct super_operations {
    struct inode *(*alloc_inode)(struct super_block *sb);
 void (*destroy_inode)(struct inode *);
    void (*dirty_inode) (struct inode *, int flags);
 int (*write_inode) (struct inode *, struct writeback_control *wbc);
 int (*drop_inode) (struct inode *);
 void (*evict_inode) (struct inode *);
 void (*put_super) (struct super_block *);
 int (*sync_fs)(struct super_block *sb, int wait);
 int (*freeze_fs) (struct super_block *);
 int (*unfreeze_fs) (struct super_block *);
 int (*statfs) (struct dentry *, struct kstatfs *);
 int (*remount_fs) (struct super_block *, int *, char *);
 void (*umount_begin) (struct super_block *);
 int (*show_options)(struct seq_file *, struct dentry *);
 int (*show_devname)(struct seq_file *, struct dentry *);
 int (*show_path)(struct seq_file *, struct dentry *);
 int (*show_stats)(struct seq_file *, struct dentry *);
 ssize_t (*quota_read)(struct super_block *, int, char *, size_t, loff_t);
 ssize_t (*quota_write)(struct super_block *, int, const char *, size_t, loff_t);
 int (*bdev_try_to_free_page)(struct super_block*, struct page*, gfp_t);
 int (*nr_cached_objects)(struct super_block *);
 void (*free_cached_objects)(struct super_block *, int);
};
enum file_time_flags {
 S_ATIME = 1, S_MTIME = 2, S_CTIME = 4, S_VERSION = 8, };
int sync_inode(struct inode *inode, struct writeback_control *wbc);
int sync_inode_metadata(struct inode *inode, int wait);
struct file_system_type {
 const char *name;
 int fs_flags;
 struct dentry *(*mount) (struct file_system_type *, int, const char *, void *);
 void (*kill_sb) (struct super_block *);
 struct module *owner;
 struct file_system_type * next;
 struct hlist_head fs_supers;
 struct lock_class_key s_lock_key;
 struct lock_class_key s_umount_key;
 struct lock_class_key s_vfs_rename_key;
 struct lock_class_key s_writers_key[(SB_FREEZE_COMPLETE - 1)];
 struct lock_class_key i_lock_key;
 struct lock_class_key i_mutex_key;
 struct lock_class_key i_mutex_dir_key;
};
void generic_shutdown_super(struct super_block *sb);
void kill_block_super(struct super_block *sb);
void kill_anon_super(struct super_block *sb);
void kill_litter_super(struct super_block *sb);
void deactivate_super(struct super_block *sb);
void deactivate_locked_super(struct super_block *sb);
int set_anon_super(struct super_block *s, void *data);
int get_anon_bdev(dev_t *);
void free_anon_bdev(dev_t);
struct super_block *sget(struct file_system_type *type, int (*test)(struct super_block *,void *), int (*set)(struct super_block *,void *), int flags, void *data);
struct audit_names;
struct filename {
 const char *name;
 const char *uptr;
 struct audit_names *aname;
 bool separate;
};
enum {
 FILE_CREATED = 1, FILE_OPENED = 2
};
unsigned long invalidate_mapping_pages(struct address_space *mapping, unsigned long start, unsigned long end);
;
int generic_write_checks(struct file *file, loff_t *pos, size_t *count, int isblk);
extern void
file_ra_state_init(struct file_ra_state *ra, struct address_space *mapping);
typedef void (dio_submit_t)(int rw, struct bio *bio, struct inode *inode, loff_t file_offset);
enum {
 DIO_LOCKING = 0x01, DIO_SKIP_HOLES = 0x02, };
void dio_end_io(struct bio *bio, int error);
ssize_t __blockdev_direct_IO(int rw, struct kiocb *iocb, struct inode *inode, struct block_device *bdev, const struct iovec *iov, loff_t offset, unsigned long nr_segs, get_block_t get_block, dio_iodone_t end_io, dio_submit_t submit_io, int flags);
void inode_dio_wait(struct inode *inode);
void inode_dio_done(struct inode *inode);
void __inode_add_bytes(struct inode *inode, loff_t bytes);
void inode_add_bytes(struct inode *inode, loff_t bytes);
void __inode_sub_bytes(struct inode *inode, loff_t bytes);
void inode_sub_bytes(struct inode *inode, loff_t bytes);
loff_t inode_get_bytes(struct inode *inode);
void inode_set_bytes(struct inode *inode, loff_t bytes);
struct tree_descr { char *name; const struct file_operations *ops; int mode; };
struct dentry *d_alloc_name(struct dentry *, const char *);
struct simple_transaction_argresp {
 ssize_t size;
 char data[0];
};
char *simple_transaction_get(struct file *file, const char *buf, size_t size);
ssize_t simple_transaction_read(struct file *file, char *buf, size_t size, loff_t *pos);
int simple_transaction_release(struct inode *inode, struct file *file);
void simple_transaction_set(struct file *file, size_t n);
int simple_attr_open(struct inode *inode, struct file *file, int (*get)(void *, u64 *), int (*set)(void *, u64), const char *fmt);
int simple_attr_release(struct inode *inode, struct file *file);
ssize_t simple_attr_read(struct file *file, char *buf, size_t len, loff_t *ppos);
ssize_t simple_attr_write(struct file *file, const char *buf, size_t len, loff_t *ppos);
struct ctl_table;
int proc_nr_files(struct ctl_table *table, int write, void *buffer, size_t *lenp, loff_t *ppos);
int proc_nr_dentry(struct ctl_table *table, int write, void *buffer, size_t *lenp, loff_t *ppos);
int proc_nr_inodes(struct ctl_table *table, int write, void *buffer, size_t *lenp, loff_t *ppos);
int get_filesystem_list(char *buf);
struct sched_param {
 int sched_priority;
};
enum page_debug_flags {
 PAGE_DEBUG_FLAG_POISON, PAGE_DEBUG_FLAG_GUARD, };
struct vm_area_struct;
struct mm_struct;
struct inode;
enum uprobe_filter_ctx {
 UPROBE_FILTER_REGISTER, UPROBE_FILTER_UNREGISTER, UPROBE_FILTER_MMAP, };
struct uprobe_consumer {
 int (*handler)(struct uprobe_consumer *self, struct pt_regs *regs);
 int (*ret_handler)(struct uprobe_consumer *self, unsigned long func, struct pt_regs *regs);
 bool (*filter)(struct uprobe_consumer *self, enum uprobe_filter_ctx ctx, struct mm_struct *mm);
 struct uprobe_consumer *next;
};
struct uprobes_state {
};
typedef struct {
 atomic64_t id;
 unsigned int vmalloc_seq;
 unsigned long sigpage;
} mm_context_t;
struct address_space;
struct page {
 unsigned long flags;
 struct address_space *mapping;
 struct {
  union {
   unsigned long index;
   void *freelist;
   bool pfmemalloc;
  };
  union {
   unsigned counters;
   struct {
    union {
     atomic_t _mapcount;
     struct {
      unsigned inuse:16;
      unsigned objects:15;
      unsigned frozen:1;
     };
     int units;
    };
    atomic_t _count;
   };
  };
 };
 union {
  struct list_head lru;
  struct {
   struct page *next;
   short int pages;
   short int pobjects;
  };
  struct list_head list;
  struct slab *slab_page;
 };
 union {
  unsigned long private;
  spinlock_t ptl;
  struct kmem_cache *slab_cache;
  struct page *first_page;
 };
}
;
struct page_frag {
 struct page *page;
 __u16 offset;
 __u16 size;
};
typedef unsigned long vm_flags_t;
struct vm_region {
 struct rb_node vm_rb;
 vm_flags_t vm_flags;
 unsigned long vm_start;
 unsigned long vm_end;
 unsigned long vm_top;
 unsigned long vm_pgoff;
 struct file *vm_file;
 int vm_usage;
 bool vm_icache_flushed : 1;
};
struct vm_area_struct {
 unsigned long vm_start;
 unsigned long vm_end;
 struct vm_area_struct *vm_next, *vm_prev;
 struct rb_node vm_rb;
 unsigned long rb_subtree_gap;
 struct mm_struct *vm_mm;
 pgprot_t vm_page_prot;
 unsigned long vm_flags;
 union {
  struct {
   struct rb_node rb;
   unsigned long rb_subtree_last;
  } linear;
  struct list_head nonlinear;
  const char *anon_name;
 } shared;
 struct list_head anon_vma_chain;
 struct anon_vma *anon_vma;
 const struct vm_operations_struct *vm_ops;
 unsigned long vm_pgoff;
 struct file * vm_file;
 void * vm_private_data;
};
struct core_thread {
 struct task_struct *task;
 struct core_thread *next;
};
struct core_state {
 atomic_t nr_threads;
 struct core_thread dumper;
 struct completion startup;
};
enum {
 MM_FILEPAGES, MM_ANONPAGES, MM_SWAPENTS, NR_MM_COUNTERS
};
struct task_rss_stat {
 int events;
 int count[NR_MM_COUNTERS];
};
struct mm_rss_stat {
 atomic_long_t count[NR_MM_COUNTERS];
};
struct mm_struct {
 struct vm_area_struct * mmap;
 struct rb_root mm_rb;
 struct vm_area_struct * mmap_cache;
 unsigned long (*get_unmapped_area) (struct file *filp, unsigned long addr, unsigned long len, unsigned long pgoff, unsigned long flags);
 void (*unmap_area) (struct mm_struct *mm, unsigned long addr);
 unsigned long mmap_base;
 unsigned long mmap_legacy_base;
 unsigned long task_size;
 unsigned long cached_hole_size;
 unsigned long free_area_cache;
 unsigned long highest_vm_end;
 pgd_t * pgd;
 atomic_t mm_users;
 atomic_t mm_count;
 int map_count;
 spinlock_t page_table_lock;
 struct rw_semaphore mmap_sem;
 struct list_head mmlist;
 unsigned long hiwater_rss;
 unsigned long hiwater_vm;
 unsigned long total_vm;
 unsigned long locked_vm;
 unsigned long pinned_vm;
 unsigned long shared_vm;
 unsigned long exec_vm;
 unsigned long stack_vm;
 unsigned long def_flags;
 unsigned long nr_ptes;
 unsigned long start_code, end_code, start_data, end_data;
 unsigned long start_brk, brk, start_stack;
 unsigned long arg_start, arg_end, env_start, env_end;
 unsigned long saved_auxv[(2*(0 + 20 + 1))];
 struct mm_rss_stat rss_stat;
 struct linux_binfmt *binfmt;
 cpumask_var_t cpu_vm_mask_var;
 mm_context_t context;
 unsigned long flags;
 struct core_state *core_state;
 spinlock_t ioctx_lock;
 struct hlist_head ioctx_list;
 struct task_struct *owner;
 struct file *exe_file;
 bool tlb_flush_pending;
 struct uprobes_state uprobes_state;
};
typedef unsigned long cputime_t;
typedef u64 cputime64_t;
struct ipc_perm
{
 __kernel_key_t key;
 __kernel_uid_t uid;
 __kernel_gid_t gid;
 __kernel_uid_t cuid;
 __kernel_gid_t cgid;
 __kernel_mode_t mode;
 unsigned short seq;
};
struct ipc64_perm {
 __kernel_key_t key;
 __kernel_uid32_t uid;
 __kernel_gid32_t gid;
 __kernel_uid32_t cuid;
 __kernel_gid32_t cgid;
 __kernel_mode_t mode;
 unsigned char __pad1[4 - sizeof(__kernel_mode_t)];
 unsigned short seq;
 unsigned short __pad2;
 unsigned long __unused1;
 unsigned long __unused2;
};
struct ipc_kludge {
 struct msgbuf *msgp;
 long msgtyp;
};
struct kern_ipc_perm
{
 spinlock_t lock;
 int deleted;
 int id;
 key_t key;
 kuid_t uid;
 kgid_t gid;
 kuid_t cuid;
 kgid_t cgid;
 umode_t mode;
 unsigned long seq;
 void *security;
};
struct semid_ds {
 struct ipc_perm sem_perm;
 __kernel_time_t sem_otime;
 __kernel_time_t sem_ctime;
 struct sem *sem_base;
 struct sem_queue *sem_pending;
 struct sem_queue **sem_pending_last;
 struct sem_undo *undo;
 unsigned short sem_nsems;
};
struct semid64_ds {
 struct ipc64_perm sem_perm;
 __kernel_time_t sem_otime;
 unsigned long __unused1;
 __kernel_time_t sem_ctime;
 unsigned long __unused2;
 unsigned long sem_nsems;
 unsigned long __unused3;
 unsigned long __unused4;
};
struct sembuf {
 unsigned short sem_num;
 short sem_op;
 short sem_flg;
};
union semun {
 int val;
 struct semid_ds *buf;
 unsigned short *array;
 struct seminfo *__buf;
 void *__pad;
};
struct seminfo {
 int semmap;
 int semmni;
 int semmns;
 int semmnu;
 int semmsl;
 int semopm;
 int semume;
 int semusz;
 int semvmx;
 int semaem;
};
struct task_struct;
struct sem_array {
 struct kern_ipc_perm
    sem_perm;
 time_t sem_ctime;
 struct sem *sem_base;
 struct list_head pending_alter;
 struct list_head pending_const;
 struct list_head list_id;
 int sem_nsems;
 int complex_count;
};
struct sysv_sem {
 struct sem_undo_list *undo_list;
};
struct siginfo;
typedef void __signalfn_t(int);
typedef __signalfn_t *__sighandler_t;
typedef void __restorefn_t(void);
typedef __restorefn_t *__sigrestore_t;
typedef struct sigaltstack {
 void *ss_sp;
 int ss_flags;
 size_t ss_size;
} stack_t;
typedef unsigned long old_sigset_t;
typedef struct {
 unsigned long sig[(64 / 32)];
} sigset_t;
struct sigcontext {
 unsigned long trap_no;
 unsigned long error_code;
 unsigned long oldmask;
 unsigned long arm_r0;
 unsigned long arm_r1;
 unsigned long arm_r2;
 unsigned long arm_r3;
 unsigned long arm_r4;
 unsigned long arm_r5;
 unsigned long arm_r6;
 unsigned long arm_r7;
 unsigned long arm_r8;
 unsigned long arm_r9;
 unsigned long arm_r10;
 unsigned long arm_fp;
 unsigned long arm_ip;
 unsigned long arm_sp;
 unsigned long arm_lr;
 unsigned long arm_pc;
 unsigned long arm_cpsr;
 unsigned long fault_address;
};
typedef union sigval {
 int sival_int;
 void *sival_ptr;
} sigval_t;
typedef struct siginfo {
 int si_signo;
 int si_errno;
 int si_code;
 union {
  int _pad[((128 - (3 * sizeof(int))) / sizeof(int))];
  struct {
   __kernel_pid_t _pid;
   __kernel_uid32_t _uid;
  } _kill;
  struct {
   __kernel_timer_t _tid;
   int _overrun;
   char _pad[sizeof( __kernel_uid32_t) - sizeof(int)];
   sigval_t _sigval;
   int _sys_private;
  } _timer;
  struct {
   __kernel_pid_t _pid;
   __kernel_uid32_t _uid;
   sigval_t _sigval;
  } _rt;
  struct {
   __kernel_pid_t _pid;
   __kernel_uid32_t _uid;
   int _status;
   __kernel_clock_t _utime;
   __kernel_clock_t _stime;
  } _sigchld;
  struct {
   void *_addr;
   short _addr_lsb;
  } _sigfault;
  struct {
   long _band;
   int _fd;
  } _sigpoll;
  struct {
   void *_call_addr;
   int _syscall;
   unsigned int _arch;
  } _sigsys;
 } _sifields;
} siginfo_t;
typedef struct sigevent {
 sigval_t sigev_value;
 int sigev_signo;
 int sigev_notify;
 union {
  int _pad[((64 - (sizeof(int) * 2 + sizeof(sigval_t))) / sizeof(int))];
   int _tid;
  struct {
   void (*_function)(sigval_t);
   void *_attribute;
  } _sigev_thread;
 } _sigev_un;
} sigevent_t;
struct siginfo;
void do_schedule_next_timer(struct siginfo *info);
struct task_struct;
struct sigqueue {
 struct list_head list;
 int flags;
 siginfo_t info;
 struct user_struct *user;
};
struct sigpending {
 struct list_head list;
 sigset_t signal;
};




struct timespec;
struct pt_regs;
struct sigaction {
 __sighandler_t sa_handler;
 unsigned long sa_flags;
 __sigrestore_t sa_restorer;
 sigset_t sa_mask;
};
struct k_sigaction {
 struct sigaction sa;
};
struct old_sigaction {
 __sighandler_t sa_handler;
 old_sigset_t sa_mask;
 unsigned long sa_flags;
 __sigrestore_t sa_restorer;
};
struct ksignal {
 struct k_sigaction ka;
 siginfo_t info;
 int sig;
};
int unhandled_signal(struct task_struct *tsk, int sig);
void signals_init(void);
int restore_altstack(const stack_t *);
int __save_altstack(stack_t *, unsigned long);
struct seq_file;
struct prop_global {
 int shift;
 struct percpu_counter events;
};
struct prop_descriptor {
 int index;
 struct prop_global pg[2];
 struct mutex mutex;
};
int prop_descriptor_init(struct prop_descriptor *pd, int shift);
void prop_change_shift(struct prop_descriptor *pd, int new_shift);
struct prop_local_percpu {
 struct percpu_counter events;
 int shift;
 unsigned long period;
 raw_spinlock_t lock;
};
int prop_local_init_percpu(struct prop_local_percpu *pl);
void prop_local_destroy_percpu(struct prop_local_percpu *pl);
void __prop_inc_percpu(struct prop_descriptor *pd, struct prop_local_percpu *pl);
void prop_fraction_percpu(struct prop_descriptor *pd, struct prop_local_percpu *pl, long *numerator, long *denominator);
void __prop_inc_percpu_max(struct prop_descriptor *pd, struct prop_local_percpu *pl, long frac);
struct prop_local_single {
 unsigned long events;
 unsigned long period;
 int shift;
 raw_spinlock_t lock;
};
int prop_local_init_single(struct prop_local_single *pl);
void prop_local_destroy_single(struct prop_local_single *pl);
void __prop_inc_single(struct prop_descriptor *pd, struct prop_local_single *pl);
void prop_fraction_single(struct prop_descriptor *pd, struct prop_local_single *pl, long *numerator, long *denominator);
struct seccomp_data {
 int nr;
 __u32 arch;
 __u64 instruction_pointer;
 __u64 args[6];
};
struct seccomp_filter;
struct seccomp {
 int mode;
 struct seccomp_filter *filter;
};
struct plist_head {
 struct list_head node_list;
};
struct plist_node {
 int prio;
 struct list_head prio_list;
 struct list_head node_list;
};
struct rt_mutex {
 raw_spinlock_t wait_lock;
 struct plist_head wait_list;
 struct task_struct *owner;
};
struct rt_mutex_waiter;
struct hrtimer_sleeper;
 static inline int rt_mutex_debug_check_no_locks_freed(const void *from, unsigned long len)
 {
 return 0;
 }
struct rusage {
 struct timeval ru_utime;
 struct timeval ru_stime;
 long ru_maxrss;
 long ru_ixrss;
 long ru_idrss;
 long ru_isrss;
 long ru_minflt;
 long ru_majflt;
 long ru_nswap;
 long ru_inblock;
 long ru_oublock;
 long ru_msgsnd;
 long ru_msgrcv;
 long ru_nsignals;
 long ru_nvcsw;
 long ru_nivcsw;
};
struct rlimit {
 unsigned long rlim_cur;
 unsigned long rlim_max;
};
struct rlimit64 {
 __u64 rlim_cur;
 __u64 rlim_max;
};
struct task_struct;
int getrusage(struct task_struct *p, int who, struct rusage *ru);
int do_prlimit(struct task_struct *tsk, unsigned int resource, struct rlimit *new_rlim, struct rlimit *old_rlim);
struct timerqueue_node {
 struct rb_node node;
 ktime_t expires;
};
struct timerqueue_head {
 struct rb_root head;
 struct timerqueue_node *next;
};
struct hrtimer_clock_base;
struct hrtimer_cpu_base;
enum hrtimer_mode {
 HRTIMER_MODE_ABS = 0x0, HRTIMER_MODE_REL = 0x1, HRTIMER_MODE_PINNED = 0x02, HRTIMER_MODE_ABS_PINNED = 0x02, HRTIMER_MODE_REL_PINNED = 0x03, };
enum hrtimer_restart {
 HRTIMER_NORESTART, HRTIMER_RESTART, };
struct hrtimer {
 struct timerqueue_node node;
 ktime_t _softexpires;
 enum hrtimer_restart (*function)(struct hrtimer *);
 struct hrtimer_clock_base *base;
 unsigned long state;
 int start_pid;
 void *start_site;
 char start_comm[16];
};
struct hrtimer_sleeper {
 struct hrtimer timer;
 struct task_struct *task;
};
struct hrtimer_clock_base {
 struct hrtimer_cpu_base *cpu_base;
 int index;
 clockid_t clockid;
 struct timerqueue_head active;
 ktime_t resolution;
 ktime_t (*get_time)(void);
 ktime_t softirq_time;
 ktime_t offset;
};
enum hrtimer_base_type {
 HRTIMER_BASE_MONOTONIC, HRTIMER_BASE_REALTIME, HRTIMER_BASE_BOOTTIME, HRTIMER_BASE_TAI, HRTIMER_MAX_CLOCK_BASES, };
struct hrtimer_cpu_base {
 raw_spinlock_t lock;
 unsigned int active_bases;
 unsigned int clock_was_set;
 ktime_t expires_next;
 int hres_active;
 int hang_detected;
 unsigned long nr_events;
 unsigned long nr_retries;
 unsigned long nr_hangs;
 ktime_t max_hang_time;
 struct hrtimer_clock_base clock_base[HRTIMER_MAX_CLOCK_BASES];
};
struct clock_event_device;

extern int
__hrtimer_start_range_ns(struct hrtimer *timer, ktime_t tim, unsigned long delta_ns, const enum hrtimer_mode mode, int wakeup);
extern u64
hrtimer_forward(struct hrtimer *timer, ktime_t now, ktime_t interval);
struct task_io_accounting {
 u64 rchar;
 u64 wchar;
 u64 syscr;
 u64 syscw;
 u64 read_bytes;
 u64 write_bytes;
 u64 cancelled_write_bytes;
};
struct task_struct;
typedef int32_t key_serial_t;
typedef uint32_t key_perm_t;
struct key;
struct seq_file;
struct user_struct;
struct signal_struct;
struct cred;
struct key_type;
struct key_owner;
struct keyring_list;
struct keyring_name;
typedef struct __key_reference_with_attributes *key_ref_t;
struct key {
 atomic_t usage;
 key_serial_t serial;
 union {
  struct list_head graveyard_link;
  struct rb_node serial_node;
 };
 struct key_type *type;
 struct rw_semaphore sem;
 struct key_user *user;
 void *security;
 union {
  time_t expiry;
  time_t revoked_at;
 };
 time_t last_used_at;
 kuid_t uid;
 kgid_t gid;
 key_perm_t perm;
 unsigned short quotalen;
 unsigned short datalen;
 unsigned long flags;
 char *description;
 union {
  struct list_head link;
  unsigned long x[2];
  void *p[2];
  int reject_error;
 } type_data;
 union {
  unsigned long value;
  void *rcudata;
  void *data;
  struct keyring_list *subscriptions;
 } payload;
};
struct selinux_audit_rule;
struct audit_context;
struct kern_ipc_perm;
bool selinux_is_enabled(void);
struct user_struct;
struct cred;
struct inode;
struct group_info {
 atomic_t usage;
 int ngroups;
 int nblocks;
 kgid_t small_block[32];
 kgid_t *blocks[0];
};
struct cred {
 atomic_t usage;
 kuid_t uid;
 kgid_t gid;
 kuid_t suid;
 kgid_t sgid;
 kuid_t euid;
 kgid_t egid;
 kuid_t fsuid;
 kgid_t fsgid;
 unsigned securebits;
 kernel_cap_t cap_inheritable;
 kernel_cap_t cap_permitted;
 kernel_cap_t cap_effective;
 kernel_cap_t cap_bset;
 kernel_cap_t cap_ambient;
 unsigned char jit_keyring;
 struct key *session_keyring;
 struct key *process_keyring;
 struct key *thread_keyring;
 struct key *request_key_auth;
 void *security;
 struct user_struct *user;
 struct user_namespace *user_ns;
 struct group_info *group_info;
 struct callback_head rcu;
};
struct exec_domain;
struct futex_pi_state;
struct robust_list_head;
struct bio_list;
struct fs_struct;
struct perf_event_context;
struct blk_plug;
struct task_migration_notifier {
 struct task_struct *task;
 int from_cpu;
 int to_cpu;
};
struct seq_file;
struct cfs_rq;
struct task_group;
extern void
print_cfs_rq(struct seq_file *m, int cpu, struct cfs_rq *cfs_rq);
struct task_struct;
void io_schedule(void);
long io_schedule_timeout(long timeout);
void lockup_detector_init(void);
 void schedule(void);
struct nsproxy;
struct user_namespace;
extern unsigned long
arch_get_unmapped_area(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
extern unsigned long
arch_get_unmapped_area_topdown(struct file *filp, unsigned long addr, unsigned long len, unsigned long pgoff, unsigned long flags);
struct sighand_struct {
 atomic_t count;
 struct k_sigaction action[64];
 spinlock_t siglock;
 wait_queue_head_t signalfd_wqh;
};
struct pacct_struct {
 int ac_flag;
 long ac_exitcode;
 unsigned long ac_mem;
 cputime_t ac_utime, ac_stime;
 unsigned long ac_minflt, ac_majflt;
};
struct cpu_itimer {
 cputime_t expires;
 cputime_t incr;
 u32 error;
 u32 incr_error;
};
struct cputime {
 cputime_t utime;
 cputime_t stime;
};
struct task_cputime {
 cputime_t utime;
 cputime_t stime;
 unsigned long long sum_exec_runtime;
};
struct thread_group_cputimer {
 struct task_cputime cputime;
 int running;
 raw_spinlock_t lock;
};
struct autogroup;
struct signal_struct {
 atomic_t sigcnt;
 atomic_t live;
 int nr_threads;
 struct list_head thread_head;
 wait_queue_head_t wait_chldexit;
 struct task_struct *curr_target;
 struct sigpending shared_pending;
 int group_exit_code;
 int notify_count;
 struct task_struct *group_exit_task;
 int group_stop_count;
 unsigned int flags;
 unsigned int is_child_subreaper:1;
 unsigned int has_child_subreaper:1;
 int posix_timer_id;
 struct list_head posix_timers;
 struct hrtimer real_timer;
 struct pid *leader_pid;
 ktime_t it_real_incr;
 struct cpu_itimer it[2];
 struct thread_group_cputimer cputimer;
 struct task_cputime cputime_expires;
 struct list_head cpu_timers[3];
 struct pid *tty_old_pgrp;
 int leader;
 struct tty_struct *tty;
 cputime_t utime, stime, cutime, cstime;
 cputime_t gtime;
 cputime_t cgtime;
 struct cputime prev_cputime;
 unsigned long nvcsw, nivcsw, cnvcsw, cnivcsw;
 unsigned long min_flt, maj_flt, cmin_flt, cmaj_flt;
 unsigned long inblock, oublock, cinblock, coublock;
 unsigned long maxrss, cmaxrss;
 struct task_io_accounting ioac;
 unsigned long long sum_sched_runtime;
 struct rlimit rlim[16];
 struct taskstats *stats;
 unsigned audit_tty;
 unsigned audit_tty_log_passwd;
 struct tty_audit_buf *tty_audit_buf;
 struct rw_semaphore group_rwsem;
 oom_flags_t oom_flags;
 short oom_score_adj;
 short oom_score_adj_min;
 struct mutex cred_guard_mutex;
};
struct user_struct {
 atomic_t __count;
 atomic_t processes;
 atomic_t files;
 atomic_t sigpending;
 atomic_t inotify_watches;
 atomic_t inotify_devs;
 atomic_long_t epoll_watches;
 unsigned long locked_shm;
 unsigned long unix_inflight;
 atomic_long_t pipe_bufs;
 struct key *uid_keyring;
 struct key *session_keyring;
 struct hlist_node uidhash_node;
 kuid_t uid;
 atomic_long_t locked_vm;
};
struct backing_dev_info;
struct reclaim_state;
struct sched_info {
 unsigned long pcount;
 unsigned long long run_delay;
 unsigned long long last_arrival, last_queued;
};
struct task_delay_info {
 spinlock_t lock;
 unsigned int flags;
 struct timespec blkio_start, blkio_end;
 u64 blkio_delay;
 u64 swapin_delay;
 u32 blkio_count;
 u32 swapin_count;
 struct timespec freepages_start, freepages_end;
 u64 freepages_delay;
 u32 freepages_count;
};
enum cpu_idle_type {
 CPU_IDLE, CPU_NOT_IDLE, CPU_NEWLY_IDLE, CPU_MAX_IDLE_TYPES
};
struct sched_domain_attr {
 int relax_domain_level;
};
struct sched_group;
struct sched_domain {
 struct sched_domain *parent;
 struct sched_domain *child;
 struct sched_group *groups;
 unsigned long min_interval;
 unsigned long max_interval;
 unsigned int busy_factor;
 unsigned int imbalance_pct;
 unsigned int cache_nice_tries;
 unsigned int busy_idx;
 unsigned int idle_idx;
 unsigned int newidle_idx;
 unsigned int wake_idx;
 unsigned int forkexec_idx;
 unsigned int smt_gain;
 int nohz_idle;
 int flags;
 int level;
 unsigned long last_balance;
 unsigned int balance_interval;
 unsigned int nr_balance_failed;
 u64 last_update;
 unsigned int lb_count[CPU_MAX_IDLE_TYPES];
 unsigned int lb_failed[CPU_MAX_IDLE_TYPES];
 unsigned int lb_balanced[CPU_MAX_IDLE_TYPES];
 unsigned int lb_imbalance[CPU_MAX_IDLE_TYPES];
 unsigned int lb_gained[CPU_MAX_IDLE_TYPES];
 unsigned int lb_hot_gained[CPU_MAX_IDLE_TYPES];
 unsigned int lb_nobusyg[CPU_MAX_IDLE_TYPES];
 unsigned int lb_nobusyq[CPU_MAX_IDLE_TYPES];
 unsigned int alb_count;
 unsigned int alb_failed;
 unsigned int alb_pushed;
 unsigned int sbe_count;
 unsigned int sbe_balanced;
 unsigned int sbe_pushed;
 unsigned int sbf_count;
 unsigned int sbf_balanced;
 unsigned int sbf_pushed;
 unsigned int ttwu_wake_remote;
 unsigned int ttwu_move_affine;
 unsigned int ttwu_move_balance;
 char *name;
 union {
  void *private;
  struct callback_head rcu;
 };
 unsigned int span_weight;
 unsigned long span[0];
};
cpumask_var_t *alloc_sched_domains(unsigned int ndoms);
void free_sched_domains(cpumask_var_t doms[], unsigned int ndoms);
bool cpus_share_cache(int this_cpu, int that_cpu);
struct io_context;

struct audit_context;
struct mempolicy;
struct pipe_inode_info;
struct uts_namespace;
struct load_weight {
 unsigned long weight, inv_weight;
};
struct sched_avg {
 u32 runnable_avg_sum, runnable_avg_period;
 u64 last_runnable_update;
 s64 decay_count;
 unsigned long load_avg_contrib;
 unsigned long load_avg_ratio;
 u32 usage_avg_sum;
};
struct sched_statistics {
 u64 wait_start;
 u64 wait_max;
 u64 wait_count;
 u64 wait_sum;
 u64 iowait_count;
 u64 iowait_sum;
 u64 sleep_start;
 u64 sleep_max;
 s64 sum_sleep_runtime;
 u64 block_start;
 u64 block_max;
 u64 exec_max;
 u64 slice_max;
 u64 nr_migrations_cold;
 u64 nr_failed_migrations_affine;
 u64 nr_failed_migrations_running;
 u64 nr_failed_migrations_hot;
 u64 nr_forced_migrations;
 u64 nr_wakeups;
 u64 nr_wakeups_sync;
 u64 nr_wakeups_migrate;
 u64 nr_wakeups_local;
 u64 nr_wakeups_remote;
 u64 nr_wakeups_affine;
 u64 nr_wakeups_affine_attempts;
 u64 nr_wakeups_passive;
 u64 nr_wakeups_idle;
};
struct sched_entity {
 struct load_weight load;
 struct rb_node run_node;
 struct list_head group_node;
 unsigned int on_rq;
 u64 exec_start;
 u64 sum_exec_runtime;
 u64 vruntime;
 u64 prev_sum_exec_runtime;
 u64 nr_migrations;
 struct sched_statistics statistics;
 struct sched_entity *parent;
 struct cfs_rq *cfs_rq;
 struct cfs_rq *my_q;
 struct sched_avg avg;
};
struct sched_rt_entity {
 struct list_head run_list;
 unsigned long timeout;
 unsigned long watchdog_stamp;
 unsigned int time_slice;
 struct sched_rt_entity *back;
 struct sched_rt_entity *parent;
 struct rt_rq *rt_rq;
 struct rt_rq *my_q;
};
struct rcu_node;
enum perf_event_task_context {
 perf_invalid_context = -1, perf_hw_context = 0, perf_sw_context, perf_nr_task_contexts, };
struct task_struct {
 volatile long state;
 void *stack;
 atomic_t usage;
 unsigned int flags;
 unsigned int ptrace;
 struct llist_node wake_entry;
 int on_cpu;
 int on_rq;
 int prio, static_prio, normal_prio;
 unsigned int rt_priority;
 const struct sched_class *sched_class;
 struct sched_entity se;
 struct sched_rt_entity rt;
 struct task_group *sched_task_group;
 unsigned char fpu_counter;
 unsigned int policy;
 int nr_cpus_allowed;
 cpumask_t cpus_allowed;
 int rcu_read_lock_nesting;
 char rcu_read_unlock_special;
 struct list_head rcu_node_entry;
 struct rcu_node *rcu_blocked_node;
 struct rt_mutex *rcu_boost_mutex;
 struct sched_info sched_info;
 struct list_head tasks;
 struct plist_node pushable_tasks;
 struct mm_struct *mm, *active_mm;
 unsigned brk_randomized:1;
 struct task_rss_stat rss_stat;
 int exit_state;
 int exit_code, exit_signal;
 int pdeath_signal;
 unsigned int jobctl;
 unsigned int personality;
 unsigned did_exec:1;
 unsigned in_execve:1;
 unsigned in_iowait:1;
 unsigned sched_reset_on_fork:1;
 unsigned sched_contributes_to_load:1;
 unsigned long atomic_flags;
 pid_t pid;
 pid_t tgid;
 struct task_struct *real_parent;
 struct task_struct *parent;
 struct list_head children;
 struct list_head sibling;
 struct task_struct *group_leader;
 struct list_head ptraced;
 struct list_head ptrace_entry;
 struct pid_link pids[PIDTYPE_MAX];
 struct list_head thread_group;
 struct list_head thread_node;
 struct completion *vfork_done;
 int *set_child_tid;
 int *clear_child_tid;
 cputime_t utime, stime, utimescaled, stimescaled;
 cputime_t gtime;
 unsigned long long cpu_power;
 struct cputime prev_cputime;
 unsigned long nvcsw, nivcsw;
 struct timespec start_time;
 struct timespec real_start_time;
 unsigned long min_flt, maj_flt;
 struct task_cputime cputime_expires;
 struct list_head cpu_timers[3];
 const struct cred *real_cred;
 const struct cred *cred;
 char comm[16];
 int link_count, total_link_count;
 struct sysv_sem sysvsem;
 unsigned long last_switch_count;
 struct thread_struct thread;
 struct fs_struct *fs;
 struct files_struct *files;
 struct nsproxy *nsproxy;
 struct signal_struct *signal;
 struct sighand_struct *sighand;
 sigset_t blocked, real_blocked;
 sigset_t saved_sigmask;
 struct sigpending pending;
 unsigned long sas_ss_sp;
 size_t sas_ss_size;
 int (*notifier)(void *priv);
 void *notifier_data;
 sigset_t *notifier_mask;
 struct callback_head *task_works;
 struct audit_context *audit_context;
 kuid_t loginuid;
 unsigned int sessionid;
 struct seccomp seccomp;
    u32 parent_exec_id;
    u32 self_exec_id;
 spinlock_t alloc_lock;
 raw_spinlock_t pi_lock;
 struct plist_head pi_waiters;
 struct rt_mutex_waiter *pi_blocked_on;
 void *journal_info;
 struct bio_list *bio_list;
 struct blk_plug *plug;
 struct reclaim_state *reclaim_state;
 struct backing_dev_info *backing_dev_info;
 struct io_context *io_context;
 unsigned long ptrace_message;
 siginfo_t *last_siginfo;
 struct task_io_accounting ioac;
 u64 acct_rss_mem1;
 u64 acct_vm_mem1;
 cputime_t acct_timexpd;
 struct css_set *cgroups;
 struct list_head cg_list;
 struct robust_list_head *robust_list;
 struct list_head pi_state_list;
 struct futex_pi_state *pi_state_cache;
 struct perf_event_context *perf_event_ctxp[perf_nr_task_contexts];
 struct mutex perf_event_mutex;
 struct list_head perf_event_list;
 struct callback_head rcu;
 struct pipe_inode_info *splice_pipe;
 struct page_frag task_frag;
 struct task_delay_info *delays;
 int nr_dirtied;
 int nr_dirtied_pause;
 unsigned long dirty_paused_when;
 unsigned long timer_slack_ns;
 unsigned long default_timer_slack_ns;
 unsigned long trace;
 unsigned long trace_recursion;
 struct memcg_batch_info {
  int do_batch;
  struct mem_cgroup *memcg;
  unsigned long nr_pages;
  unsigned long memsw_nr_pages;
 } memcg_batch;
 unsigned int memcg_kmem_skip_account;
 struct memcg_oom_info {
  struct mem_cgroup *memcg;
  gfp_t gfp_mask;
  int order;
  unsigned int may_oom:1;
 } memcg_oom;
 atomic_t ptrace_bp_refcnt;
};
struct pid_namespace;
pid_t __task_pid_nr_ns(struct task_struct *task, enum pid_type type, struct pid_namespace *ns);
pid_t task_tgid_nr_ns(struct task_struct *tsk, struct pid_namespace *ns);
void calc_load_enter_idle(void);
void calc_load_exit_idle(void);


extern unsigned long long
task_sched_runtime(struct task_struct *task);





void yield(void);
union thread_union {
 struct thread_info thread_info;
 unsigned long stack[8192/sizeof(long)];
};
 extern void kick_process(struct task_struct *tsk);
struct task_struct *fork_idle(int);
void scheduler_ipi(void);



void thread_group_cputime(struct task_struct *tsk, struct task_cputime *times);
void thread_group_cputimer(struct task_struct *tsk, struct task_cputime *times);
enum {
 DOS_EXTENDED_PARTITION = 5, LINUX_EXTENDED_PARTITION = 0x85, WIN98_EXTENDED_PARTITION = 0x0f, SUN_WHOLE_DISK = DOS_EXTENDED_PARTITION, LINUX_SWAP_PARTITION = 0x82, LINUX_DATA_PARTITION = 0x83, LINUX_LVM_PARTITION = 0x8e, LINUX_RAID_PARTITION = 0xfd, SOLARIS_X86_PARTITION = LINUX_SWAP_PARTITION, NEW_SOLARIS_X86_PARTITION = 0xbf, DM6_AUX1PARTITION = 0x51, DM6_AUX3PARTITION = 0x53, DM6_PARTITION = 0x54, EZD_PARTITION = 0x55, FREEBSD_PARTITION = 0xa5, OPENBSD_PARTITION = 0xa6, NETBSD_PARTITION = 0xa9, BSDI_PARTITION = 0xb7, MINIX_PARTITION = 0x81, UNIXWARE_PARTITION = 0x63, };
struct resource {
 resource_size_t start;
 resource_size_t end;
 const char *name;
 unsigned long flags;
 struct resource *parent, *sibling, *child;
};
void release_child_resources(struct resource *new);
struct resource *lookup_resource(struct resource *root, resource_size_t start);
int adjust_resource(struct resource *res, resource_size_t start, resource_size_t size);
resource_size_t resource_alignment(struct resource *res);
struct device;
extern int
walk_system_ram_range(unsigned long start_pfn, unsigned long nr_pages, void *arg, int (*func)(unsigned long, unsigned long, void *));
struct klist_node;
struct klist {
 spinlock_t k_lock;
 struct list_head k_list;
 void (*get)(struct klist_node *);
 void (*put)(struct klist_node *);
} ;
struct klist_node {
 void *n_klist;
 struct list_head n_node;
 struct kref n_ref;
};
struct klist_iter {
 struct klist *i_klist;
 struct klist_node *i_cur;
};
struct seq_operations;
struct file;
struct path;
struct inode;
struct dentry;
struct user_namespace;
struct seq_file {
 char *buf;
 size_t size;
 size_t from;
 size_t count;
 loff_t index;
 loff_t read_pos;
 u64 version;
 struct mutex lock;
 const struct seq_operations *op;
 int poll_event;
 void *private;
};
struct seq_operations {
 void * (*start) (struct seq_file *m, loff_t *pos);
 void (*stop) (struct seq_file *m, void *v);
 void * (*next) (struct seq_file *m, void *v, loff_t *pos);
 int (*show) (struct seq_file *m, void *v);
};
char *mangle_path(char *s, const char *p, const char *esc);
int seq_open(struct file *, const struct seq_operations *);
ssize_t seq_read(struct file *, char *, size_t, loff_t *);
loff_t seq_lseek(struct file *, loff_t, int);
int seq_release(struct inode *, struct file *);
int seq_escape(struct seq_file *, const char *, const char *);
int seq_putc(struct seq_file *m, char c);
int seq_puts(struct seq_file *m, const char *s);
int seq_write(struct seq_file *seq, const void *data, size_t len);
 int seq_printf(struct seq_file *, const char *, ...);
 int seq_vprintf(struct seq_file *, const char *, va_list args);
int seq_path(struct seq_file *, const struct path *, const char *);
int seq_dentry(struct seq_file *, struct dentry *, const char *);
int seq_path_root(struct seq_file *m, const struct path *path, const struct path *root, const char *esc);
int seq_bitmap(struct seq_file *m, const unsigned long *bits, unsigned int nr_bits);
int seq_bitmap_list(struct seq_file *m, const unsigned long *bits, unsigned int nr_bits);
int single_open(struct file *, int (*)(struct seq_file *, void *), void *);
int single_open_size(struct file *, int (*)(struct seq_file *, void *), void *, size_t);
int single_release(struct inode *, struct file *);
void *__seq_open_private(struct file *, const struct seq_operations *, int);
int seq_open_private(struct file *, const struct seq_operations *, int);
int seq_release_private(struct inode *, struct file *);
int seq_put_decimal_ull(struct seq_file *m, char delimiter, unsigned long long num);
int seq_put_decimal_ll(struct seq_file *m, char delimiter, long long num);
struct pinctrl;
struct pinctrl_state;
struct device;
struct dev_pin_info {
 struct pinctrl *p;
 struct pinctrl_state *default_state;
 struct pinctrl_state *sleep_state;
 struct pinctrl_state *idle_state;
};
struct device;
struct device;
typedef struct pm_message {
 int event;
} pm_message_t;
struct dev_pm_ops {
 int (*prepare)(struct device *dev);
 void (*complete)(struct device *dev);
 int (*suspend)(struct device *dev);
 int (*resume)(struct device *dev);
 int (*freeze)(struct device *dev);
 int (*thaw)(struct device *dev);
 int (*poweroff)(struct device *dev);
 int (*restore)(struct device *dev);
 int (*suspend_late)(struct device *dev);
 int (*resume_early)(struct device *dev);
 int (*freeze_late)(struct device *dev);
 int (*thaw_early)(struct device *dev);
 int (*poweroff_late)(struct device *dev);
 int (*restore_early)(struct device *dev);
 int (*suspend_noirq)(struct device *dev);
 int (*resume_noirq)(struct device *dev);
 int (*freeze_noirq)(struct device *dev);
 int (*thaw_noirq)(struct device *dev);
 int (*poweroff_noirq)(struct device *dev);
 int (*restore_noirq)(struct device *dev);
 int (*runtime_suspend)(struct device *dev);
 int (*runtime_resume)(struct device *dev);
 int (*runtime_idle)(struct device *dev);
};
enum rpm_status {
 RPM_ACTIVE = 0, RPM_RESUMING, RPM_SUSPENDED, RPM_SUSPENDING, };
enum rpm_request {
 RPM_REQ_NONE = 0, RPM_REQ_IDLE, RPM_REQ_SUSPEND, RPM_REQ_AUTOSUSPEND, RPM_REQ_RESUME, };
struct wakeup_source;
struct pm_domain_data {
 struct list_head list_node;
 struct device *dev;
};
struct pm_subsys_data {
 spinlock_t lock;
 unsigned int refcount;
 struct list_head clock_list;
};
struct dev_pm_info {
 pm_message_t power_state;
 unsigned int can_wakeup:1;
 unsigned int async_suspend:1;
 bool is_prepared:1;
 bool is_suspended:1;
 bool ignore_children:1;
 bool early_init:1;
 spinlock_t lock;
 struct list_head entry;
 struct completion completion;
 struct wakeup_source *wakeup;
 bool wakeup_path:1;
 bool syscore:1;
 struct timer_list suspend_timer;
 unsigned long timer_expires;
 struct work_struct work;
 wait_queue_head_t wait_queue;
 atomic_t usage_count;
 atomic_t child_count;
 unsigned int disable_depth:3;
 unsigned int idle_notification:1;
 unsigned int request_pending:1;
 unsigned int deferred_resume:1;
 unsigned int run_wake:1;
 unsigned int runtime_auto:1;
 unsigned int no_callbacks:1;
 unsigned int irq_safe:1;
 unsigned int use_autosuspend:1;
 unsigned int timer_autosuspends:1;
 unsigned int memalloc_noio:1;
 enum rpm_request request;
 enum rpm_status runtime_status;
 int runtime_error;
 int autosuspend_delay;
 unsigned long last_busy;
 unsigned long active_jiffies;
 unsigned long suspended_jiffies;
 unsigned long accounting_timestamp;
 struct pm_subsys_data *subsys_data;
 struct dev_pm_qos *qos;
};
struct dev_pm_domain {
 struct dev_pm_ops ops;
 void (*detach)(struct device *dev, bool power_off);
};
enum dpm_order {
 DPM_ORDER_NONE, DPM_ORDER_DEV_AFTER_PARENT, DPM_ORDER_PARENT_BEFORE_DEV, DPM_ORDER_DEV_LAST, };
struct ratelimit_state {
 raw_spinlock_t lock;
 int interval;
 int burst;
 int printed;
 int missed;
 unsigned long begin;
};
struct dev_archdata {
 struct dma_map_ops *dma_ops;
 void *iommu;
};
struct omap_device;
struct pdev_archdata {
};
struct device;
struct device_private;
struct device_driver;
struct driver_private;
struct module;
struct class;
struct subsys_private;
struct bus_type;
struct device_node;
struct iommu_ops;
struct iommu_group;
struct bus_attribute {
 struct attribute attr;
 ssize_t (*show)(struct bus_type *bus, char *buf);
 ssize_t (*store)(struct bus_type *bus, const char *buf, size_t count);
};
struct bus_type {
 const char *name;
 const char *dev_name;
 struct device *dev_root;
 struct bus_attribute *bus_attrs;
 struct device_attribute *dev_attrs;
 struct driver_attribute *drv_attrs;
 int (*match)(struct device *dev, struct device_driver *drv);
 int (*uevent)(struct device *dev, struct kobj_uevent_env *env);
 int (*probe)(struct device *dev);
 int (*remove)(struct device *dev);
 void (*shutdown)(struct device *dev);
 int (*suspend)(struct device *dev, pm_message_t state);
 int (*resume)(struct device *dev);
 const struct dev_pm_ops *pm;
 struct iommu_ops *iommu_ops;
 struct subsys_private *p;
 struct lock_class_key lock_key;
};
struct subsys_dev_iter {
 struct klist_iter ki;
 const struct device_type *type;
};
void subsys_dev_iter_init(struct subsys_dev_iter *iter, struct bus_type *subsys, struct device *start, const struct device_type *type);
struct device *subsys_dev_iter_next(struct subsys_dev_iter *iter);
void subsys_dev_iter_exit(struct subsys_dev_iter *iter);
int bus_for_each_dev(struct bus_type *bus, struct device *start, void *data, int (*fn)(struct device *dev, void *data));
struct device *bus_find_device(struct bus_type *bus, struct device *start, void *data, int (*match)(struct device *dev, void *data));
struct device *bus_find_device_by_name(struct bus_type *bus, struct device *start, const char *name);
struct device *subsys_find_device_by_id(struct bus_type *bus, unsigned int id, struct device *hint);
int bus_for_each_drv(struct bus_type *bus, struct device_driver *start, void *data, int (*fn)(struct device_driver *, void *));
void bus_sort_breadthfirst(struct bus_type *bus, int (*compare)(const struct device *a, const struct device *b));
struct notifier_block;
struct device_driver {
 const char *name;
 struct bus_type *bus;
 struct module *owner;
 const char *mod_name;
 bool suppress_bind_attrs;
 const struct of_device_id *of_match_table;
 const struct acpi_device_id *acpi_match_table;
 int (*probe) (struct device *dev);
 int (*remove) (struct device *dev);
 void (*shutdown) (struct device *dev);
 int (*suspend) (struct device *dev, pm_message_t state);
 int (*resume) (struct device *dev);
 const struct attribute_group **groups;
 const struct dev_pm_ops *pm;
 struct driver_private *p;
};
struct driver_attribute {
 struct attribute attr;
 ssize_t (*show)(struct device_driver *driver, char *buf);
 ssize_t (*store)(struct device_driver *driver, const char *buf, size_t count);
};
struct device *driver_find_device(struct device_driver *drv, struct device *start, void *data, int (*match)(struct device *dev, void *data));
struct subsys_interface {
 const char *name;
 struct bus_type *subsys;
 struct list_head node;
 int (*add_dev)(struct device *dev, struct subsys_interface *sif);
 int (*remove_dev)(struct device *dev, struct subsys_interface *sif);
};
int subsys_interface_register(struct subsys_interface *sif);
void subsys_interface_unregister(struct subsys_interface *sif);
int subsys_system_register(struct bus_type *subsys, const struct attribute_group **groups);
int subsys_virtual_register(struct bus_type *subsys, const struct attribute_group **groups);
struct class {
 const char *name;
 struct module *owner;
 struct class_attribute *class_attrs;
 struct device_attribute *dev_attrs;
 struct bin_attribute *dev_bin_attrs;
 struct kobject *dev_kobj;
 int (*dev_uevent)(struct device *dev, struct kobj_uevent_env *env);
 char *(*devnode)(struct device *dev, umode_t *mode);
 void (*class_release)(struct class *class);
 void (*dev_release)(struct device *dev);
 int (*suspend)(struct device *dev, pm_message_t state);
 int (*resume)(struct device *dev);
 const struct kobj_ns_type_operations *ns_type;
 const void *(*namespace)(struct device *dev);
 const struct dev_pm_ops *pm;
 struct subsys_private *p;
};
struct class_dev_iter {
 struct klist_iter ki;
 const struct device_type *type;
};
struct class_compat;
struct class_compat *class_compat_register(const char *name);
void class_compat_unregister(struct class_compat *cls);
int class_compat_create_link(struct class_compat *cls, struct device *dev, struct device *device_link);
void class_compat_remove_link(struct class_compat *cls, struct device *dev, struct device *device_link);
struct class_attribute {
 struct attribute attr;
 ssize_t (*show)(struct class *class, struct class_attribute *attr, char *buf);
 ssize_t (*store)(struct class *class, struct class_attribute *attr, const char *buf, size_t count);
 const void *(*namespace)(struct class *class, const struct class_attribute *attr);
};
struct class_attribute_string {
 struct class_attribute attr;
 char *str;
};
struct class_interface {
 struct list_head node;
 struct class *class;
 int (*add_dev) (struct device *, struct class_interface *);
 void (*remove_dev) (struct device *, struct class_interface *);
};
struct device_type {
 const char *name;
 const struct attribute_group **groups;
 int (*uevent)(struct device *dev, struct kobj_uevent_env *env);
 char *(*devnode)(struct device *dev, umode_t *mode, kuid_t *uid, kgid_t *gid);
 void (*release)(struct device *dev);
 const struct dev_pm_ops *pm;
};
struct device_attribute {
 struct attribute attr;
 ssize_t (*show)(struct device *dev, struct device_attribute *attr, char *buf);
 ssize_t (*store)(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
};
struct dev_ext_attribute {
 struct device_attribute attr;
 void *var;
};
ssize_t device_show_ulong(struct device *dev, struct device_attribute *attr, char *buf);
ssize_t device_store_ulong(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
ssize_t device_show_int(struct device *dev, struct device_attribute *attr, char *buf);
ssize_t device_store_int(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
ssize_t device_show_bool(struct device *dev, struct device_attribute *attr, char *buf);
ssize_t device_store_bool(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
typedef void (*dr_release_t)(struct device *dev, void *res);
typedef int (*dr_match_t)(struct device *dev, void *res, void *match_data);
void *devm_ioremap_resource(struct device *dev, struct resource *res);
void *devm_request_and_ioremap(struct device *dev, struct resource *res);
void *devm_ioremap_exec_resource(struct device *dev, struct resource *res);
void *devm_request_and_ioremap_exec(struct device *dev, struct resource *res);
int devm_add_action(struct device *dev, void (*action)(void *), void *data);
void devm_remove_action(struct device *dev, void (*action)(void *), void *data);
struct device_dma_parameters {
 unsigned int max_segment_size;
 unsigned long segment_boundary_mask;
};
struct acpi_dev_node {
};
struct device {
 struct device *parent;
 struct device_private *p;
 struct kobject kobj;
 const char *init_name;
 const struct device_type *type;
 struct mutex mutex;
 struct bus_type *bus;
 struct device_driver *driver;
 void *platform_data;
 struct dev_pm_info power;
 struct dev_pm_domain *pm_domain;
 struct dev_pin_info *pins;
 u64 *dma_mask;
 u64 coherent_dma_mask;
 struct device_dma_parameters *dma_parms;
 struct list_head dma_pools;
 struct dma_coherent_mem *dma_mem;
 struct cma *cma_area;
 struct dev_archdata archdata;
 struct device_node *of_node;
 struct acpi_dev_node acpi_node;
 dev_t devt;
 u32 id;
 spinlock_t devres_lock;
 struct list_head devres_head;
 struct klist_node knode_class;
 struct class *class;
 const struct attribute_group **groups;
 void (*release)(struct device *dev);
 struct iommu_group *iommu_group;
};
struct wakeup_source {
 const char *name;
 struct list_head entry;
 spinlock_t lock;
 struct timer_list timer;
 unsigned long timer_expires;
 ktime_t total_time;
 ktime_t max_time;
 ktime_t last_time;
 ktime_t start_prevent_time;
 ktime_t prevent_sleep_time;
 unsigned long event_count;
 unsigned long active_count;
 unsigned long relax_count;
 unsigned long expire_count;
 unsigned long wakeup_count;
 bool active:1;
 bool autosleep_enabled:1;
};
extern
int dev_set_name(struct device *dev, const char *name, ...);
void driver_init(void);
extern
struct device *device_create(struct class *cls, struct device *parent, dev_t devt, void *drvdata, const char *fmt, ...);
extern
int dev_vprintk_emit(int level, const struct device *dev, const char *fmt, va_list args);
extern
int dev_printk_emit(int level, const struct device *dev, const char *fmt, ...);
extern
int dev_printk(const char *level, const struct device *dev, const char *fmt, ...);
extern
int dev_emerg(const struct device *dev, const char *fmt, ...);
extern
int dev_alert(const struct device *dev, const char *fmt, ...);
extern
int dev_crit(const struct device *dev, const char *fmt, ...);
extern
int dev_err(const struct device *dev, const char *fmt, ...);
extern
int dev_warn(const struct device *dev, const char *fmt, ...);
extern
int dev_notice(const struct device *dev, const char *fmt, ...);
extern
int _dev_info(const struct device *dev, const char *fmt, ...);
struct partition {
 unsigned char boot_ind;
 unsigned char head;
 unsigned char sector;
 unsigned char cyl;
 unsigned char sys_ind;
 unsigned char end_head;
 unsigned char end_sector;
 unsigned char end_cyl;
 __le32 start_sect;
 __le32 nr_sects;
} ;
struct disk_stats {
 unsigned long sectors[2];
 unsigned long ios[2];
 unsigned long merges[2];
 unsigned long ticks[2];
 unsigned long io_ticks;
 unsigned long time_in_queue;
};
struct partition_meta_info {
 char uuid[37];
 u8 volname[64];
};
struct hd_struct {
 sector_t start_sect;
 sector_t nr_sects;
 seqcount_t nr_sects_seq;
 sector_t alignment_offset;
 unsigned int discard_alignment;
 struct device __dev;
 struct kobject *holder_dir;
 int policy, partno;
 struct partition_meta_info *info;
 unsigned long stamp;
 atomic_t in_flight[2];
 struct disk_stats *dkstats;
 atomic_t ref;
 struct callback_head callback_head;
};
enum {
 DISK_EVENT_MEDIA_CHANGE = 1 << 0, DISK_EVENT_EJECT_REQUEST = 1 << 1, };
struct blk_scsi_cmd_filter {
 unsigned long read_ok[((256) / (sizeof(long) * 8))];
 unsigned long write_ok[((256) / (sizeof(long) * 8))];
 struct kobject kobj;
};
struct disk_part_tbl {
 struct callback_head callback_head;
 int len;
 struct hd_struct *last_lookup;
 struct hd_struct *part[];
};
struct disk_events;
struct gendisk {
 int major;
 int first_minor;
 int minors;
 char disk_name[32];
 char *(*devnode)(struct gendisk *gd, umode_t *mode);
 unsigned int events;
 unsigned int async_events;
 struct disk_part_tbl *part_tbl;
 struct hd_struct part0;
 const struct block_device_operations *fops;
 struct request_queue *queue;
 void *private_data;
 int flags;
 struct device *driverfs_dev;
 struct kobject *slave_dir;
 struct timer_rand_state *random;
 atomic_t sync_io;
 struct disk_events *ev;
 int node_id;
 int emmc_disk;
};
struct disk_part_iter {
 struct gendisk *disk;
 struct hd_struct *part;
 int idx;
 unsigned int flags;
};
extern struct hd_struct * add_partition(struct gendisk *disk, int partno, sector_t start, sector_t len, int flags, struct partition_meta_info
             *info);
struct task_struct;
struct task_struct;
struct range {
 u64 start;
 u64 end;
};
int add_range(struct range *range, int az, int nr_range, u64 start, u64 end);
int add_range_with_merge(struct range *range, int az, int nr_range, u64 start, u64 end);
void subtract_range(struct range *range, int az, u64 start, u64 end);
int clean_sort_range(struct range *range, int az);
void sort_range(struct range *range, int nr_range);
struct mempolicy;
struct anon_vma;
struct anon_vma_chain;
struct file_ra_state;
struct user_struct;
struct writeback_control;
struct mm_struct;
extern struct processor {
 void (*_data_abort)(unsigned long pc);
 unsigned long (*_prefetch_abort)(unsigned long lr);
 void (*_proc_init)(void);
 void (*_proc_fin)(void);
 void (*reset)(unsigned long addr) ;
 int (*_do_idle)(void);
 void (*dcache_clean_area)(void *addr, int size);
 void (*switch_mm)(unsigned long pgd_phys, struct mm_struct *mm);
 void (*set_pte_ext)(pte_t *ptep, pte_t pte, unsigned int ext);
 unsigned int suspend_size;
 void (*do_suspend)(void *);
 void (*do_resume)(void *);
} processor;
typedef struct { pgd_t pgd; } pud_t;




struct cpu_tlb_fns {
 void (*flush_user_range)(unsigned long, unsigned long, struct vm_area_struct *);
 void (*flush_kern_range)(unsigned long, unsigned long);
 unsigned long tlb_flags;
};
struct file;
;
;
;
;
;
;

int ptep_clear_flush_young(struct vm_area_struct *vma, unsigned long address, pte_t *ptep);
int pmdp_clear_flush_young(struct vm_area_struct *vma, unsigned long address, pmd_t *pmdp);
struct mm_struct;
void pgd_clear_bad(pgd_t *);
void pud_clear_bad(pud_t *);
void pmd_clear_bad(pmd_t *);
struct vm_fault {
 unsigned int flags;
 unsigned long pgoff;
 void *virtual_address;
 struct page *page;
};
struct vm_operations_struct {
 void (*open)(struct vm_area_struct * area);
 void (*close)(struct vm_area_struct * area);
 int (*fault)(struct vm_area_struct *vma, struct vm_fault *vmf);
 int (*page_mkwrite)(struct vm_area_struct *vma, struct vm_fault *vmf);
 int (*access)(struct vm_area_struct *vma, unsigned long addr, void *buf, int len, int write);
 int (*remap_pages)(struct vm_area_struct *vma, unsigned long addr, unsigned long size, unsigned long pgoff);
};
struct mmu_gather;
struct inode;
enum pageflags {
 PG_locked, PG_error, PG_referenced, PG_uptodate, PG_dirty, PG_lru, PG_active, PG_slab, PG_owner_priv_1, PG_arch_1, PG_reserved, PG_private, PG_private_2, PG_writeback, PG_head, PG_tail, PG_swapcache, PG_mappedtodisk, PG_reclaim, PG_swapbacked, PG_unevictable, PG_mlocked, __NR_PAGEFLAGS, PG_checked = PG_owner_priv_1, PG_fscache = PG_private_2, PG_pinned = PG_owner_priv_1, PG_savepinned = PG_dirty, PG_slob_free = PG_private, };
struct page;
;
u64 stable_page_flags(struct page *page);

int test_clear_page_writeback(struct page *page);
int __test_set_page_writeback(struct page *page, bool keep_write);


enum transparent_hugepage_flag {
 TRANSPARENT_HUGEPAGE_FLAG, TRANSPARENT_HUGEPAGE_REQ_MADV_FLAG, TRANSPARENT_HUGEPAGE_DEFRAG_FLAG, TRANSPARENT_HUGEPAGE_DEFRAG_REQ_MADV_FLAG, TRANSPARENT_HUGEPAGE_DEFRAG_KHUGEPAGED_FLAG, TRANSPARENT_HUGEPAGE_USE_ZERO_PAGE_FLAG, };
enum page_check_address_pmd_flag {
 PAGE_CHECK_ADDRESS_PMD_FLAG, PAGE_CHECK_ADDRESS_PMD_NOTSPLITTING_FLAG, PAGE_CHECK_ADDRESS_PMD_SPLITTING_FLAG, };
struct page *vmalloc_to_page(const void *addr);
unsigned long vmalloc_to_pfn(const void *addr);
void put_page(struct page *page);
void put_pages_list(struct list_head *pages);
void split_page(struct page *page, unsigned int order);
int split_free_page(struct page *page);
typedef void compound_page_dtor(struct page *);
enum vm_event_item { PGPGIN, PGPGOUT, PSWPIN, PSWPOUT, PGALLOC_NORMAL , PGALLOC_HIGH , PGALLOC_MOVABLE, PGFREE, PGACTIVATE, PGDEACTIVATE, PGFAULT, PGMAJFAULT, PGREFILL_NORMAL , PGREFILL_HIGH , PGREFILL_MOVABLE, PGSTEAL_KSWAPD_NORMAL , PGSTEAL_KSWAPD_HIGH , PGSTEAL_KSWAPD_MOVABLE, PGSTEAL_DIRECT_NORMAL , PGSTEAL_DIRECT_HIGH , PGSTEAL_DIRECT_MOVABLE, PGSCAN_KSWAPD_NORMAL , PGSCAN_KSWAPD_HIGH , PGSCAN_KSWAPD_MOVABLE, PGSCAN_DIRECT_NORMAL , PGSCAN_DIRECT_HIGH , PGSCAN_DIRECT_MOVABLE, PGSCAN_DIRECT_THROTTLE, PGINODESTEAL, SLABS_SCANNED, KSWAPD_INODESTEAL, KSWAPD_LOW_WMARK_HIT_QUICKLY, KSWAPD_HIGH_WMARK_HIT_QUICKLY, PAGEOUTRUN, ALLOCSTALL, PGROTATED, PGMIGRATE_SUCCESS, PGMIGRATE_FAIL, COMPACTMIGRATE_SCANNED, COMPACTFREE_SCANNED, COMPACTISOLATED, COMPACTSTALL, COMPACTFAIL, COMPACTSUCCESS, UNEVICTABLE_PGCULLED, UNEVICTABLE_PGSCANNED, UNEVICTABLE_PGRESCUED, UNEVICTABLE_PGMLOCKED, UNEVICTABLE_PGMUNLOCKED, UNEVICTABLE_PGCLEARED, UNEVICTABLE_PGSTRANDED, NR_VM_EVENT_ITEMS
};
void __mod_zone_page_state(struct zone *, enum zone_stat_item item, int);
void __inc_zone_page_state(struct page *, enum zone_stat_item);
void __dec_zone_page_state(struct page *, enum zone_stat_item);
void mod_zone_page_state(struct zone *, enum zone_stat_item, int);
void inc_zone_page_state(struct page *, enum zone_stat_item);
void dec_zone_page_state(struct page *, enum zone_stat_item);
void refresh_cpu_vm_stats(int);
void refresh_zone_stat_thresholds(void);
void drain_zonestat(struct zone *zone, struct per_cpu_pageset *);
int calculate_pressure_threshold(struct zone *zone);
int calculate_normal_threshold(struct zone *zone);
void set_pgdat_percpu_threshold(pg_data_t *pgdat, int (*calculate_pressure)(struct zone *));
void *page_address(const struct page *page);
void set_page_address(struct page *page, void *virtual);
void page_address_init(void);
void shmem_set_file(struct vm_area_struct *vma, struct file *file);
int shmem_zero_setup(struct vm_area_struct *);
struct zap_details {
 struct vm_area_struct *nonlinear_vma;
 struct address_space *check_mapping;
 unsigned long first_index;
 unsigned long last_index;
};
struct page *vm_normal_page(struct vm_area_struct *vma, unsigned long addr, pte_t pte);
int zap_vma_ptes(struct vm_area_struct *vma, unsigned long address, unsigned long size);
void zap_page_range(struct vm_area_struct *vma, unsigned long address, unsigned long size, struct zap_details *);
void unmap_vmas(struct mmu_gather *tlb, struct vm_area_struct *start_vma, unsigned long start, unsigned long end);
struct mm_walk {
 int (*pgd_entry)(pgd_t *pgd, unsigned long addr, unsigned long next, struct mm_walk *walk);
 int (*pud_entry)(pud_t *pud, unsigned long addr, unsigned long next, struct mm_walk *walk);
 int (*pmd_entry)(pmd_t *pmd, unsigned long addr, unsigned long next, struct mm_walk *walk);
 int (*pte_entry)(pte_t *pte, unsigned long addr, unsigned long next, struct mm_walk *walk);
 int (*pte_hole)(unsigned long addr, unsigned long next, struct mm_walk *walk);
 int (*hugetlb_entry)(pte_t *pte, unsigned long hmask, unsigned long addr, unsigned long next, struct mm_walk *walk);
 struct mm_struct *mm;
 void *private;
};
int walk_page_range(unsigned long addr, unsigned long end, struct mm_walk *walk);
void free_pgd_range(struct mmu_gather *tlb, unsigned long addr, unsigned long end, unsigned long floor, unsigned long ceiling);
int copy_page_range(struct mm_struct *dst, struct mm_struct *src, struct vm_area_struct *vma);
void unmap_mapping_range(struct address_space *mapping, loff_t const holebegin, loff_t const holelen, int even_cows);
int follow_pfn(struct vm_area_struct *vma, unsigned long address, unsigned long *pfn);
int follow_phys(struct vm_area_struct *vma, unsigned long address, unsigned int flags, unsigned long *prot, resource_size_t *phys);
int generic_access_phys(struct vm_area_struct *vma, unsigned long addr, void *buf, int len, int write);
void pagecache_isize_extended(struct inode *inode, loff_t from, loff_t to);
void truncate_pagecache_range(struct inode *inode, loff_t offset, loff_t end);
int truncate_inode_page(struct address_space *mapping, struct page *page);
int generic_error_remove_page(struct address_space *mapping, struct page *page);
int invalidate_inode_page(struct page *page);
long __get_user_pages(struct task_struct *tsk, struct mm_struct *mm, unsigned long start, unsigned long nr_pages, unsigned int foll_flags, struct page **pages, struct vm_area_struct **vmas, int *nonblocking);
long get_user_pages(struct task_struct *tsk, struct mm_struct *mm, unsigned long start, unsigned long nr_pages, int write, int force, struct page **pages, struct vm_area_struct **vmas);
int get_user_pages_fast(unsigned long start, int nr_pages, int write, struct page **pages);
struct kvec;
int get_kernel_pages(const struct kvec *iov, int nr_pages, int write, struct page **pages);
int get_kernel_page(unsigned long start, int write, struct page **pages);
struct page *get_dump_page(unsigned long addr);
int __set_page_dirty_nobuffers(struct page *page);
int __set_page_dirty_no_writeback(struct page *page);
int redirty_page_for_writepage(struct writeback_control *wbc, struct page *page);
void account_page_dirtied(struct page *page, struct address_space *mapping);
void account_page_writeback(struct page *page);
int set_page_dirty(struct page *page);
int set_page_dirty_lock(struct page *page);
int clear_page_dirty_for_io(struct page *page);
extern pid_t
vm_is_stack(struct task_struct *task, struct vm_area_struct *vma, int in_group);
int __get_user_pages_fast(unsigned long start, int nr_pages, int write, struct page **pages);
void sync_mm_rss(struct mm_struct *mm);
int vma_wants_writenotify(struct vm_area_struct *vma);
int __pmd_alloc(struct mm_struct *mm, pud_t *pud, unsigned long address);
int __pte_alloc(struct mm_struct *mm, struct vm_area_struct *vma, pmd_t *pmd, unsigned long address);
int __pte_alloc_kernel(pmd_t *pmd, unsigned long address);
extern
void warn_alloc_failed(gfp_t gfp_mask, int order, const char *fmt, ...);
void vma_interval_tree_insert(struct vm_area_struct *node, struct rb_root *root);
void vma_interval_tree_insert_after(struct vm_area_struct *node, struct vm_area_struct *prev, struct rb_root *root);
void vma_interval_tree_remove(struct vm_area_struct *node, struct rb_root *root);
struct vm_area_struct *vma_interval_tree_iter_first(struct rb_root *root, unsigned long start, unsigned long last);
struct vm_area_struct *vma_interval_tree_iter_next(struct vm_area_struct *node, unsigned long start, unsigned long last);
void anon_vma_interval_tree_insert(struct anon_vma_chain *node, struct rb_root *root);
void anon_vma_interval_tree_remove(struct anon_vma_chain *node, struct rb_root *root);
struct anon_vma_chain *anon_vma_interval_tree_iter_first( struct rb_root *root, unsigned long start, unsigned long last);
struct anon_vma_chain *anon_vma_interval_tree_iter_next( struct anon_vma_chain *node, unsigned long start, unsigned long last);
struct vm_unmapped_area_info {
 unsigned long flags;
 unsigned long length;
 unsigned long low_limit;
 unsigned long high_limit;
 unsigned long align_mask;
 unsigned long align_offset;
};
int write_one_page(struct page *page, int wait);
void task_dirty_inc(struct task_struct *tsk);
int force_page_cache_readahead(struct address_space *mapping, struct file *filp, unsigned long offset, unsigned long nr_to_read);
void page_cache_sync_readahead(struct address_space *mapping, struct file_ra_state *ra, struct file *filp, unsigned long offset, unsigned long size);
void page_cache_async_readahead(struct address_space *mapping, struct file_ra_state *ra, struct file *filp, struct page *pg, unsigned long offset, unsigned long size);
unsigned long max_sane_readahead(unsigned long nr);
unsigned long ra_submit(struct file_ra_state *ra, struct address_space *mapping, struct file *filp);
pgprot_t vm_get_page_prot(unsigned long vm_flags);
struct vm_area_struct *find_extend_vma(struct mm_struct *, unsigned long addr);
int remap_pfn_range(struct vm_area_struct *, unsigned long addr, unsigned long pfn, unsigned long size, pgprot_t);
int vm_insert_page(struct vm_area_struct *, unsigned long addr, struct page *);
int vm_insert_pfn(struct vm_area_struct *vma, unsigned long addr, unsigned long pfn);
int vm_insert_mixed(struct vm_area_struct *vma, unsigned long addr, unsigned long pfn);
int vm_iomap_memory(struct vm_area_struct *vma, phys_addr_t start, unsigned long len);
struct page *follow_page_mask(struct vm_area_struct *vma, unsigned long address, unsigned int foll_flags, unsigned int *page_mask);
typedef int (*pte_fn_t)(pte_t *pte, pgtable_t token, unsigned long addr, void *data);
void vm_stat_account(struct mm_struct *, unsigned long, struct file *, long);


           ;
struct exception_table_entry
{
 unsigned long insn, fixup;
};


struct task_struct;











struct irqaction;
struct pt_regs;
void handle_IRQ(unsigned int, struct pt_regs *);
void init_IRQ(void);
void arch_trigger_all_cpu_backtrace(void);
typedef struct {
 unsigned int __softirq_pending;
 unsigned int ipi_irqs[8];
} irq_cpustat_t;
u64 smp_irq_stat_cpu(unsigned int cpu);


struct cpu_cache_fns {
 void (*flush_icache_all)(void);
 void (*flush_kern_all)(void);
 void (*flush_kern_louis)(void);
 void (*flush_user_all)(void);
 void (*flush_user_range)(unsigned long, unsigned long, unsigned int);
 void (*coherent_kern_range)(unsigned long, unsigned long);
 int (*coherent_user_range)(unsigned long, unsigned long);
 void (*flush_kern_dcache_area)(void *, size_t);
 void (*dma_map_area)(const void *, size_t, int);
 void (*dma_unmap_area)(const void *, size_t, int);
 void (*dma_flush_range)(const void *, const void *);
};
unsigned int nr_free_highpages(void);
void kmap_flush_unused(void);
struct page *kmap_to_page(void *addr);
enum mapping_flags {
 AS_EIO = 25 + 0, AS_ENOSPC = 25 + 1, AS_MM_ALL_LOCKS = 25 + 2, AS_UNEVICTABLE = 25 + 3, AS_BALLOON_MAP = 25 + 4, };
void release_pages(struct page **pages, int nr, int cold);
typedef int filler_t(void *, struct page *);
unsigned long page_cache_next_hole(struct address_space *mapping, unsigned long index, unsigned long max_scan);
unsigned find_get_pages(struct address_space *mapping, unsigned long start, unsigned int nr_pages, struct page **pages);
unsigned find_get_pages_contig(struct address_space *mapping, unsigned long start, unsigned int nr_pages, struct page **pages);
unsigned find_get_pages_tag(struct address_space *mapping, unsigned long *index, int tag, unsigned int nr_pages, struct page **pages);
struct page *grab_cache_page_write_begin(struct address_space *mapping, unsigned long index, unsigned flags);
void wait_for_stable_page(struct page *page);
int add_to_page_cache_locked(struct page *page, struct address_space *mapping, unsigned long index, gfp_t gfp_mask);
int add_to_page_cache_lru(struct page *page, struct address_space *mapping, unsigned long index, gfp_t gfp_mask);
int replace_page_cache_page(struct page *old, struct page *new, gfp_t gfp_mask);
struct fprop_global {
 struct percpu_counter events;
 unsigned int period;
 seqcount_t sequence;
};
int fprop_global_init(struct fprop_global *p);
void fprop_global_destroy(struct fprop_global *p);
bool fprop_new_period(struct fprop_global *p, int periods);
struct fprop_local_single {
 unsigned long events;
 unsigned int period;
 raw_spinlock_t lock;
};
int fprop_local_init_single(struct fprop_local_single *pl);
void fprop_local_destroy_single(struct fprop_local_single *pl);
void __fprop_inc_single(struct fprop_global *p, struct fprop_local_single *pl);
void fprop_fraction_single(struct fprop_global *p, struct fprop_local_single *pl, unsigned long *numerator, unsigned long *denominator);
struct fprop_local_percpu {
 struct percpu_counter events;
 unsigned int period;
 raw_spinlock_t lock;
};
int fprop_local_init_percpu(struct fprop_local_percpu *pl);
void fprop_local_destroy_percpu(struct fprop_local_percpu *pl);
void __fprop_inc_percpu(struct fprop_global *p, struct fprop_local_percpu *pl);
void __fprop_inc_percpu_max(struct fprop_global *p, struct fprop_local_percpu *pl, int max_frac);
void fprop_fraction_percpu(struct fprop_global *p, struct fprop_local_percpu *pl, unsigned long *numerator, unsigned long *denominator);
struct backing_dev_info;
enum writeback_sync_modes {
 WB_SYNC_NONE, WB_SYNC_ALL, };
enum wb_reason {
 WB_REASON_BACKGROUND, WB_REASON_TRY_TO_FREE_PAGES, WB_REASON_SYNC, WB_REASON_PERIODIC, WB_REASON_LAPTOP_TIMER, WB_REASON_FREE_MORE_MEM, WB_REASON_FS_FREE_SPACE, WB_REASON_FORKER_THREAD, WB_REASON_MAX, };
struct writeback_control {
 long nr_to_write;
 long pages_skipped;
 loff_t range_start;
 loff_t range_end;
 enum writeback_sync_modes sync_mode;
 unsigned for_kupdate:1;
 unsigned for_background:1;
 unsigned tagged_writepages:1;
 unsigned for_reclaim:1;
 unsigned range_cyclic:1;
 unsigned for_sync:1;
};
struct bdi_writeback;
int inode_wait(void *);
void writeback_inodes_sb(struct super_block *, enum wb_reason reason);
void writeback_inodes_sb_nr(struct super_block *, unsigned long nr, enum wb_reason reason);
int try_to_writeback_inodes_sb(struct super_block *, enum wb_reason reason);
int try_to_writeback_inodes_sb_nr(struct super_block *, unsigned long nr, enum wb_reason reason);
void sync_inodes_sb(struct super_block *);
long writeback_inodes_wb(struct bdi_writeback *wb, long nr_pages, enum wb_reason reason);
long wb_do_writeback(struct bdi_writeback *wb, int force_wait);
void wakeup_flusher_threads(long nr_pages, enum wb_reason reason);
void inode_wait_for_writeback(struct inode *inode);
void laptop_io_completion(struct backing_dev_info *info);
void laptop_sync_completion(void);
void laptop_mode_sync(struct work_struct *work);
void laptop_mode_timer_fn(unsigned long data);
void throttle_vm_writeout(gfp_t gfp_mask);
bool zone_dirty_ok(struct zone *zone);
struct ctl_table;
int dirty_writeback_centisecs_handler(struct ctl_table *, int, void *, size_t *, loff_t *);
void global_dirty_limits(unsigned long *pbackground, unsigned long *pdirty);
unsigned long bdi_dirty_limit(struct backing_dev_info *bdi, unsigned long dirty);
void __bdi_update_bandwidth(struct backing_dev_info *bdi, unsigned long thresh, unsigned long bg_thresh, unsigned long dirty, unsigned long bdi_thresh, unsigned long bdi_dirty, unsigned long start_time);
void page_writeback_init(void);
void balance_dirty_pages_ratelimited(struct address_space *mapping);
typedef int (*writepage_t)(struct page *page, struct writeback_control *wbc, void *data);
int generic_writepages(struct address_space *mapping, struct writeback_control *wbc);
void tag_pages_for_writeback(struct address_space *mapping, unsigned long start, unsigned long end);
int write_cache_pages(struct address_space *mapping, struct writeback_control *wbc, writepage_t writepage, void *data);
int do_writepages(struct address_space *mapping, struct writeback_control *wbc);
void set_page_dirty_balance(struct page *page, int page_mkwrite);
void writeback_set_ratelimit(void);
void tag_pages_for_writeback(struct address_space *mapping, unsigned long start, unsigned long end);
void account_page_redirty(struct page *page);
struct page;
struct device;
struct dentry;
enum bdi_state {
 BDI_wb_alloc, BDI_async_congested, BDI_sync_congested, BDI_registered, BDI_writeback_running, BDI_unused, };
typedef int (congested_fn)(void *, int);
enum bdi_stat_item {
 BDI_RECLAIMABLE, BDI_WRITEBACK, BDI_DIRTIED, BDI_WRITTEN, NR_BDI_STAT_ITEMS
};
struct bdi_writeback {
 struct backing_dev_info *bdi;
 unsigned int nr;
 unsigned long last_old_flush;
 struct delayed_work dwork;
 struct list_head b_dirty;
 struct list_head b_io;
 struct list_head b_more_io;
 spinlock_t list_lock;
};
struct backing_dev_info {
 struct list_head bdi_list;
 unsigned long ra_pages;
 unsigned long state;
 unsigned int capabilities;
 congested_fn *congested_fn;
 void *congested_data;
 char *name;
 struct percpu_counter bdi_stat[NR_BDI_STAT_ITEMS];
 unsigned long bw_time_stamp;
 unsigned long dirtied_stamp;
 unsigned long written_stamp;
 unsigned long write_bandwidth;
 unsigned long avg_write_bandwidth;
 unsigned long dirty_ratelimit;
 unsigned long balanced_dirty_ratelimit;
 struct fprop_local_percpu completions;
 int dirty_exceeded;
 unsigned int min_ratio;
 unsigned int max_ratio, max_prop_frac;
 struct bdi_writeback wb;
 spinlock_t wb_lock;
 struct list_head work_list;
 struct device *dev;
 struct timer_list laptop_mode_wb_timer;
 struct dentry *debug_dir;
 struct dentry *debug_stats;
};
int bdi_init(struct backing_dev_info *bdi);
void bdi_destroy(struct backing_dev_info *bdi);

int bdi_register(struct backing_dev_info *bdi, struct device *parent, const char *fmt, ...);
int bdi_register_dev(struct backing_dev_info *bdi, dev_t dev);
void bdi_unregister(struct backing_dev_info *bdi);
int bdi_setup_and_register(struct backing_dev_info *, char *, unsigned int);
void bdi_start_writeback(struct backing_dev_info *bdi, long nr_pages, enum wb_reason reason);
void bdi_start_background_writeback(struct backing_dev_info *bdi);
void bdi_writeback_workfn(struct work_struct *work);
int bdi_has_dirty_io(struct backing_dev_info *bdi);
void bdi_wakeup_thread_delayed(struct backing_dev_info *bdi);
void bdi_lock_two(struct bdi_writeback *wb1, struct bdi_writeback *wb2);
int bdi_set_min_ratio(struct backing_dev_info *bdi, unsigned int min_ratio);
int bdi_set_max_ratio(struct backing_dev_info *bdi, unsigned int max_ratio);
int writeback_in_progress(struct backing_dev_info *bdi);
enum {
 BLK_RW_ASYNC = 0, BLK_RW_SYNC = 1, };
void clear_bdi_congested(struct backing_dev_info *bdi, int sync);
void set_bdi_congested(struct backing_dev_info *bdi, int sync);
long congestion_wait(int sync, long timeout);
long wait_iff_congested(struct zone *zone, int sync, long timeout);
int pdflush_proc_obsolete(struct ctl_table *table, int write, void *buffer, size_t *lenp, loff_t *ppos);
struct kmem_cache;
typedef void * (mempool_alloc_t)(gfp_t gfp_mask, void *pool_data);
typedef void (mempool_free_t)(void *element, void *pool_data);
typedef struct mempool_s {
 spinlock_t lock;
 int min_nr;
 int curr_nr;
 void **elements;
 void *pool_data;
 mempool_alloc_t *alloc;
 mempool_free_t *free;
 wait_queue_head_t wait;
} mempool_t;
void *mempool_alloc_slab(gfp_t gfp_mask, void *pool_data);
void mempool_free_slab(void *element, void *pool_data);
void *mempool_kmalloc(gfp_t gfp_mask, void *pool_data);
void mempool_kfree(void *element, void *pool_data);
void *mempool_alloc_pages(gfp_t gfp_mask, void *pool_data);
void mempool_free_pages(void *element, void *pool_data);
enum {
 ICQ_EXITED = 1 << 2, };
struct io_cq {
 struct request_queue *q;
 struct io_context *ioc;
 union {
  struct list_head q_node;
  struct kmem_cache *__rcu_icq_cache;
 };
 union {
  struct hlist_node ioc_node;
  struct callback_head __rcu_head;
 };
 unsigned int flags;
};
struct io_context {
 atomic_long_t refcount;
 atomic_t active_ref;
 atomic_t nr_tasks;
 spinlock_t lock;
 unsigned short ioprio;
 int nr_batch_requests;
 unsigned long last_waited;
 struct radix_tree_root icq_tree;
 struct io_cq *icq_hint;
 struct hlist_head icq_list;
 struct work_struct release_work;
};
struct task_struct;
void put_io_context(struct io_context *ioc);
void put_io_context_active(struct io_context *ioc);
void exit_io_context(struct task_struct *task);
struct io_context *get_task_io_context(struct task_struct *task, gfp_t gfp_flags, int node);
enum {
 IOPRIO_CLASS_NONE, IOPRIO_CLASS_RT, IOPRIO_CLASS_BE, IOPRIO_CLASS_IDLE, };
enum {
 IOPRIO_WHO_PROCESS = 1, IOPRIO_WHO_PGRP, IOPRIO_WHO_USER, };
struct pci_dev;
struct pci_dev;
struct bio_pair {
 struct bio bio1, bio2;
 struct bio_vec bv1, bv2;
 atomic_t cnt;
 int error;
};
struct request_queue;
struct sg_iovec;
struct rq_map_data;
void zero_fill_bio(struct bio *bio);


struct bio_list {
 struct bio *head;
 struct bio *tail;
};
struct bio_set {
 struct kmem_cache *bio_slab;
 unsigned int front_pad;
 mempool_t *bio_pool;
 mempool_t *bvec_pool;
 spinlock_t rescue_lock;
 struct bio_list rescue_list;
 struct work_struct rescue_work;
 struct workqueue_struct *rescue_workqueue;
};
struct biovec_slab {
 int nr_vecs;
 char *name;
 struct kmem_cache *slab;
};
struct sg_io_v4 {
 __s32 guard;
 __u32 protocol;
 __u32 subprotocol;
 __u32 request_len;
 __u64 request;
 __u64 request_tag;
 __u32 request_attr;
 __u32 request_priority;
 __u32 request_extra;
 __u32 max_response_len;
 __u64 response;
 __u32 dout_iovec_count;
 __u32 dout_xfer_len;
 __u32 din_iovec_count;
 __u32 din_xfer_len;
 __u64 dout_xferp;
 __u64 din_xferp;
 __u32 timeout;
 __u32 flags;
 __u64 usr_ptr;
 __u32 spare_in;
 __u32 driver_status;
 __u32 transport_status;
 __u32 device_status;
 __u32 retry_delay;
 __u32 info;
 __u32 duration;
 __u32 response_len;
 __s32 din_resid;
 __s32 dout_resid;
 __u64 generated_tag;
 __u32 spare_out;
 __u32 padding;
};
struct bsg_class_device {
 struct device *class_dev;
 struct device *parent;
 int minor;
 struct request_queue *queue;
 struct kref ref;
 void (*release)(struct device *);
};
struct scatterlist {
 unsigned long page_link;
 unsigned int offset;
 unsigned int length;
 dma_addr_t dma_address;
};
struct module;
struct scsi_ioctl_command;
struct request_queue;
struct elevator_queue;
struct request_pm_state;
struct blk_trace;
struct request;
struct sg_io_hdr;
struct bsg_job;
struct blkcg_gq;
struct request;
typedef void (rq_end_io_fn)(struct request *, int);
struct request_list {
 struct request_queue *q;
 int count[2];
 int starved[2];
 mempool_t *rq_pool;
 wait_queue_head_t wait[2];
 unsigned int flags;
};
enum rq_cmd_type_bits {
 REQ_TYPE_FS = 1, REQ_TYPE_BLOCK_PC, REQ_TYPE_SENSE, REQ_TYPE_PM_SUSPEND, REQ_TYPE_PM_RESUME, REQ_TYPE_PM_SHUTDOWN, REQ_TYPE_SPECIAL, REQ_TYPE_ATA_TASKFILE, REQ_TYPE_ATA_PC, };
struct request {
 struct list_head queuelist;
 struct call_single_data csd;
 struct request_queue *q;
 unsigned int cmd_flags;
 enum rq_cmd_type_bits cmd_type;
 unsigned long atomic_flags;
 int cpu;
 unsigned int __data_len;
 sector_t __sector;
 struct bio *bio;
 struct bio *biotail;
 struct hlist_node hash;
 union {
  struct rb_node rb_node;
  void *completion_data;
 };
 union {
  struct {
   struct io_cq *icq;
   void *priv[2];
  } elv;
  struct {
   unsigned int seq;
   struct list_head list;
   rq_end_io_fn *saved_end_io;
  } flush;
 };
 struct gendisk *rq_disk;
 struct hd_struct *part;
 unsigned long start_time;
 unsigned short nr_phys_segments;
 unsigned short ioprio;
 int ref_count;
 void *special;
 char *buffer;
 int tag;
 int errors;
 unsigned char __cmd[16];
 unsigned char *cmd;
 unsigned short cmd_len;
 unsigned int extra_len;
 unsigned int sense_len;
 unsigned int resid_len;
 void *sense;
 unsigned long deadline;
 struct list_head timeout_list;
 unsigned int timeout;
 int retries;
 rq_end_io_fn *end_io;
 void *end_io_data;
 struct request *next_rq;
};
struct request_pm_state
{
 int pm_step;
 u32 pm_state;
 void* data;
};
struct io_cq;
struct elevator_type;
typedef int (elevator_merge_fn) (struct request_queue *, struct request **, struct bio *);
typedef void (elevator_merge_req_fn) (struct request_queue *, struct request *, struct request *);
typedef void (elevator_merged_fn) (struct request_queue *, struct request *, int);
typedef int (elevator_allow_merge_fn) (struct request_queue *, struct request *, struct bio *);
typedef void (elevator_bio_merged_fn) (struct request_queue *, struct request *, struct bio *);
typedef int (elevator_dispatch_fn) (struct request_queue *, int);
typedef void (elevator_add_req_fn) (struct request_queue *, struct request *);
typedef struct request *(elevator_request_list_fn) (struct request_queue *, struct request *);
typedef void (elevator_completed_req_fn) (struct request_queue *, struct request *);
typedef int (elevator_may_queue_fn) (struct request_queue *, int);
typedef void (elevator_init_icq_fn) (struct io_cq *);
typedef void (elevator_exit_icq_fn) (struct io_cq *);
typedef int (elevator_set_req_fn) (struct request_queue *, struct request *, struct bio *, gfp_t);
typedef void (elevator_put_req_fn) (struct request *);
typedef void (elevator_activate_req_fn) (struct request_queue *, struct request *);
typedef void (elevator_deactivate_req_fn) (struct request_queue *, struct request *);
typedef int (elevator_init_fn) (struct request_queue *, struct elevator_type *e);
typedef void (elevator_exit_fn) (struct elevator_queue *);
struct elevator_ops
{
 elevator_merge_fn *elevator_merge_fn;
 elevator_merged_fn *elevator_merged_fn;
 elevator_merge_req_fn *elevator_merge_req_fn;
 elevator_allow_merge_fn *elevator_allow_merge_fn;
 elevator_bio_merged_fn *elevator_bio_merged_fn;
 elevator_dispatch_fn *elevator_dispatch_fn;
 elevator_add_req_fn *elevator_add_req_fn;
 elevator_activate_req_fn *elevator_activate_req_fn;
 elevator_deactivate_req_fn *elevator_deactivate_req_fn;
 elevator_completed_req_fn *elevator_completed_req_fn;
 elevator_request_list_fn *elevator_former_req_fn;
 elevator_request_list_fn *elevator_latter_req_fn;
 elevator_init_icq_fn *elevator_init_icq_fn;
 elevator_exit_icq_fn *elevator_exit_icq_fn;
 elevator_set_req_fn *elevator_set_req_fn;
 elevator_put_req_fn *elevator_put_req_fn;
 elevator_may_queue_fn *elevator_may_queue_fn;
 elevator_init_fn *elevator_init_fn;
 elevator_exit_fn *elevator_exit_fn;
};
struct elv_fs_entry {
 struct attribute attr;
 ssize_t (*show)(struct elevator_queue *, char *);
 ssize_t (*store)(struct elevator_queue *, const char *, size_t);
};
struct elevator_type
{
 struct kmem_cache *icq_cache;
 struct elevator_ops ops;
 size_t icq_size;
 size_t icq_align;
 struct elv_fs_entry *elevator_attrs;
 char elevator_name[(16)];
 struct module *elevator_owner;
 char icq_cache_name[(16) + 5];
 struct list_head list;
};
struct elevator_queue
{
 struct elevator_type *type;
 void *elevator_data;
 struct kobject kobj;
 struct mutex sysfs_lock;
 unsigned int registered:1;
 struct hlist_head hash[1 << (6)];
};
enum {
 ELV_MQUEUE_MAY, ELV_MQUEUE_NO, ELV_MQUEUE_MUST, };
typedef void (request_fn_proc) (struct request_queue *q);
typedef void (make_request_fn) (struct request_queue *q, struct bio *bio);
typedef int (prep_rq_fn) (struct request_queue *, struct request *);
typedef void (unprep_rq_fn) (struct request_queue *, struct request *);
struct bio_vec;
struct bvec_merge_data {
 struct block_device *bi_bdev;
 sector_t bi_sector;
 unsigned bi_size;
 unsigned long bi_rw;
};
typedef int (merge_bvec_fn) (struct request_queue *, struct bvec_merge_data *, struct bio_vec *);
typedef void (softirq_done_fn)(struct request *);
typedef int (dma_drain_needed_fn)(struct request *);
typedef int (lld_busy_fn) (struct request_queue *q);
typedef int (bsg_job_fn) (struct bsg_job *);
enum blk_eh_timer_return {
 BLK_EH_NOT_HANDLED, BLK_EH_HANDLED, BLK_EH_RESET_TIMER, };
typedef enum blk_eh_timer_return (rq_timed_out_fn)(struct request *);
enum blk_queue_state {
 Queue_down, Queue_up, };
struct blk_queue_tag {
 struct request **tag_index;
 unsigned long *tag_map;
 int busy;
 int max_depth;
 int real_max_depth;
 atomic_t refcnt;
};
struct queue_limits {
 unsigned long bounce_pfn;
 unsigned long seg_boundary_mask;
 unsigned int max_hw_sectors;
 unsigned int max_sectors;
 unsigned int max_segment_size;
 unsigned int physical_block_size;
 unsigned int alignment_offset;
 unsigned int io_min;
 unsigned int io_opt;
 unsigned int max_discard_sectors;
 unsigned int max_write_same_sectors;
 unsigned int discard_granularity;
 unsigned int discard_alignment;
 unsigned short logical_block_size;
 unsigned short max_segments;
 unsigned short max_integrity_segments;
 unsigned char misaligned;
 unsigned char discard_misaligned;
 unsigned char cluster;
 unsigned char discard_zeroes_data;
};

struct request_queue {
 struct list_head queue_head;
 struct request *last_merge;
 struct elevator_queue *elevator;
 int nr_rqs[2];
 int nr_rqs_elvpriv;
 struct request_list root_rl;
 request_fn_proc *request_fn;
 make_request_fn *make_request_fn;
 prep_rq_fn *prep_rq_fn;
 unprep_rq_fn *unprep_rq_fn;
 merge_bvec_fn *merge_bvec_fn;
 softirq_done_fn *softirq_done_fn;
 rq_timed_out_fn *rq_timed_out_fn;
 dma_drain_needed_fn *dma_drain_needed;
 lld_busy_fn *lld_busy_fn;
 sector_t end_sector;
 struct request *boundary_rq;
 struct delayed_work delay_work;
 struct backing_dev_info backing_dev_info;
 void *queuedata;
 unsigned long queue_flags;
 int id;
 gfp_t bounce_gfp;
 spinlock_t __queue_lock;
 spinlock_t *queue_lock;
 struct kobject kobj;
 struct device *dev;
 int rpm_status;
 unsigned int nr_pending;
 unsigned long nr_requests;
 unsigned int nr_congestion_on;
 unsigned int nr_congestion_off;
 unsigned int nr_batching;
 unsigned int dma_drain_size;
 void *dma_drain_buffer;
 unsigned int dma_pad_mask;
 unsigned int dma_alignment;
 struct blk_queue_tag *queue_tags;
 struct list_head tag_busy_list;
 unsigned int nr_sorted;
 unsigned int in_flight[2];
 unsigned int request_fn_active;
 unsigned int rq_timeout;
 struct timer_list timeout;
 struct list_head timeout_list;
 struct list_head icq_list;
 struct queue_limits limits;
 unsigned int sg_timeout;
 unsigned int sg_reserved_size;
 int node;
 unsigned int flush_flags;
 unsigned int flush_not_queueable:1;
 unsigned int flush_queue_delayed:1;
 unsigned int flush_pending_idx:1;
 unsigned int flush_running_idx:1;
 unsigned long flush_pending_since;
 struct list_head flush_queue[2];
 struct list_head flush_data_in_flight;
 struct request flush_rq;
 struct mutex sysfs_lock;
 int bypass_depth;
 bsg_job_fn *bsg_job_fn;
 int bsg_job_size;
 struct bsg_class_device bsg_dev;
 struct callback_head callback_head;
};


struct rq_map_data {
 struct page **pages;
 int page_order;
 int nr_entries;
 unsigned long offset;
 int null_mapped;
 int from_user;
};
struct req_iterator {
 int i;
 struct bio *bio;
};
bool blk_get_queue(struct request_queue *);
struct request_queue *blk_alloc_queue(gfp_t);
struct request_queue *blk_alloc_queue_node(gfp_t, int);
struct blk_plug {
 unsigned long magic;
 struct list_head list;
 struct list_head cb_list;
};
struct blk_plug_cb;
typedef void (*blk_plug_cb_fn)(struct blk_plug_cb *, bool);
struct blk_plug_cb {
 struct list_head list;
 blk_plug_cb_fn callback;
 void *data;
};
enum blk_default_limits {
 BLK_MAX_SEGMENTS = 128, BLK_SAFE_MAX_SECTORS = 255, BLK_DEF_MAX_SECTORS = 1024, BLK_MAX_SEGMENT_SIZE = 65536, BLK_SEG_BOUNDARY_MASK = 0xFFFFFFFFUL, };
typedef struct {struct page *v;} Sector;
unsigned char *read_dev_sector(struct block_device *, sector_t, Sector *);
struct work_struct;
int kblockd_schedule_work(struct request_queue *q, struct work_struct *work);


struct bio;
struct block_device;
struct gendisk;
struct blk_integrity;
struct block_device_operations {
 int (*open) (struct block_device *, fmode_t);
 void (*release) (struct gendisk *, fmode_t);
 int (*ioctl) (struct block_device *, fmode_t, unsigned, unsigned long);
 int (*compat_ioctl) (struct block_device *, fmode_t, unsigned, unsigned long);
 int (*direct_access) (struct block_device *, sector_t, void **, unsigned long *);
 unsigned int (*check_events) (struct gendisk *disk, unsigned int clearing);
 int (*media_changed) (struct gendisk *);
 void (*unlock_native_capacity) (struct gendisk *);
 int (*revalidate_disk) (struct gendisk *);
 int (*getgeo)(struct block_device *, struct hd_geometry *);
 void (*swap_slot_free_notify) (struct block_device *, unsigned long);
 struct module *owner;
};
struct blkpg_ioctl_arg {
        int op;
        int flags;
        int datalen;
        void *data;
};
struct blkpg_partition {
 long long start;
 long long length;
 int pno;
 char devname[64];
 char volname[64];
};
typedef union ide_reg_valid_s {
 unsigned all : 16;
 struct {
  unsigned data : 1;
  unsigned error_feature : 1;
  unsigned sector : 1;
  unsigned nsector : 1;
  unsigned lcyl : 1;
  unsigned hcyl : 1;
  unsigned select : 1;
  unsigned status_command : 1;
  unsigned data_hob : 1;
  unsigned error_feature_hob : 1;
  unsigned sector_hob : 1;
  unsigned nsector_hob : 1;
  unsigned lcyl_hob : 1;
  unsigned hcyl_hob : 1;
  unsigned select_hob : 1;
  unsigned control_hob : 1;
 } b;
} ide_reg_valid_t;
typedef struct ide_task_request_s {
 __u8 io_ports[8];
 __u8 hob_ports[8];
 ide_reg_valid_t out_flags;
 ide_reg_valid_t in_flags;
 int data_phase;
 int req_cmd;
 unsigned long out_size;
 unsigned long in_size;
} ide_task_request_t;
typedef struct ide_ioctl_request_s {
 ide_task_request_t *task_request;
 unsigned char *out_buffer;
 unsigned char *in_buffer;
} ide_ioctl_request_t;
struct hd_drive_cmd_hdr {
 __u8 command;
 __u8 sector_number;
 __u8 feature;
 __u8 sector_count;
};
struct hd_geometry {
      unsigned char heads;
      unsigned char sectors;
      unsigned short cylinders;
      unsigned long start;
};
enum {
 BUSSTATE_OFF = 0, BUSSTATE_ON, BUSSTATE_TRISTATE
};
typedef struct {
 __u8 b[16];
} uuid_le;
typedef struct {
 __u8 b[16];
} uuid_be;
typedef unsigned long kernel_ulong_t;
struct pci_device_id {
 __u32 vendor, device;
 __u32 subvendor, subdevice;
 __u32 class, class_mask;
 kernel_ulong_t driver_data;
};
struct ieee1394_device_id {
 __u32 match_flags;
 __u32 vendor_id;
 __u32 model_id;
 __u32 specifier_id;
 __u32 version;
 kernel_ulong_t driver_data;
};
struct usb_device_id {
 __u16 match_flags;
 __u16 idVendor;
 __u16 idProduct;
 __u16 bcdDevice_lo;
 __u16 bcdDevice_hi;
 __u8 bDeviceClass;
 __u8 bDeviceSubClass;
 __u8 bDeviceProtocol;
 __u8 bInterfaceClass;
 __u8 bInterfaceSubClass;
 __u8 bInterfaceProtocol;
 __u8 bInterfaceNumber;
 kernel_ulong_t driver_info
  ;
};
struct hid_device_id {
 __u16 bus;
 __u16 group;
 __u32 vendor;
 __u32 product;
 kernel_ulong_t driver_data;
};
struct ccw_device_id {
 __u16 match_flags;
 __u16 cu_type;
 __u16 dev_type;
 __u8 cu_model;
 __u8 dev_model;
 kernel_ulong_t driver_info;
};
struct ap_device_id {
 __u16 match_flags;
 __u8 dev_type;
 kernel_ulong_t driver_info;
};
struct css_device_id {
 __u8 match_flags;
 __u8 type;
 kernel_ulong_t driver_data;
};
struct acpi_device_id {
 __u8 id[9];
 kernel_ulong_t driver_data;
};
struct pnp_device_id {
 __u8 id[8];
 kernel_ulong_t driver_data;
};
struct pnp_card_device_id {
 __u8 id[8];
 kernel_ulong_t driver_data;
 struct {
  __u8 id[8];
 } devs[8];
};
struct serio_device_id {
 __u8 type;
 __u8 extra;
 __u8 id;
 __u8 proto;
};
struct of_device_id
{
 char name[32];
 char type[32];
 char compatible[128];
 const void *data;
};
struct vio_device_id {
 char type[32];
 char compat[32];
};
struct pcmcia_device_id {
 __u16 match_flags;
 __u16 manf_id;
 __u16 card_id;
 __u8 func_id;
 __u8 function;
 __u8 device_no;
 __u32 prod_id_hash[4];
 const char * prod_id[4];
 kernel_ulong_t driver_info;
 char * cisfile;
};
struct input_device_id {
 kernel_ulong_t flags;
 __u16 bustype;
 __u16 vendor;
 __u16 product;
 __u16 version;
 kernel_ulong_t evbit[0x1f / 32 + 1];
 kernel_ulong_t keybit[0x2ff / 32 + 1];
 kernel_ulong_t relbit[0x0f / 32 + 1];
 kernel_ulong_t absbit[0x3f / 32 + 1];
 kernel_ulong_t mscbit[0x07 / 32 + 1];
 kernel_ulong_t ledbit[0x0f / 32 + 1];
 kernel_ulong_t sndbit[0x07 / 32 + 1];
 kernel_ulong_t ffbit[0x7f / 32 + 1];
 kernel_ulong_t swbit[0x0f / 32 + 1];
 kernel_ulong_t driver_info;
};
struct eisa_device_id {
 char sig[8];
 kernel_ulong_t driver_data;
};
struct parisc_device_id {
 __u8 hw_type;
 __u8 hversion_rev;
 __u16 hversion;
 __u32 sversion;
};
struct sdio_device_id {
 __u8 class;
 __u16 vendor;
 __u16 device;
 kernel_ulong_t driver_data;
};
struct ssb_device_id {
 __u16 vendor;
 __u16 coreid;
 __u8 revision;
};
struct bcma_device_id {
 __u16 manuf;
 __u16 id;
 __u8 rev;
 __u8 class;
};
struct virtio_device_id {
 __u32 device;
 __u32 vendor;
};
struct hv_vmbus_device_id {
 __u8 guid[16];
 kernel_ulong_t driver_data;
};
struct rpmsg_device_id {
 char name[32];
};
struct i2c_device_id {
 char name[20];
 kernel_ulong_t driver_data;
};
struct spi_device_id {
 char name[32];
 kernel_ulong_t driver_data;
};
enum dmi_field {
 DMI_NONE, DMI_BIOS_VENDOR, DMI_BIOS_VERSION, DMI_BIOS_DATE, DMI_SYS_VENDOR, DMI_PRODUCT_NAME, DMI_PRODUCT_VERSION, DMI_PRODUCT_SERIAL, DMI_PRODUCT_UUID, DMI_BOARD_VENDOR, DMI_BOARD_NAME, DMI_BOARD_VERSION, DMI_BOARD_SERIAL, DMI_BOARD_ASSET_TAG, DMI_CHASSIS_VENDOR, DMI_CHASSIS_TYPE, DMI_CHASSIS_VERSION, DMI_CHASSIS_SERIAL, DMI_CHASSIS_ASSET_TAG, DMI_STRING_MAX, };
struct dmi_strmatch {
 unsigned char slot:7;
 unsigned char exact_match:1;
 char substr[79];
};
struct dmi_system_id {
 int (*callback)(const struct dmi_system_id *);
 const char *ident;
 struct dmi_strmatch matches[4];
 void *driver_data;
};
struct platform_device_id {
 char name[20];
 kernel_ulong_t driver_data;
};
struct mdio_device_id {
 __u32 phy_id;
 __u32 phy_id_mask;
};
struct zorro_device_id {
 __u32 id;
 kernel_ulong_t driver_data;
};
struct isapnp_device_id {
 unsigned short card_vendor, card_device;
 unsigned short vendor, function;
 kernel_ulong_t driver_data;
};
struct amba_id {
 unsigned int id;
 unsigned int mask;
 void *data;
};
struct x86_cpu_id {
 __u16 vendor;
 __u16 family;
 __u16 model;
 __u16 feature;
 kernel_ulong_t driver_data;
};
struct cpu_feature {
 __u16 feature;
};
struct ipack_device_id {
 __u8 format;
 __u32 vendor;
 __u32 device;
};
struct mei_cl_device_id {
 char name[32];
 kernel_ulong_t driver_info;
};
struct mfd_cell;

struct mfd_cell {
	const char		*name;
	int			id;
	atomic_t		*usage_count;
	int			(*enable)(struct platform_device *dev);
	int			(*disable)(struct platform_device *dev);
	int			(*suspend)(struct platform_device *dev);
	int			(*resume)(struct platform_device *dev);
	void			*platform_data;
	size_t			pdata_size;
	const char		*of_compatible;
	int			num_resources;
	const struct resource	*resources;
	//bool			ignore_resource_conflicts;
	bool			pm_runtime_no_callbacks;
};

struct platform_device {
 const char *name;
 int id;
 bool id_auto;
 struct device dev;
 u32 num_resources;
 struct resource *resource;
 const struct platform_device_id *id_entry;
 struct mfd_cell *mfd_cell;
 struct pdev_archdata archdata;
};
struct platform_device_info {
  struct device *parent;
  struct acpi_dev_node acpi_node;
  const char *name;
  int id;
  const struct resource *res;
  unsigned int num_res;
  const void *data;
  size_t size_data;
  u64 dma_mask;
};
struct platform_driver {
 int (*probe)(struct platform_device *);
 int (*remove)(struct platform_device *);
 void (*shutdown)(struct platform_device *);
 int (*suspend)(struct platform_device *, pm_message_t state);
 int (*resume)(struct platform_device *);
 struct device_driver driver;
 const struct platform_device_id *id_table;
};
struct early_platform_driver {
 const char *class_str;
 struct platform_driver *pdrv;
 struct list_head list;
 int requested_id;
 char *buffer;
 int bufsize;
};
enum irqreturn {
 IRQ_NONE = (0 << 0), IRQ_HANDLED = (1 << 0), IRQ_WAKE_THREAD = (1 << 1), };
typedef enum irqreturn irqreturn_t;
unsigned int irq_get_next_irq(unsigned int offset);
enum {
 IRQC_IS_HARDIRQ = 0, IRQC_IS_NESTED, };
typedef irqreturn_t (*irq_handler_t)(int, void *);
struct irqaction {
 irq_handler_t handler;
 void *dev_id;
 void *percpu_dev_id;
 struct irqaction *next;
 irq_handler_t thread_fn;
 struct task_struct *thread;
 unsigned int irq;
 unsigned int flags;
 unsigned long thread_flags;
 unsigned long thread_mask;
 const char *name;
 struct proc_dir_entry *dir;
} ;
extern int
request_threaded_irq(unsigned int irq, irq_handler_t handler, irq_handler_t thread_fn, unsigned long flags, const char *name, void *dev);
extern int
request_any_context_irq(unsigned int irq, irq_handler_t handler, unsigned long flags, const char *name, void *dev_id);
extern int
request_percpu_irq(unsigned int irq, irq_handler_t handler, const char *devname, void *percpu_dev_id);
struct device;
extern int
devm_request_threaded_irq(struct device *dev, unsigned int irq, irq_handler_t handler, irq_handler_t thread_fn, unsigned long irqflags, const char *devname, void *dev_id);
struct irq_affinity_notify {
 unsigned int irq;
 struct kref kref;
 struct work_struct work;
 void (*notify)(struct irq_affinity_notify *, const cpumask_t *mask);
 void (*release)(struct kref *ref);
};
extern int
irq_set_affinity_notifier(unsigned int irq, struct irq_affinity_notify *notify);
enum
{
 HI_SOFTIRQ=0, TIMER_SOFTIRQ, NET_TX_SOFTIRQ, NET_RX_SOFTIRQ, BLOCK_SOFTIRQ, BLOCK_IOPOLL_SOFTIRQ, TASKLET_SOFTIRQ, SCHED_SOFTIRQ, HRTIMER_SOFTIRQ, RCU_SOFTIRQ, NR_SOFTIRQS
};
struct softirq_action
{
 void (*action)(struct softirq_action *);
};
 void do_softirq(void);
 void __do_softirq(void);
struct tasklet_struct
{
 struct tasklet_struct *next;
 unsigned long state;
 atomic_t count;
 void (*func)(unsigned long);
 unsigned long data;
};
enum
{
 TASKLET_STATE_SCHED, TASKLET_STATE_RUN
};
struct tasklet_hrtimer {
 struct hrtimer timer;
 struct tasklet_struct tasklet;
 enum hrtimer_restart (*function)(struct hrtimer *);
};
extern void
tasklet_hrtimer_init(struct tasklet_hrtimer *ttimer, enum hrtimer_restart (*function)(struct hrtimer *), clockid_t which_clock, enum hrtimer_mode mode);
struct seq_file;
int show_interrupts(struct seq_file *p, void *v);
int arch_show_interrupts(struct seq_file *p, int prec);
struct delay_timer {
 unsigned long (*read_current_timer)(void);
 unsigned long freq;
};
extern struct arm_delay_ops {
 void (*delay)(unsigned long);
 void (*const_udelay)(unsigned long);
 void (*udelay)(unsigned long);
 unsigned long ticks_per_jiffy;
} arm_delay_ops;
void calibrate_delay(void);
void msleep(unsigned int msecs);
unsigned long msleep_interruptible(unsigned int msecs);
void usleep_range(unsigned long min, unsigned long max);
struct device;
struct clk;
struct clk_notifier {
 struct clk *clk;
 struct srcu_notifier_head notifier_head;
 struct list_head node;
};
struct clk_notifier_data {
 struct clk *clk;
 unsigned long old_rate;
 unsigned long new_rate;
};
int clk_notifier_register(struct clk *clk, struct notifier_block *nb);
int clk_notifier_unregister(struct clk *clk, struct notifier_block *nb);
int clk_prepare(struct clk *clk);
void clk_unprepare(struct clk *clk);
struct clk *clk_get(struct device *dev, const char *id);
struct clk *devm_clk_get(struct device *dev, const char *id);
int clk_enable(struct clk *clk);
void clk_disable(struct clk *clk);
unsigned long clk_get_rate(struct clk *clk);
void clk_put(struct clk *clk);
void devm_clk_put(struct device *dev, struct clk *clk);
long clk_round_rate(struct clk *clk, unsigned long rate);
int clk_set_rate(struct clk *clk, unsigned long rate);
int clk_set_parent(struct clk *clk, struct clk *parent);
struct clk *clk_get_parent(struct clk *clk);
struct clk *clk_get_sys(const char *dev_id, const char *con_id);
int clk_add_alias(const char *alias, const char *alias_dev_name, char *id, struct device *dev);
struct device_node;
struct of_phandle_args;
struct clk *of_clk_get(struct device_node *np, int index);
struct clk *of_clk_get_by_name(struct device_node *np, const char *name);
struct clk *of_clk_get_from_provider(struct of_phandle_args *clkspec);

struct task_struct *kthread_create_on_node(int (*threadfn)(void *data), void *data, int node, const char namefmt[], ...);
struct task_struct *kthread_create_on_cpu(int (*threadfn)(void *data), void *data, unsigned int cpu, const char *namefmt);
void kthread_bind(struct task_struct *k, unsigned int cpu);
int kthread_stop(struct task_struct *k);
bool kthread_should_stop(void);
bool kthread_should_park(void);
bool kthread_freezable_should_stop(bool *was_frozen);
void *kthread_data(struct task_struct *k);
void *probe_kthread_data(struct task_struct *k);
int kthread_park(struct task_struct *k);
void kthread_unpark(struct task_struct *k);
void kthread_parkme(void);
int kthreadd(void *unused);
struct kthread_work;
typedef void (*kthread_work_func_t)(struct kthread_work *work);
struct kthread_worker {
 spinlock_t lock;
 struct list_head work_list;
 struct task_struct *task;
 struct kthread_work *current_work;
};
struct kthread_work {
 struct list_head node;
 kthread_work_func_t func;
 wait_queue_head_t done;
 struct kthread_worker *worker;
};
int kthread_worker_fn(void *worker_ptr);
bool queue_kthread_work(struct kthread_worker *worker, struct kthread_work *work);
void flush_kthread_work(struct kthread_work *work);
void flush_kthread_worker(struct kthread_worker *worker);
struct proc_dir_entry;
int rk_vendor_read(u32 id, void *pbuf, u32 size);
int rk_vendor_write(u32 id, void *pbuf, u32 size);
int rk_vendor_register(void *read, void *write);
struct nand_part {
 unsigned char name[32];
 unsigned long offset;
 unsigned long size;
 unsigned char type;
};

// size==0x2c
struct nand_blk_dev {
 struct nand_blk_ops *nandr;
 struct list_head list;

  //@@@@int devnum;
  uchar    ch0;
  uchar    ch1;
  ushort   us0;

  unsigned long devnum;  //@@@@ size
  unsigned long size; //@@@@off_size;

  int offset; //pad[1]; //@@@@

 int readonly;
 int writeonly;
 int disable_access;
 gendisk *blkcore_priv;
};

struct nand_blk_ops {
 char *name;
 int major;
 int minorbits;

  //@@@@
  int  (*fnc_add_dev)();
  int  (*fnc_remove_dev)(struct nand_blk_dev*);
  int  (*fnc_getgeo)();
  int  (*fnc_flush)();


 int last_dev_index;
 int pad0;
 struct completion thread_exit; // int[4]
 int quit;
 int nand_th_quited;
 int pad1[1]; //
 wait_queue_head_t thread_wq;// int[3]
  //struct list_head devs;
 struct request_queue *rq;
 spinlock_t queue_lock;

  int pad[6];//@@@@
  list_head  list;

 struct module *owner;
};
void rknand_dev_suspend(void);
void rknand_dev_resume(void);
void rknand_dev_shutdown(void);
void rknand_dev_flush(void);
int rknand_dev_init(void);
int rknand_dev_exit(void);
void rk_nandc_flash_xfer_completed(void *nandc_reg);
void rk_nandc_flash_ready(void *nandc_reg);
u32 rk_nandc_get_irq_status(void *nandc_reg);
int rknand_proc_ftlread(char *page);
int FtlRead(u8 lun, u32 index, u32 sectors, u8 *buf);
int FtlWrite(u8 lun, u32 index, u32 sectors, u8 *buf);
int rk_ftl_garbage_collect(u32 mode, u32 pages);
void rk_ftl_cache_write_back(void);
int FtlDiscard(u32 index, u32 sectors);
int rk_nand_schedule_enable_config(int en);
int rk_ftl_get_capacity(void);
void rk_ftl_storage_sys_init(void);
int rk_ftl_init(void);
void rk_nand_de_init(void);
void rk_ftl_de_init(void);
void rk_nand_suspend(void);
void rk_nand_resume(void);
int rknand_get_reg_addr(unsigned long *p_nandc0, unsigned long *p_nandc1);
long rknand_sys_storage_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
long rk_ftl_vendor_storage_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
int rk_ftl_vendor_write(u32 id, void *pbuf, u32 size);
int rk_ftl_vendor_read(u32 id, void *pbuf, u32 size);
int rk_ftl_vendor_storage_init(void);
int rknand_vendor_storage_init(void);


int  rknand_open    (struct block_device *, fmode_t);
void rknand_release (struct gendisk *, fmode_t);
int  rknand_ioctl   (struct block_device *, fmode_t, unsigned, unsigned long);

void set_disk_ro(struct gendisk *disk, int flag);

struct miscdevice  {
	int minor;
	const char *name;
	const struct file_operations *fops;
	struct list_head list;
	struct device *parent;
	struct device *this_device;
	const char *nodename;
	umode_t mode;
};

int nand_add_dev(struct nand_blk_ops *nandr, struct nand_part *part);
int rknand_get_part(char *parts, struct nand_part *this_part, int *part_index);
int nand_blk_register(struct nand_blk_ops *nandr);
int nand_blktrans_thread(void *arg);
void nand_blk_unregister(struct nand_blk_ops *nandr);
int  fnc_remove_dev(struct nand_blk_dev*);

void add_timer(struct timer_list *timer);


// for kernel
typedef int suspend_state_t;
struct regmap;
struct regulator_consumer_supply;
struct regulator_state {
	int uV;	/* suspend voltage */
	unsigned int mode; /* suspend regulator operating mode */
	int enabled; /* is regulator enabled in this suspend state */
	int disabled; /* is the regulator disbled in this suspend state */
};
struct regulation_constraints {
	const char *name;
	int min_uV;
	int max_uV;
	int uV_offset;
	int min_uA;
	int max_uA;
	unsigned int valid_modes_mask;
	unsigned int valid_ops_mask;
	int input_uV;
	struct regulator_state state_disk;
	struct regulator_state state_mem;
	struct regulator_state state_standby;
	suspend_state_t initial_state; /* suspend state to set at init */
	unsigned int initial_mode;
	unsigned int ramp_delay;
	unsigned always_on:1;	/* regulator never off when system is on */
	unsigned boot_on:1;	/* bootloader/firmware enabled regulator */
	unsigned apply_uV:1;	/* apply uV constraint if min == max */
};
struct regulator_init_data {
	const char *supply_regulator;        /* or NULL for system supply */
	struct regulation_constraints constraints;
	int num_consumer_supplies;
	struct regulator_consumer_supply *consumer_supplies;
	/* optional regulator machine specific init */
	int (*regulator_init)(void *driver_data);
	void *driver_data;	/* core does not touch this */
};
struct regulator_config {
	struct device *dev;
	const struct regulator_init_data *init_data;
	void *driver_data;
	struct device_node *of_node;
	struct regmap *regmap;

	int ena_gpio;
	unsigned int ena_gpio_invert:1;
	unsigned int ena_gpio_flags;
};
struct regulator_ops {
	int (*list_voltage) (struct regulator_dev *, unsigned selector);
	int (*set_voltage) (struct regulator_dev *, int min_uV, int max_uV,unsigned *selector);
	int (*map_voltage)(struct regulator_dev *, int min_uV, int max_uV);
	int (*set_voltage_sel) (struct regulator_dev *, unsigned selector);
	int (*get_voltage) (struct regulator_dev *);
	int (*get_voltage_sel) (struct regulator_dev *);
	int (*set_current_limit) (struct regulator_dev *,int min_uA, int max_uA);
	int (*get_current_limit) (struct regulator_dev *);
	int (*enable) (struct regulator_dev *);
	int (*disable) (struct regulator_dev *);
	int (*is_enabled) (struct regulator_dev *);
	int (*set_mode) (struct regulator_dev *, unsigned int mode);
	unsigned int (*get_mode) (struct regulator_dev *);
	int (*enable_time) (struct regulator_dev *);
	int (*set_ramp_delay) (struct regulator_dev *, int ramp_delay);
	int (*set_voltage_time_sel) (struct regulator_dev *,unsigned int old_selector,unsigned int new_selector);
	int (*get_status)(struct regulator_dev *);
	unsigned int (*get_optimum_mode) (struct regulator_dev *, int input_uV,int output_uV, int load_uA);
	int (*set_bypass)(struct regulator_dev *dev, bool enable);
	int (*get_bypass)(struct regulator_dev *dev, bool *enable);
	int (*set_suspend_voltage) (struct regulator_dev *, int uV);
	int (*set_suspend_enable) (struct regulator_dev *);
	int (*set_suspend_disable) (struct regulator_dev *);
	int (*set_suspend_mode) (struct regulator_dev *, unsigned int mode);
};

struct regulator_desc {
	const char *name;
	const char *supply_name;
	int id;
	bool continuous_voltage_range;
	unsigned n_voltages;
	struct regulator_ops *ops;
	int irq;
	enum regulator_type type;
	struct module *owner;
	unsigned int min_uV;
	unsigned int uV_step;
	unsigned int linear_min_sel;
	unsigned int ramp_delay;
	const unsigned int *volt_table;
	unsigned int vsel_reg;
	unsigned int vsel_mask;
	unsigned int apply_reg;
	unsigned int apply_bit;
	unsigned int enable_reg;
	unsigned int enable_mask;
	bool enable_is_inverted;
	unsigned int bypass_reg;
	unsigned int bypass_mask;
	unsigned int enable_time;
};

struct axp192_regulator_info {
  struct regulator_desc   desc;
  int   i0_[4];
	int	min_uV;
	int	max_uV;
	int	step_uV;
	int	vol_reg;
	int	vol_shift;
	int	vol_nbits;
	int	enable_reg;
	int	enable_bit;
  int   i1_[3];
};

struct regulator_dev*regulator_register(const struct regulator_desc *regulator_desc,const struct regulator_config *config);
struct i2c_client;
s32 i2c_smbus_read_byte(const struct i2c_client *client);
s32 i2c_smbus_write_byte(const struct i2c_client *client, u8 value);
s32 i2c_smbus_read_byte_data(const struct i2c_client *client, u8 command);
s32 i2c_smbus_write_byte_data(const struct i2c_client *client, u8 command,u8 value);
s32 i2c_smbus_read_word_data(const struct i2c_client *client, u8 command);
s32 i2c_smbus_write_word_data(const struct i2c_client *client, u8 command,u16 value);
s32 i2c_smbus_read_block_data(const struct i2c_client *client, u8 command,u8 *values);
s32 i2c_smbus_write_block_data(const struct i2c_client *client, u8 command,u8 length, const u8 *values);
s32 i2c_smbus_read_i2c_block_data(const struct i2c_client *client, u8 command,u8 length, u8 *values);
s32 i2c_smbus_write_i2c_block_data(const struct i2c_client *client, u8 command,u8 length, const u8 *values);
s32 i2c_smbus_xfer(struct i2c_adapter *adapter, u16 addr, unsigned short flags,char read_write, u8 command, int protocol,union i2c_smbus_data *data);

int regulator_disable(struct regulator *regulator);
int regulator_enable(struct regulator *regulator);

int mfd_add_devices(struct device *parent, int id, struct mfd_cell *cells, int n_devs,struct resource *mem_base,int irq_base, struct irq_domain *domain);
int mfd_add_device(struct device *parent, int id,const struct mfd_cell *cell,struct resource *mem_base,int irq_base, struct irq_domain *domain);


s32 axp192_reg_read(i2c_client*i2c, int addr, int*out);
s32 axp192_reg_write(i2c_client*i2c, int addr, int data);

int axp192_list_voltage(struct regulator_dev *, unsigned selector);
int axp192_set_voltage(struct regulator_dev *, int min_uV, int max_uV,unsigned *selector);
int axp192_get_voltage(struct regulator_dev *);
int axp192_enable(struct regulator_dev *);
int axp192_disable(struct regulator_dev *);
int axp192_is_enabled(struct regulator_dev *);
int axp192_set_suspend_voltage(struct regulator_dev *, int uV);
