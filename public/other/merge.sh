#!/bin/bash


PROJECTS="
bionic
external/kernel-headers
frameworks/base
frameworks/native
frameworks/opt/net/wifi
hardware/libhardware
hardware/libhardware_legacy
system/extras
system/core
"



# add local and remote branch
for P in $PROJECTS
do
    echo "--- $P ---"
    pushd $P >> /dev/null

    git checkout android-7.1.2_r39
    git checkout -b android-7.1.2_tstation
    git remote   add -t android-7.1.2_tstation gitlab https://gitlab.com/aosp-tstation-ngt/aosp/${P}.git

    popd >> /dev/null
done


# fetch and merge
for P in $PROJECTS
do
    echo "--- $P ---"
    pushd $P >> /dev/null

    git checkout android-7.1.2_tstation
    git fetch    gitlab
    git branch   -u gitlab/android-7.1.2_tstation android-7.1.2_tstation
    git merge

    popd >> /dev/null
done

