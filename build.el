;; execute M-x eval-buffer 3 times to publish. (1st and 2nd eval-buffer might fail ??)


(require 'org)
;(require 'org-list)
;(require 'org-element)
(require 'ox-publish)
;(require 'htmlize)

(setq org-directory "./")
;(setq org-publish-use-timestamps-flag nil) ;; publish un-conditionally( ie. ignore timestamp ).
(setq org-publish-use-timestamps-flag t) ;; publish only modified files.
;;(setq org-html-indent nil) ;; keep indent


(defun tmp-htmlize (alist file dir)
  (htmlize-file file dir))

(setq org-publish-project-alist
 '(
   ("org"
  :base-extension "org"
  :base-directory "./src"
  :publishing-directory "./public"
  :publishing-function org-html-publish-to-html
  :recursive t

  :htmlized-source nil

  :html-head "<link rel=\"stylesheet\" href=\"../css/norang.css\" type=\"text/css\"/>"
  :html-preamble nil

  :with-author nil
  :with-timestamps nil
  :with-timestamp nil
  :with-toc nil
  :html-postamble nil
  :html-link-up "readme.html"
;  :html-link-home "readme.html"
  :html-home/up-format "<div id=\"org-div-home-and-up\">  <a accesskey=\"h\" href=\"%s\"> UP </a> </div>"
  )

  ("image"
  :base-extension "jpg\\|png"
  :base-directory "./src"
  :htmlized-source t
  :publishing-directory "./public"
  :publishing-function org-publish-attachment
  :recursive t
  )

  ("other"
  :base-extension "sh\\|txt\\|xml\\|html\\|patch"
  :base-directory "./src"
  :exclude "publish\\.el"
  :htmlized-source t
  :publishing-directory "./public"
  :publishing-function org-publish-attachment
  :recursive t
  )
  ("website" :components ("org" "image" "other"))
 )
)
(org-publish-all)
